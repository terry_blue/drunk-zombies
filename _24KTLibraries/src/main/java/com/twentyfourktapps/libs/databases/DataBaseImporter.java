package com.twentyfourktapps.libs.databases;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

public abstract class DataBaseImporter extends SQLiteOpenHelper{

	
	
	final static String TAG = "DataBaseImporter";
	final private String DB_NAME;
	final private String DB_PATH;
	final private int DB_VERS;
	final protected Context CXT;
	
	final private static String SP_VERSION_KEY = "com.twentyfourktapps.libs.databases.DataBaseImporter";
	
	protected SQLiteDatabase sqLiteDb; 
	
	public DataBaseImporter(Context context, String db_name, int versionNum) {		 
		super(context, db_name, null, versionNum);
		DB_NAME = db_name;
		DB_VERS = versionNum;
	    DB_PATH = context.getDatabasePath(DB_NAME).getAbsolutePath();   
	    CXT = context;	        
	    
	    try {
	       	createDataBase();
	    } catch(Exception e) { Log.e(TAG, "constructor could not create database: " + e.toString()); }
	}	
	 
	/**
	  * Creates a empty database on the system and rewrites it with your own database.
	**/
	public void createDataBase() throws IOException{
		
	   	if(checkDataBase())	openDataBase();
	   	
	   	else {
	   		Log.i(TAG, "DB DOES NOT EXIST...... Creating...");
	   		//By calling this method and empty database will be created into the default system path
	        //of your application so we are gonna be able to overwrite that database with our database.
	       	sqLiteDb = getReadableDatabase();
	 
	       	try {	 
	   			copyDataBase();	 
	   			openDataBase();
	   		} catch (IOException e) { throw new Error("Error copying database"); }
	  	}
	 
	 }
	 
	 /**
	 * Check if the database already exist to avoid re-copying the file each time you open the application.
	 * @return true if it exists, false if it doesn't
	 */
	 protected boolean checkDataBase(){
	 
	 	SQLiteDatabase checkDB = null;
	 	
	   	try {
	   		checkDB = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READONLY);
	   		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(CXT);
	   		if(DB_VERS > sp.getInt(SP_VERSION_KEY, 0))
	   			return false;
	   	} catch(SQLiteException e){ Log.e(TAG, "checkDatabase() catch: " + e.toString()); }
	 
	   	if(checkDB != null)	checkDB.close();
	   	return checkDB != null;
	   	//return false; //TO COPY DB EACH TIME GAME LOADS FOR DEBUGGING
	
	 }
	 
	 /**
	  * Copies your database from your local assets-folder to the just created empty database in the
	  * system folder, from where it can be accessed and handled.
	  * This is done by transfering bytestream.
	 * */
	 private void copyDataBase() throws IOException{
	 
	   	//Open your local db as the input stream
	   	InputStream myInput = CXT.getAssets().open(DB_NAME);
	 
	 
	   	//Open the empty db as the output stream
	   	OutputStream myOutput = new FileOutputStream(DB_PATH);
	 
	   	//transfer bytes from the inputfile to the outputfile
	   	byte[] buffer = new byte[1024];
	   	int length;
	   	while ((length = myInput.read(buffer))>0){
	   		myOutput.write(buffer, 0, length);
	   		//Log.e(TAG, buffer.toString());
	   	}
	 
	   	//Close the streams
	   	myOutput.flush();
	   	myOutput.close();
	   	myInput.close();
	   	
	   	SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(CXT);
	   	SharedPreferences.Editor edit = sp.edit();
	   	edit.putInt(SP_VERSION_KEY, DB_VERS);
	   	edit.commit();
	 
	 }
	 
	public void openDataBase() throws SQLException{
	   	try {	   	
	   		sqLiteDb = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);
	   	} catch(Exception e) { Log.e(TAG, "open database error: " + e.toString()); }	 
	}
	 
	@Override
	public synchronized void close() {	 
	    if(sqLiteDb != null)
		    sqLiteDb.close();	 
	   	super.close();	 
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		CXT.deleteDatabase(DB_NAME);
		
		try { 
			Log.i(TAG, "upgrading database");
			copyDataBase();	
		} catch(Exception e) { Log.e(TAG, "error upgrading DataBase: " + e.toString()); }
	}
	 
	
}
