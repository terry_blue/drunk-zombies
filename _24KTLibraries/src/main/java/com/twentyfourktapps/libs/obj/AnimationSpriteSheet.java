package com.twentyfourktapps.libs.obj;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class AnimationSpriteSheet {

	public Bitmap BITMAP;
	final public int MAX_FRAMES;	
	
	public AnimationSpriteSheet(Resources res, int resID, int frameCount) {
		MAX_FRAMES = frameCount;
		BITMAP = BitmapFactory.decodeResource(res, resID);
		
	}
	
	public Rect getSrcRectForFrame(int frame) {
		return new Rect((int)(((float)BITMAP.getWidth() / (float)MAX_FRAMES) * (float)frame)
						, 0
						, (int)(((float)BITMAP.getWidth() / (float)MAX_FRAMES) * ((float)frame + 1f))
						, BITMAP.getHeight());		
	}
	
	public Float getFrameWidth() {		
		return (float)BITMAP.getWidth() / (float)MAX_FRAMES;
	}
	
	public void releaseBitmap() {
		if(BITMAP != null) {
			BITMAP.recycle();
		}
		BITMAP = null;
	}	
		
}
