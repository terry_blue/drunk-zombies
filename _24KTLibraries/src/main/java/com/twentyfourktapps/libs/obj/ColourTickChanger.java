package com.twentyfourktapps.libs.obj;

import android.graphics.Color;

public class ColourTickChanger {

	final int START_COLOR, END_COLOR, MAX_TICKS;
	int curTickCount = 0;
	boolean isIncrementUp = true;
	
	float incrementAlpha, incrementRed, incrementGreen, incrementBlue;
	
	public ColourTickChanger(int startColor, int endColor, int delayBetweenFullChange) {
		START_COLOR = startColor;
		END_COLOR = endColor;
		MAX_TICKS = delayBetweenFullChange;
		
		incrementAlpha = ((float)Color.alpha(END_COLOR) - (float)Color.alpha(START_COLOR)) / (float)MAX_TICKS;
		incrementRed = ((float)Color.red(END_COLOR) - (float)Color.red(START_COLOR)) / (float)MAX_TICKS;
		incrementGreen = ((float)Color.green(END_COLOR) - (float)Color.green(START_COLOR)) / (float)MAX_TICKS;
		incrementBlue = ((float)Color.blue(END_COLOR) - (float)Color.blue(START_COLOR)) / (float)MAX_TICKS;
	}
	
	public int getColor() {
		if(isIncrementUp) {
			if(curTickCount < MAX_TICKS)
				curTickCount++;
			else {
				curTickCount--;
				isIncrementUp = false;
			}
		} else {
			if(curTickCount > 0) 
				curTickCount--;
			else {
				curTickCount++;
				isIncrementUp = true;
			}
		}
		
		int alpha = (Color.alpha(START_COLOR) + (int)(incrementAlpha * (float)curTickCount));
		int red = Color.red(START_COLOR) + (int)(incrementRed * (float)curTickCount);
		int green = Color.green(START_COLOR) + (int)(incrementGreen * (float)curTickCount);
		int blue = Color.blue(START_COLOR) + (int)(incrementBlue * (float)curTickCount);
		
		return Color.argb(alpha, red, green, blue);
	}
}
