package com.twentyfourktapps.drunkzombies.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.obj.Player;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;

public abstract class ServerUtil {
	
	final static String TAG = "ServerUtil";
	final private static boolean showResponse = true;
	
	
	private final static String WEB_PREFIX = "http://www.drunkzombies.com/Android/Games/DrunkZombies/";
	
	public final static String ADDRESS_STORE_DATA = WEB_PREFIX + "storeLevels.php";
	public final static String ADDRESS_GET_DATA = WEB_PREFIX + "getLevels.php";
	public final static String ADDRESS_CREATE_USER = WEB_PREFIX + "createUser.php"; 
	private static String lastError = "none";
	
	
	
	public static String getLastError() {
		return lastError;
	}
	
	public static String getResponse(String targetAddress) {
		ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
		return getResponse(targetAddress, pairs);
	}
	
	
	public static String getResponse(String targetAddress, ArrayList<NameValuePair> postPairs) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(targetAddress);
			httpPost.setEntity(new UrlEncodedFormEntity(postPairs));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();			
			final InputStream inputStream = entity.getContent();
			
			//Read the response
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 32);
				StringBuilder sb = new StringBuilder();
				String line;
				while((line = reader.readLine()) != null) {
					sb.append(line);					
				}
				if(showResponse) Log.i(TAG, "response for targetAddress " + targetAddress + " = " + sb.toString());
				return sb.toString();
				
			} catch(Exception e) {
				Log.e(TAG, "error reading response for targetAddress: " + targetAddress + "... " + e.toString());
			}
		} catch (UnknownHostException e) {
			Log.e(TAG, "UnknownHostException: " + e.toString());
			return "host_error";
		} catch(Exception e) {
			Log.e(TAG, "getResponse() error for address: " + targetAddress + "... " + e.toString());
		}
		return "fail";
	}
	
	
	public static boolean createDZuser(Context context) {
		if(Util.isUserLoggedIn(context)) {
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
			ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("udid", sp.getString(Consts.SP_KEY_USER_ID, "Johnny")));
			
			String response = getResponse(ADDRESS_CREATE_USER, pairs);
			if(response.equals("user_created")) 
				return sendDZdata(context);		
			else lastError = response;
			
		}
		
		return false;
	}
	
	public static boolean sendDZdata(Context context) {
		if(Util.isUserLoggedIn(context)) {
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
			ArrayList<NameValuePair> httpPairs = new ArrayList<NameValuePair>();
			
			int currencySyncCount = sp.getInt(Consts.SP_KEY_SYNC_COUNT_CURRENCIES, 1) + 1;
			int levelsSyncCount = sp.getInt(Consts.SP_KEY_SYNC_COUNT_LEVELS, 1) + 1;
			int userSyncCount = sp.getInt(Consts.SP_KEY_SYNC_COUNT_USER, 1) + 1;
			int coinsSpent = sp.getInt(Consts.SP_KEY_STATS_COINS_SPENT, 0);
			int tokensSpent = sp.getInt(Consts.SP_KEY_STATS_TOKENS_SPENT, 0);
			String udid = sp.getString(Consts.SP_KEY_USER_ID, "Johnny");
			
			SharedPreferences.Editor editor = sp.edit();
			editor.putInt(Consts.SP_KEY_SYNC_COUNT_USER, userSyncCount);
			editor.putInt(Consts.SP_KEY_SYNC_COUNT_CURRENCIES, currencySyncCount);
			editor.putInt(Consts.SP_KEY_SYNC_COUNT_LEVELS, levelsSyncCount);
			editor.commit();
		
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_USER_ID, udid));			
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_STATS_COINS_SPENT, Integer.toString(coinsSpent)));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_STATS_TOKENS_SPENT, Integer.toString(tokensSpent)));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_SYNC_COUNT_CURRENCIES, Integer.toString(currencySyncCount)));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_SYNC_COUNT_LEVELS, Integer.toString(levelsSyncCount)));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_SYNC_COUNT_USER, Integer.toString(userSyncCount)));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_TURRET_SPOT_1, Integer.toString(sp.getInt(Consts.SP_KEY_TURRET_SPOT_1, -1))));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_TURRET_SPOT_2, Integer.toString(sp.getInt(Consts.SP_KEY_TURRET_SPOT_2, -1))));
			
			
			httpPairs.add(new BasicNameValuePair(Player.KEY_COINS, Integer.toString(PlayerLevels.curLevelCoins)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_TOKENS, Integer.toString(PlayerLevels.curLevelTokens)));
			
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_TURRET_BLUE_BOUGHT, Boolean.toString(sp.getBoolean(Consts.SP_KEY_TURRET_BLUE_BOUGHT, false))));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_TURRET_GREEN_BOUGHT, Boolean.toString(sp.getBoolean(Consts.SP_KEY_TURRET_GREEN_BOUGHT, false))));
			httpPairs.add(new BasicNameValuePair(Consts.SP_KEY_TURRET_YELLOW_BOUGHT, Boolean.toString(sp.getBoolean(Consts.SP_KEY_TURRET_YELLOW_BOUGHT, false))));		
			//ALL Levels
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_STRENGTH, Integer.toString(PlayerLevels.curStrength)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_AGILITY, Integer.toString(PlayerLevels.curAgility)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_CRITICAL, Integer.toString(PlayerLevels.curCritical)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POISON, Integer.toString(PlayerLevels.curPoison)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_KICK, Integer.toString(PlayerLevels.curKick)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_MULTI, Integer.toString(PlayerLevels.curMultiShot)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_YELLOW, Integer.toString(PlayerLevels.curTurretYellow)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_GREEN, Integer.toString(PlayerLevels.curTurretGreen)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_BLUE, Integer.toString(PlayerLevels.curTurretBlue)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_YELLOW_DAMAGE, Integer.toString(PlayerLevels.curTurretYellowDamage)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_GREEN_DAMAGE, Integer.toString(PlayerLevels.curTurretGreenDamage)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_BLUE_DAMAGE, Integer.toString(PlayerLevels.curTurretBlueDamage)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_YELLOW_ROF, Integer.toString(PlayerLevels.curTurretYellowROF)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_GREEN_ROF, Integer.toString(PlayerLevels.curTurretGreenROF)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_TURRET_BLUE_ROF, Integer.toString(PlayerLevels.curTurretBlueROF)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_BOTTLE_QUALITY, Integer.toString(PlayerLevels.curBottleQuality)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_BOTTLE_THICKNESS, Integer.toString(PlayerLevels.curBottleThickness)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_BOTTLE_COUNT_RED, Integer.toString(PlayerLevels.curBottlesRed)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_BOTTLE_COUNT_ORANGE, Integer.toString(PlayerLevels.curBottlesOrange)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_BOTTLE_COUNT_GREEN, Integer.toString(PlayerLevels.curBottlesGreen)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_YELLOW_DAMAGE, Integer.toString(PlayerLevels.curPowerYellowDamage)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_GREEN_DAMAGE, Integer.toString(PlayerLevels.curPowerGreenDamage)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_BLUE_DAMAGE, Integer.toString(PlayerLevels.curPowerBlueDamage)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_YELLOW, Integer.toString(PlayerLevels.curPowerYellow)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_GREEN, Integer.toString(PlayerLevels.curPowerGreen)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_BLUE, Integer.toString(PlayerLevels.curPowerBlue)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_YELLOW_RADIUS, Integer.toString(PlayerLevels.curPowerYellowRadius)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_GREEN_RADIUS, Integer.toString(PlayerLevels.curPowerGreenRadius)));
			httpPairs.add(new BasicNameValuePair(Player.KEY_LEVEL_POWER_BLUE_RADIUS, Integer.toString(PlayerLevels.curPowerBlueRadius)));
			
			String response = getResponse(ADDRESS_STORE_DATA, httpPairs);
			if(response.equals("Data Saved")) return true;
			else lastError = response;
		} 		
		
		return false;
	}

	public static boolean getDZdata(Context context) {
		if(Util.isUserLoggedIn(context)) {
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
			String udid = sp.getString(Consts.SP_KEY_USER_ID, "Johnny");
			int syncCountCurrency = sp.getInt(Consts.SP_KEY_SYNC_COUNT_CURRENCIES, 1);
			int syncCountUsers = sp.getInt(Consts.SP_KEY_SYNC_COUNT_USER, 1);
			int syncCountLevels = sp.getInt(Consts.SP_KEY_SYNC_COUNT_LEVELS, 1);			
			
			ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair(Consts.SP_KEY_USER_ID, udid));
			pairs.add(new BasicNameValuePair(Consts.SP_KEY_SYNC_COUNT_CURRENCIES, Integer.toString(syncCountCurrency)));
			pairs.add(new BasicNameValuePair(Consts.SP_KEY_SYNC_COUNT_LEVELS, Integer.toString(syncCountLevels)));
			pairs.add(new BasicNameValuePair(Consts.SP_KEY_SYNC_COUNT_USER, Integer.toString(syncCountUsers)));
			
			String serverResponse = getResponse(ADDRESS_GET_DATA, pairs);			
			
			
			if(serverResponse != null && serverResponse.length() > 0 && (serverResponse.charAt(0) == '{' || serverResponse.charAt(0) == '[')) {
				SharedPreferences.Editor editor = sp.edit();				
				try {
					//final JSONArray jArray = new JSONArray(serverResponse);
					final JSONObject json_data = new JSONObject(serverResponse);
					
					//CURRENCIES
					if(json_data.has("curTokens")) {
						editor.putInt(Player.KEY_TOKENS, Integer.parseInt(json_data.getString("curTokens")));
						editor.putInt(Player.KEY_COINS, Integer.parseInt(json_data.getString("curCoins")));
						editor.putInt(Consts.SP_KEY_STATS_TOKENS_EANRED, Integer.parseInt(json_data.getString("tokensEarned")));
						editor.putInt(Consts.SP_KEY_STATS_TOKENS_SPENT, Integer.parseInt(json_data.getString("tokensSpent")));
						editor.putInt(Consts.SP_KEY_STATS_COINS_EARNED, Integer.parseInt(json_data.getString("coinsEarned")));
						editor.putInt(Consts.SP_KEY_STATS_COINS_SPENT, Integer.parseInt(json_data.getString("coinsSpent")));
						editor.putInt(Consts.SP_KEY_SYNC_COUNT_CURRENCIES, Integer.parseInt(json_data.getString("SyncCountCurrency")));
						editor.putBoolean(Consts.SP_KEY_TURRET_BLUE_BOUGHT, json_data.getString("blueTurretBought").equals("true"));
						editor.putBoolean(Consts.SP_KEY_TURRET_GREEN_BOUGHT, json_data.getString("greenTurretBought").equals("true"));
						editor.putBoolean(Consts.SP_KEY_TURRET_YELLOW_BOUGHT, json_data.getString("yellowTurretBought").equals("true"));
						
						
					}
					
					//LEVELS
					if(json_data.has("Strength")) {
						editor.putInt(Consts.SP_KEY_SYNC_COUNT_LEVELS, Integer.parseInt(json_data.getString("SyncCountLevels")));
						editor.putInt(Player.KEY_LEVEL_STRENGTH, Integer.parseInt(json_data.getString("Strength")));
						editor.putInt(Player.KEY_LEVEL_AGILITY, Integer.parseInt(json_data.getString("Agility")));
						editor.putInt(Player.KEY_LEVEL_CRITICAL, Integer.parseInt(json_data.getString("Critical")));
						editor.putInt(Player.KEY_LEVEL_POISON, Integer.parseInt(json_data.getString("Poison")));
						editor.putInt(Player.KEY_LEVEL_KICK, Integer.parseInt(json_data.getString("Kick")));
						editor.putInt(Player.KEY_LEVEL_MULTI, Integer.parseInt(json_data.getString("Multishot")));
						
						editor.putInt(Player.KEY_LEVEL_BOTTLE_COUNT_RED, Integer.parseInt(json_data.getString("BottleCountRed")));
						editor.putInt(Player.KEY_LEVEL_BOTTLE_COUNT_ORANGE, Integer.parseInt(json_data.getString("BottleCountOrange")));
						editor.putInt(Player.KEY_LEVEL_BOTTLE_COUNT_GREEN, Integer.parseInt(json_data.getString("BottleCountGreen")));
						editor.putInt(Player.KEY_LEVEL_BOTTLE_THICKNESS, Integer.parseInt(json_data.getString("BottleThickness")));
						editor.putInt(Player.KEY_LEVEL_BOTTLE_QUALITY, Integer.parseInt(json_data.getString("BottleQuality")));
						
						editor.putInt(Player.KEY_LEVEL_TURRET_BLUE, Integer.parseInt(json_data.getString("TurretBlue")));
						editor.putInt(Player.KEY_LEVEL_TURRET_GREEN, Integer.parseInt(json_data.getString("TurretGreen")));
						editor.putInt(Player.KEY_LEVEL_TURRET_YELLOW, Integer.parseInt(json_data.getString("TurretYellow")));
						editor.putInt(Player.KEY_LEVEL_TURRET_BLUE_ROF, Integer.parseInt(json_data.getString("TurretBlueROF")));
						editor.putInt(Player.KEY_LEVEL_TURRET_GREEN_ROF, Integer.parseInt(json_data.getString("TurretGreenROF")));
						editor.putInt(Player.KEY_LEVEL_TURRET_YELLOW_ROF, Integer.parseInt(json_data.getString("TurretYellowROF")));
						editor.putInt(Player.KEY_LEVEL_TURRET_BLUE_DAMAGE, Integer.parseInt(json_data.getString("TurretBlueDamage")));
						editor.putInt(Player.KEY_LEVEL_TURRET_GREEN_DAMAGE, Integer.parseInt(json_data.getString("TurretGreenDamage")));
						editor.putInt(Player.KEY_LEVEL_TURRET_YELLOW_DAMAGE, Integer.parseInt(json_data.getString("TurretYellowDamage")));
						
						
						editor.putInt(Player.KEY_LEVEL_POWER_BLUE_COUNT, Integer.parseInt(json_data.getString("PowerBlueCount")));
						editor.putInt(Player.KEY_LEVEL_POWER_GREEN_COUNT, Integer.parseInt(json_data.getString("PowerGreenCount")));
						editor.putInt(Player.KEY_LEVEL_POWER_YELLOW_COUNT, Integer.parseInt(json_data.getString("PowerYellowCount")));
						editor.putInt(Player.KEY_LEVEL_POWER_BLUE_DAMAGE, Integer.parseInt(json_data.getString("PowerBlueDamage")));
						editor.putInt(Player.KEY_LEVEL_POWER_GREEN_DAMAGE, Integer.parseInt(json_data.getString("PowerGreenDamage")));
						editor.putInt(Player.KEY_LEVEL_POWER_YELLOW_DAMAGE, Integer.parseInt(json_data.getString("PowerYellowDamage")));
						editor.putInt(Player.KEY_LEVEL_POWER_BLUE, Integer.parseInt(json_data.getString("PowerBlueLength")));
						editor.putInt(Player.KEY_LEVEL_POWER_GREEN, Integer.parseInt(json_data.getString("PowerGreenLength")));
						editor.putInt(Player.KEY_LEVEL_POWER_YELLOW, Integer.parseInt(json_data.getString("PowerYellowLength")));
						editor.putInt(Player.KEY_LEVEL_POWER_BLUE_RADIUS, Integer.parseInt(json_data.getString("PowerBlueRadius")));
						editor.putInt(Player.KEY_LEVEL_POWER_GREEN_RADIUS, Integer.parseInt(json_data.getString("PowerGreenRadius")));
						editor.putInt(Player.KEY_LEVEL_POWER_YELLOW_RADIUS, Integer.parseInt(json_data.getString("PowerYellowRadius")));
						
					}
					
					//USERS
					if(json_data.has("TimePlayed")) {
						editor.putInt(Consts.SP_KEY_SYNC_COUNT_USER, Integer.parseInt(json_data.getString("SyncCountUsers")));
						editor.putLong(Consts.SP_KEY_STATS_TIME_PLAYED, Long.parseLong(json_data.getString("TimePlayed")));
						editor.putLong(Consts.SP_KEY_STATS_TOTAL_ZOMBIES_KILLED, Long.parseLong(json_data.getString("ZombiesKilled")));
						editor.putInt(Player.KEY_WAVE, Integer.parseInt(json_data.getString("WavesCompleted")) +1);
						editor.putInt(Consts.SP_KEY_TURRET_SPOT_1, json_data.getInt("TurretSpot1"));
						editor.putInt(Consts.SP_KEY_TURRET_SPOT_2, json_data.getInt("TurretSpot2"));
					}
					
					editor.commit();
					return true;
				} catch(Exception e) {
					Log.e(TAG, "JSON error: " + e.toString());
				}
				
			} else lastError = serverResponse;
		}
		return false;
	}


	public static boolean sendPlaytestData(Context context, boolean isVictorious) {
		ArrayList<NameValuePair> postPairs = new ArrayList<NameValuePair>();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		
		//UUID
		postPairs.add(new BasicNameValuePair("udid", sp.getString(Consts.SP_KEY_USER_ID, "null")));
		
		//CurWave
		postPairs.add(new BasicNameValuePair("CurWave", Integer.toString(PlayerLevels.curLevelWave)));
		
		//CurCoins
		postPairs.add(new BasicNameValuePair("CurCoins", Integer.toString(PlayerLevels.curLevelCoins)));
		
		//CurTokens
		postPairs.add(new BasicNameValuePair("CurTokens", Integer.toString(PlayerLevels.curLevelTokens)));
		
		//DamageTaken
		
		//UpgradesBought
		
		//LevelVictory
		postPairs.add(new BasicNameValuePair("LevelVictory", Boolean.toString(isVictorious)));
		
		//CoinsEarned
		
		
		String response = getResponse("", postPairs);
		
		if(response.contains("done")) return true;
		
		return false;
	}
}
