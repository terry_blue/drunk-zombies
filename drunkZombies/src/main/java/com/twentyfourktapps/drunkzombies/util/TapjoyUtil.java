package com.twentyfourktapps.drunkzombies.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tapjoy.TapjoyConnect;
import com.twentyfourktapps.drunkzombies.Consts;

public class TapjoyUtil {

	public static String uuid = null;
	final public static String APP_ID = "d9fe0e7c-e752-4179-9570-6a701ef10936";
	final public static String SECRET_KEY = "AfcIceN2D86aKA7ZEKCR";
	final public static String CURRENCY_ID_COIN = "09db1263-4b75-4ede-887c-614e4f3add8d";
	final public static String CURRENCY_ID_TOKEN = "d9fe0e7c-e752-4179-9570-6a701ef10936";
	
	public static void connect(Context context) {
		if(uuid == null) {
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
			uuid = sp.getString(Consts.SP_KEY_USER_ID, "Johnny");		
		}
		
		TapjoyConnect.requestTapjoyConnect(context, APP_ID, SECRET_KEY);		
		TapjoyConnect.getTapjoyConnectInstance().setUserID(uuid);
	}
}
