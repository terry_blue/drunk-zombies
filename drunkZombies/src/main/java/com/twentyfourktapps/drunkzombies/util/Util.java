package com.twentyfourktapps.drunkzombies.util;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.RectF;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import com.tapjoy.TapjoyConnect;
import com.twentyfourktapps.drunkzombies.Consts;

public class Util {

	final static String TAG = "Util";
	
	public static String getAppLiKey(Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		StringBuilder sb = new StringBuilder();
		
		sb.append(sp.getString("Marge", "23"));
		sb.append(sp.getString("Bart", "33"));
		sb.append(sp.getString("Homer", "dd"));
		sb.append(sp.getString("Lisa", "s"));
		return sb.toString();
	}
	
	/**
	 * This method converts device specific pixels to device independent pixels.
	 * 
	 * @param px A value in px (pixels) unit. Which we need to convert into db
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent db equivalent to px value
	 */
	public static float convertPixelsToDp(float px, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;

	}
	/**
	 * This method convets dp unit to equivalent device specific value in pixels. 
	 * 
	 * @param dp A value in dp(Device independent pixels) unit. Which we need to convert into pixels
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent Pixels equivalent to dp according to device
	 */
	public static float convertDpToPixel(float dp,Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi/160f);
	    return px;
	}
	
	
	public static PointF getLeftMostPoint(ArrayList<PointF> points) {
		if(points.isEmpty() || points.get(0) == null) return null;
		PointF temp = points.get(0);
		for(PointF pts : points) {
			if(pts != null) {
				if(pts.x < temp.x) temp = pts;
			}
		}
		
		return temp;
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        // Calculate ratios of height and width to requested height and width
        final int heightRatio = Math.round((float) height / (float) reqHeight);
        final int widthRatio = Math.round((float) width / (float) reqWidth);

        // Choose the smallest ratio as inSampleSize value, this will guarantee
        // a final image with both dimensions larger than or equal to the
        // requested height and width.
        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
    }

    return inSampleSize;
}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}

	public static boolean isUserLoggedIn(Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		String uuid = sp.getString(Consts.SP_KEY_USER_ID, "Johnny");
		return !(uuid.equals("Johnny"));
		
	}
	
	public static PointF getRotatedPoint(PointF point, PointF origin, double degs) {
		double diffX = point.x - origin.x;
		double diffY = point.y - origin.y;
		double hyp = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2)); 
		return new PointF((float)Math.sin(Math.toRadians(degs)) * (float)hyp, (float)Math.cos(Math.toRadians(degs) * (float)hyp));
	}
	
	
	public static interface RectAdjuster {
		public float getScaleModifier();
		public RectF getAdjustedDestRect();
	}

}
