package com.twentyfourktapps.drunkzombies.util;

public interface BanterDismissListener {

	public void onBanterDismiss();
}
