package com.twentyfourktapps.drunkzombies;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyFullScreenAdNotifier;
import com.twentyfourktapps.drunkzombies.DialogBanter.Scenario;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.obj.Turret.TurretType;
import com.twentyfourktapps.drunkzombies.util.ServerUtil;
import com.twentyfourktapps.drunkzombies.util.TapjoyUtil;
import com.twentyfourktapps.drunkzombies.util.Util;

public class ActivityEndOfLevel extends FragmentActivity implements TapjoyFullScreenAdNotifier {

	final String TAG = "ActivityEndOfLevel";
	boolean isExitEnabled = false;
	AllowExitHandler allowExitHandler;
	ArrayList<NameValuePair> httpPairs;
	
	boolean isAfterFirstOnPause;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_endoflevel);
		allowExitHandler = new AllowExitHandler(this);		
		isAfterFirstOnPause = false;
		
		if(PlayerLevels.curLevelWave == 1) 
			new DialogBanter().initialize(getResources(), Scenario.CONGRATZ, TurretType.BLUE, TurretType.PLAYER).show(getSupportFragmentManager(), DialogBanter.TAG);
		
		try {
			TapjoyConnect.getTapjoyConnectInstance().getFullScreenAd(this); 
		} catch(Exception e) {
			Log.e(TAG, "Tapjoy Connect error: " + e.toString());
			TapjoyUtil.connect(getApplicationContext());
		}
		
		if(!SoundManager.isInitialized) SoundManager.initialize(ActivityEndOfLevel.this);
		httpPairs = new ArrayList<NameValuePair>();
		setFonts();
		setData();
		new SendDataThread().start();
		animateTextViews();
		LinearLayout rootLayout = (LinearLayout)findViewById(R.id.endoflevel_linearlayout_root);
		rootLayout.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
			
				if(isExitEnabled) {
					Intent intent = new Intent(ActivityEndOfLevel.this, ActivityUpgrades.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}
				return true;
			}
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(isAfterFirstOnPause) {
			DialogFetching dialog = DialogFetching.getInstance();
			dialog.show(getSupportFragmentManager(), DialogFetching.TAG);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		isAfterFirstOnPause = true;		
	}
	
	private void setFonts() {
		((TextView)findViewById(R.id.endoflevel_textview_victorystatus)).setTypeface(StaticResources.getFont(this, Font.FEAST));		
		((TextView)findViewById(R.id.endoflevel_textview_zombieskilled_label)).setTypeface(StaticResources.getFont(this, Font.TREASURE));
		((TextView)findViewById(R.id.endoflevel_textview_zombieskilled)).setTypeface(StaticResources.getFont(this, Font.TREASURE));
		((TextView)findViewById(R.id.endoflevel_textview_bottlesleft_label)).setTypeface(StaticResources.getFont(this, Font.TREASURE));
		((TextView)findViewById(R.id.endoflevel_textview_bottlesleft)).setTypeface(StaticResources.getFont(this, Font.TREASURE));
		((TextView)findViewById(R.id.endoflevel_textview_bonus_label)).setTypeface(StaticResources.getFont(this, Font.TREASURE));
		((TextView)findViewById(R.id.endoflevel_textview_bonus)).setTypeface(StaticResources.getFont(this, Font.TREASURE));		
		
		((TextView)findViewById(R.id.endoflevel_textview_taptocontinue)).setTypeface(StaticResources.getFont(this, Font.KREE));
	}
	
	private void setData() {
		
		boolean isVictorious = getIntent().getExtras().getBoolean(Consts.BundleKey_victoryStatus);
		String statusString = (isVictorious) ? getResources().getString(R.string.wave_complete) : getResources().getString(R.string.you_lose);
		((TextView)findViewById(R.id.endoflevel_textview_victorystatus)).setText(statusString);
		((TextView)findViewById(R.id.endoflevel_textview_zombieskilled)).setText(Integer.toString(getIntent().getExtras().getInt(Consts.BundleKey_zombiesKilled)));
		((TextView)findViewById(R.id.endoflevel_textview_bottlesleft)).setText(Integer.toString(getIntent().getExtras().getInt(Consts.BundleKey_bottlesSaved)));
		((TextView)findViewById(R.id.endoflevel_textview_bonus)).setText(Integer.toString(getIntent().getExtras().getInt(Consts.BundleKey_bonus_amount)));		
	}
	
	private void animateTextViews() {
		long offset = 400;
		TextView zombiesKilledLabel = (TextView)findViewById(R.id.endoflevel_textview_zombieskilled_label);
		TextView bottlesLeftLabel = (TextView)findViewById(R.id.endoflevel_textview_bottlesleft_label);
		TextView bonusLabel = (TextView)findViewById(R.id.endoflevel_textview_bonus_label);
		
		TextView zombiesKilledTV = (TextView)findViewById(R.id.endoflevel_textview_zombieskilled);
		TextView bottlesLeftTV = (TextView)findViewById(R.id.endoflevel_textview_bottlesleft);
		TextView bonusTV = (TextView)findViewById(R.id.endoflevel_textview_bonus);
		
		
		Animation slideRightAnimation = AnimationUtils.loadAnimation(this,R.anim.slide_right);
		Animation slideLeftAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_right);
		
		bonusLabel.startAnimation(slideRightAnimation);
		slideRightAnimation.setStartOffset(offset);
		bottlesLeftLabel.startAnimation(slideRightAnimation);
		slideRightAnimation.setStartOffset(offset * 2);
		zombiesKilledLabel.startAnimation(slideRightAnimation);
		
		bonusTV.startAnimation(slideLeftAnimation);	
		slideLeftAnimation.setStartOffset(offset);
		bottlesLeftTV.startAnimation(slideLeftAnimation);
		slideLeftAnimation.setStartOffset(offset * 2);
		zombiesKilledTV.startAnimation(slideLeftAnimation);
		
		
		slideLeftAnimation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation animation) {
								
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				
			}

			@Override
			public void onAnimationStart(Animation animation) {
				
			}
			
		});
		
	
		
	}
	
	
	@Override
	public void onBackPressed() {
		
	}
	
	
	
	private class SendDataThread extends Thread {
		
		@Override
		public void run() {
			ServerUtil.sendDZdata(ActivityEndOfLevel.this);
		}
	}

	@Override
	public void getFullScreenAdResponse() {
		if(Util.isUserLoggedIn(ActivityEndOfLevel.this)) TapjoyConnect.getTapjoyConnectInstance().showFullScreenAd();
		else TapjoyConnect.getTapjoyConnectInstance().showOffersWithCurrencyID(TapjoyUtil.CURRENCY_ID_TOKEN, false);		
		allowExitHandler.sendMessage(new Message());
		
	}

	@Override
	public void getFullScreenAdResponseFailed(int arg0) {
		allowExitHandler.sendMessage(new Message());
		
	}
	
	private static class AllowExitHandler extends Handler {
		final private WeakReference<ActivityEndOfLevel> TARGET;
		
		AllowExitHandler(ActivityEndOfLevel target) {
			TARGET = new WeakReference<ActivityEndOfLevel>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			ActivityEndOfLevel thisTarget = TARGET.get();
			ViewSwitcher vS = (ViewSwitcher)thisTarget.findViewById(R.id.endoflevel_viewswitcher_taptocontinue);
			vS.showNext();
			thisTarget.isExitEnabled = true;
			SoundManager.playSound(thisTarget.getBaseContext(), SoundManager.KACHING);
		}
	}
}
