package com.twentyfourktapps.drunkzombies;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.twentyfourktapps.drunkzombies.obj.Player;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;

public class DialogDailyRewards extends DialogFragment implements OnClickListener {

	final static String TAG = "DialogDailyRewards";
	public static DialogDailyRewards instance = null;
	
	final static long REWARD_TIME = 86400000l;
	final static String KEY_EXPIRE_TIME = "daily_rewards_expire_time";
	final static String KEY_NEXT_REWARD_AVAILABLE_TIME = "daily_rewards_next_available_time";
	final static String KEY_IS_REWARD_COLLECTED = "daily_rewards_is_collected";
	final static String KEY_CURRENT_REWARD_DAY = "daily_rewards_current_reward_day";
	
	final int[] REWARDS = new int[] { 100, 200, 300, 400, 2 };
	int curRewardDay = 0;
	
	public static DialogDailyRewards getInstance() {
		if(instance == null)
			instance = new DialogDailyRewards();
		return instance;
	}
	
	
	
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
		curRewardDay = sp.getInt(KEY_CURRENT_REWARD_DAY, 0); 		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog_theme);
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_dailyrewards, container);
		
		view.findViewById(R.id.dailyrewrd_button_collect).setOnClickListener(this);
		
		//Set the info text
		((TextView)(view.findViewById(R.id.dailyreward_textview_todays_reward))).setText(getRewardTextInfo());
		
		//Highlight days progress
		RelativeLayout[] days = new RelativeLayout[] {
				(RelativeLayout)(view.findViewById(R.id.dailyreward_relativelayout_day1))
				, (RelativeLayout)(view.findViewById(R.id.dailyreward_relativelayout_day2))
				, (RelativeLayout)(view.findViewById(R.id.dailyreward_relativelayout_day3))
				, (RelativeLayout)(view.findViewById(R.id.dailyreward_relativelayout_day4))
				, (RelativeLayout)(view.findViewById(R.id.dailyreward_relativelayout_day5))
		};
		
		days[curRewardDay].setBackgroundColor(Color.YELLOW);
		
		return view;
	}	

	@Override
	public void onStart() {
		super.onStart();
		getDialog().setOnDismissListener((ActivityUpgrades)getActivity());
	}
	
	public static boolean isRewardAvailable(Context context, long curTime) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor edit = sp.edit();
		
		boolean isCollected = sp.getBoolean(KEY_IS_REWARD_COLLECTED, true);
		long expireTime = sp.getLong(KEY_EXPIRE_TIME, -1);
		long nextRewardTime = sp.getLong(KEY_NEXT_REWARD_AVAILABLE_TIME, -1);	
		
		if(!isCollected) 
			return true;
		
		if(expireTime == -1)
			expireTime = curTime + (REWARD_TIME * 2);
		if(nextRewardTime == -1)
			nextRewardTime = curTime + REWARD_TIME;
		
		if(curTime < expireTime && curTime > nextRewardTime) {
			expireTime = curTime + (REWARD_TIME * 2);
			nextRewardTime = curTime + REWARD_TIME;
			
			edit.putBoolean(KEY_IS_REWARD_COLLECTED, false);
			edit.putLong(KEY_EXPIRE_TIME, expireTime);
			edit.putLong(KEY_NEXT_REWARD_AVAILABLE_TIME, nextRewardTime);
			edit.commit();
			return true;
		} else if (curTime > expireTime) {
			Toast.makeText(context, "You missed your daily reward, come back tomorrow", Toast.LENGTH_SHORT).show();
			expireTime = curTime + (REWARD_TIME * 2);
			nextRewardTime = curTime + REWARD_TIME;
			edit.putInt(KEY_CURRENT_REWARD_DAY, 0);
		}
		
		edit.putLong(KEY_EXPIRE_TIME, expireTime);
		edit.putLong(KEY_NEXT_REWARD_AVAILABLE_TIME, nextRewardTime);
		edit.commit();
		
		
		
		return false;
	}
	
	
	
	public String getRewardTextInfo() {
		String amount = Integer.toString(REWARDS[curRewardDay]);
		String currency = (curRewardDay < 4) ? "DZ Coins" : "DZ Bottlecaps";
		return amount + " " + currency;
	}
	
	public void collectReward() {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		SharedPreferences.Editor edit = sp.edit();
		
		PlayerLevels playerLevels = new PlayerLevels(getActivity());
		String storeKey = (curRewardDay < 4) ? Player.KEY_COINS : Player.KEY_TOKENS; //Only day 5(index 4) awards DZ bottlecaps
		int newCurrencyAmount = PlayerLevels.getLevel(storeKey) + REWARDS[curRewardDay];	
		
		if(curRewardDay < (REWARDS.length - 1)) curRewardDay++;
		else curRewardDay = 0;
		edit.putInt(KEY_CURRENT_REWARD_DAY, curRewardDay);		
		
		edit.putBoolean(KEY_IS_REWARD_COLLECTED, true);
		
		playerLevels.storeNewLevels(storeKey, newCurrencyAmount);
		
		edit.commit();
		dismiss();
	}


	@Override
	public void onClick(View v) {
		
		switch(v.getId()) {
		case R.id.dailyrewrd_button_collect: {
			collectReward();
			break;
		}
		}
		
	}
	
	
	
	
	
	
}
