package com.twentyfourktapps.drunkzombies;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.twentyfourktapps.drunkzombies.obj.Player;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.util.ServerUtil;

public class DialogFetching extends DialogFragment {

	final public static String TAG = "DialogFetching";
	private static DialogFetching instance = null;
	final private int DZ_REWARD = 10;
	
	final private static String SP_KEY_FIRST_TIME = "dialog_fetching_first_time_flag";
	final static String RESPONSE_CODE_USER_EXISTS = "user_already_exists";
	
	FetchHandler fetchHandler;
	ToastHandler toastHandler;	
	
	public static DialogFetching getInstance() {
		if(instance == null)
			instance = new DialogFetching();
		return instance;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		View view = inflater.inflate(R.layout.dialog_fetching, container);
		((TextView)(view.findViewById(R.id.fetchingdialog_textview_title))).setTypeface(StaticResources.getFont(view.getContext(), Font.FEAST));
		
		setCancelable(false);
			
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		getDialog().setCanceledOnTouchOutside(false);
		//setRetainInstance(true);
		fetchHandler = new FetchHandler(this);
		toastHandler = new ToastHandler(this);
		
		new FetchThread().start();
	}
	
	public void showToast(String text) {
		if(getActivity() != null)
			Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
	}
	
	private static class FetchHandler extends Handler {
		private final WeakReference<DialogFetching> TARGET;
		
		FetchHandler(DialogFetching target) {
			TARGET = new WeakReference<DialogFetching>(target);
			
		}
		
		@Override
		public void handleMessage(Message msg) {
			DialogFetching thisTarget = TARGET.get();			
			thisTarget.dismiss();			
		}
	}
	private static class ToastHandler extends Handler {
		private final SoftReference<DialogFetching> TARGET;
		
		ToastHandler(DialogFetching target) {
			TARGET = new SoftReference<DialogFetching>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			final DialogFetching THIS = TARGET.get();
			if(THIS != null) {
				if(msg.obj instanceof String) {
					THIS.showToast((String)msg.obj);
				}
			}
		}
		
	}
	
	private class FetchThread extends Thread {
		
		
		@Override
		public void run() {
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());			
			if(sp.getBoolean(SP_KEY_FIRST_TIME, true)) {
				if(ServerUtil.createDZuser(getActivity())) {
					SharedPreferences.Editor edit = sp.edit();
					edit.putBoolean(SP_KEY_FIRST_TIME, false);
					edit.commit();
					
					//Add bonus for signing up
					PlayerLevels.storeNewLevels(Player.KEY_TOKENS, PlayerLevels.curLevelTokens + DZ_REWARD);
					Toast.makeText(getActivity(), "You have been awarded "+ Integer.toString(DZ_REWARD) + " bonus DZ bottlecaps", Toast.LENGTH_SHORT).show();
					
				} else if(ServerUtil.getLastError().equals(RESPONSE_CODE_USER_EXISTS)) {
					getDZdata();
				} else {
					Log.e(TAG, "error creating user: " + ServerUtil.getLastError());			
					Message msg = new Message();
					msg.obj = "error, please try again later";
					toastHandler.sendMessage(msg);
				}
			} else {
				getDZdata();
			}
			
			fetchHandler.sendMessage(new Message());
		}
		
		
		private void getDZdata() {
			if(!ServerUtil.getDZdata(getDialog().getContext())) {				
				Log.e(TAG, "error getting data: " + ServerUtil.getLastError());
				Message msg = new Message();
				msg.obj = "error fetching data, please try again later";
				toastHandler.sendMessage(msg);					
			}
		}
	}
}
