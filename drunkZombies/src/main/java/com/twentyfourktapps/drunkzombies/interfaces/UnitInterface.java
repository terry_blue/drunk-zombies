package com.twentyfourktapps.drunkzombies.interfaces;

import android.graphics.Canvas;

public interface UnitInterface {
   void update();
   void draw(Canvas canvas);

   void spawn();
   void consume();

}
