package com.twentyfourktapps.drunkzombies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;

public class DialogPause extends DialogFragment implements OnClickListener {

	public final static String TAG = "dialog_pause";
	
	static DialogPause mInstance = null;
	
	public static DialogPause getInstance() {
		if(mInstance == null) 
			mInstance = new DialogPause();
		return mInstance;
	}
	
	@Override
	public void onAttach(Activity activ) {
		super.onAttach(activ);
		setCancelable(false);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		View view = inflater.inflate(R.layout.dialog_pause, container);
		
		view.findViewById(R.id.pause_imagebutton_resume).setOnClickListener(this);
		view.findViewById(R.id.pause_imagebutton_home).setOnClickListener(this);		
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		setFonts();
	}
		

	@Override
	public void onClick(View view) {
		switch(view.getId()) {
		case R.id.pause_imagebutton_resume: {
			SoundManager.playSound(getActivity(), SoundManager.POP2);
			((ActivityGame)getActivity()).resumeGame();
			break;
		}
		
		case R.id.pause_imagebutton_home: {
			SoundManager.playSound(getActivity(), SoundManager.POP2);
			((ActivityGame)getActivity()).game.isGameRunning = false;
			((ActivityGame)getActivity()).game.savePlayerLevels();
			
			Intent intent = new Intent(getActivity(), ActivityUpgrades.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getActivity().startActivity(intent);

			setCancelable(true);
			getDialog().cancel();
            getActivity().finish();
			break;
		}
		}
		
	}
	
	private void setFonts() {
		((TextView)getView().findViewById(R.id.pausedialog_textview_paused)).setTypeface(StaticResources.getFont(getDialog().getContext(), Font.FEAST));
	}
	
}
