package com.twentyfourktapps.drunkzombies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.data.DataBaseHelper;
import com.twentyfourktapps.drunkzombies.obj.Player;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.util.ServerUtil;

public class DialogUpgrade extends DialogFragment implements OnClickListener, OnDismissListener{

	final static String TAG = "DialogUpgrade";
	
	public static enum Currency { COINS, TOKENS }
	Currency curCurrency;
	
	String title, desc, priceString, nextLevel, curLevel;	
	
	String upgradeId;
	Cursor upgradeCursor = null;
	Integer[] costs;
	Float[] levels;
	
	public static DialogUpgrade instance = null;
	
	public static DialogUpgrade getInstance() {
		if(instance == null) 
			instance = new DialogUpgrade();
		
		return instance;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		View view = inflater.inflate(R.layout.dialog_upgrade, container);
		
		view.findViewById(R.id.upgradedialog_button_back).setOnClickListener(this);		
		view.findViewById(R.id.upgradedialog_button_upgrade).setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		setFonts();
	}

	public void setUpgradeId(String resID) {
		upgradeId = resID;				
		setData();
	}
	
	

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.upgradedialog_button_upgrade: {
			String key = Player.getKeysMap().getKey(Integer.parseInt(upgradeId));
			int cost = costs[PlayerLevels.getLevel(key)];			
			
			DataBaseHelper dbH = new DataBaseHelper(getActivity());
			Cursor c = dbH.getUpgradeDataForId(upgradeId);
			if(c.moveToFirst()) curCurrency = (c.getString(c.getColumnIndex(DataBaseHelper.COL_COST_TYPE)).equals("CAP")) ? Currency.TOKENS : Currency.COINS;			
			dbH.close();
			
			String[] currencyKeys = new String[] { Player.KEY_TOKENS, Player.KEY_COINS };
			String[] spentKeys = new String[] { Consts.SP_KEY_STATS_TOKENS_SPENT, Consts.SP_KEY_STATS_COINS_SPENT };
			int[] playerCurrencies = new int[] { PlayerLevels.curLevelTokens, PlayerLevels.curLevelCoins };
			int pos = 0;
			
			switch(curCurrency) {
			case COINS: pos++;			
			case TOKENS: 				
				break;			
			}		
			
			//if(playerCurrencies[pos] >= cost) {
				//buy
				SoundManager.playSound(getActivity(), SoundManager.SHOTGUN_COCK);				
				
				PlayerLevels.storeNewLevels(key, PlayerLevels.getLevel(key) + 1);	
				PlayerLevels.storeNewLevels(currencyKeys[pos], playerCurrencies[pos] - cost);
				
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
				SharedPreferences.Editor editor = sp.edit();
				
				int curUpgradeCount = sp.getInt(Consts.SP_KEY_STATS_UPGRADES_BOUGHT, 0);
				int currencySpent = sp.getInt(spentKeys[pos], 0) + cost;
								
				editor.putInt(Consts.SP_KEY_STATS_UPGRADES_BOUGHT, curUpgradeCount + 1);
				editor.putInt(spentKeys[pos], currencySpent);
				
				editor.commit();
				
			//} else { //not enough cash
				//DialogMoneyPrompt dialog = DialogMoneyPrompt.getInstance();
				//dialog.show(getActivity().getSupportFragmentManager(), DialogMoneyPrompt.TAG);
			//}
			
				
			setData();
		}
		case R.id.upgradedialog_button_back: {
			dismiss();
		}
		}
		
	}	

	
	private void setFonts() {		
		((Button)getView().findViewById(R.id.upgradedialog_button_back)).setTypeface(StaticResources.getFont(getActivity(), Font.FEAST));
		((Button)getView().findViewById(R.id.upgradedialog_button_upgrade)).setTypeface(StaticResources.getFont(getActivity(), Font.FEAST));
		
		
		((TextView)getView().findViewById(R.id.upgradedialog_textview_title)).setTypeface(StaticResources.getFont(getActivity(), Font.FEAST));
		((TextView)getView().findViewById(R.id.upgradedialog_textview_label_currentlevel)).setTypeface(StaticResources.getFont(getActivity(), Font.FEAST));
		((TextView)getView().findViewById(R.id.upgradedialog_textview_label_nextlevel)).setTypeface(StaticResources.getFont(getActivity(), Font.FEAST));		
		
		((TextView)getView().findViewById(R.id.upgradedialog_textview_description)).setTypeface(StaticResources.getFont(getActivity(), Font.TREASURE));
		((TextView)getView().findViewById(R.id.upgradedialog_textview_currentlevel)).setTypeface(StaticResources.getFont(getActivity(), Font.TREASURE));
		((TextView)getView().findViewById(R.id.upgradedialog_textview_nextlevel)).setTypeface(StaticResources.getFont(getActivity(), Font.TREASURE));		
		
		((TextView)getView().findViewById(R.id.upgradedialog_textview_price)).setTypeface(StaticResources.getFont(getActivity(), Font.TREASURE));
	}
	
	
	public void setData() {
		DataBaseHelper dbH = new DataBaseHelper(getActivity());
		upgradeCursor = dbH.getUpgradeDataForId(upgradeId);		
		
		if(upgradeCursor.moveToFirst()) {
			((TextView)getView().findViewById(R.id.upgradedialog_textview_title)).setText(upgradeCursor.getString(upgradeCursor.getColumnIndex(DataBaseHelper.COL_TITLE)));
			((TextView)getView().findViewById(R.id.upgradedialog_textview_description)).setText(upgradeCursor.getString(upgradeCursor.getColumnIndex(DataBaseHelper.COL_DESC)));
			
			TextView curLevelTV = (TextView)getView().findViewById(R.id.upgradedialog_textview_currentlevel);
			TextView nextLevelTV = (TextView)getView().findViewById(R.id.upgradedialog_textview_nextlevel);
			TextView costTV = (TextView)getView().findViewById(R.id.upgradedialog_textview_price);				
				
			//Parse arrays from cursor
			List<String> levelList = Arrays.asList(upgradeCursor.getString(upgradeCursor.getColumnIndex(DataBaseHelper.COL_LEVELS)).split("\\s*,\\s*"));
			List<String> costList = Arrays.asList(upgradeCursor.getString(upgradeCursor.getColumnIndex(DataBaseHelper.COL_COST)).split("\\s*,\\s*"));						
			ArrayList<Integer> costArr = new ArrayList<Integer>();
			ArrayList<Float> levelArr = new ArrayList<Float>();
			for(String level : levelList) levelArr.add(Float.parseFloat(level));
			for(String cost : costList) costArr.add(Integer.parseInt(cost));
			levels = levelArr.toArray(new Float[levelArr.size()]);
			costs = costArr.toArray(new Integer[costArr.size()]);
				
			int _id = Integer.parseInt(upgradeId);
			String key = Player.getKeysMap().getKey(_id);			
			curLevelTV.setText(Float.toString(levels[PlayerLevels.getLevel(key)]));
				
			String nextLvlTxt = (PlayerLevels.getLevel(key) < levels.length - 2) ? Float.toString(levels[PlayerLevels.getLevel(key) + 1]) : getActivity().getResources().getString(R.string.max_level);
			nextLevelTV.setText(nextLvlTxt);
				
			costTV.setText(Integer.toString(costs[PlayerLevels.getLevel(key)]));
			Drawable drawable;
			boolean isCurrencyCoin = upgradeCursor.getString(upgradeCursor.getColumnIndex(DataBaseHelper.COL_COST_TYPE)).equals("COIN");
			if(isCurrencyCoin) {
				curCurrency = Currency.COINS;
				drawable = getActivity().getResources().getDrawable(R.drawable.coin);
				costTV.setTextAppearance(getActivity(), R.style.upgradedialog_pricetext_coins);
			} else {
				curCurrency = Currency.TOKENS;
				drawable = getActivity().getResources().getDrawable(R.drawable.coin_dz);
				costTV.setTextAppearance(getActivity(), R.style.upgradedialog_pricetext_tokens);
			}
			drawable.setBounds(0,0,60,60);
			costTV.setCompoundDrawables(drawable, null, null, null);
		}
		
		dbH.close();
	}
		

	@Override
	public void onDismiss(DialogInterface arg0) {			
		sendData();
	}
	
	public void sendData() {
		new SendDataThread().start();
	}
	private class SendDataThread extends Thread {
		
		@Override
		public void run() {
			ServerUtil.sendDZdata(getActivity());
		}
	}
}
