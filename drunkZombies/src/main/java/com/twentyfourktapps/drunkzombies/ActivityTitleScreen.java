package com.twentyfourktapps.drunkzombies;

import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.util.TapjoyUtil;
import com.twentyfourktapps.drunkzombies.util.Util;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ActivityTitleScreen extends FragmentActivity implements OnClickListener {
	
	final String TAG = "ActivityTitleScreen";
	//public static PlusClient googlePlusClient = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_titlescreen);
		
		TapjoyUtil.connect(getApplicationContext());	
		
		AppLi();
			
		if(!SoundManager.isInitialized) SoundManager.initialize(getApplicationContext());		
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		setListeners();
		setFonts();
		
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor edit = sp.edit();
		edit.putInt(DialogRateMe.SP_KEY_RATE_ME_IS_TO_SHOW_TEST_COUNT, sp.getInt(DialogRateMe.SP_KEY_RATE_ME_IS_TO_SHOW_TEST_COUNT, 0) + 1);
		edit.commit();		
	}
	
	private void AppLi() {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ActivityTitleScreen.this);
		SharedPreferences.Editor editor = sp.edit();
		editor.putString("Bart", "OSrfVcPaU0XGxwVh7duuH0pLQ/T9RIwNeoTZ2cXr/bQhCg01VHdBteAKiQgnfPC6D7t5gP89JuHaowpJjKzzIwzw0S6vXjJAaGd/FK82AthVNreK2y091u6zdl");
		editor.putString("Homer", "RZ7sVBoBGZM/Ms0nCuv2ZTkNFmxcfAsD8o6k7TiN8tpicF18nvpzKer9kxFzVNBETRsPP4NUskxkMld6ZMhaV3xG4u9ZGeQ5ysgV9mgHh4Jh3kffWaW86XA3Ro");
		editor.commit();
	}

	
	
	
	
	
	
		
	
	
	@Override
	public void onClick(View view) {
		
		
		Intent intent = null;
		
		switch(view.getId()) {
		case R.id.title_button_start: {
			SoundManager.playSound(getApplicationContext(), SoundManager.DRUNK_ZOMBIES);	
			intent = new Intent(ActivityTitleScreen.this, ActivityUpgrades.class);
			break;
		}
		case R.id.title_button_stats: {
			SoundManager.playSound(getApplicationContext(), SoundManager.POP2);	
			intent = new Intent(ActivityTitleScreen.this, ActivityStatusScreen.class);
			break;
		}
		case R.id.title_button_share: {
			SoundManager.playSound(getApplicationContext(), SoundManager.POP2);	
			intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, "http://www.24ktstudios.com/makeitrain");
			intent = Intent.createChooser(intent, getResources().getString(R.string.share_via));
			break;
		}
		}
		
		if(intent != null) startActivity(intent);
	}



	private void setListeners() {
		findViewById(R.id.title_button_start).setOnClickListener(this);
		findViewById(R.id.title_button_rate).setOnClickListener(this);
		findViewById(R.id.title_button_stats).setOnClickListener(this);
		findViewById(R.id.title_button_share).setOnClickListener(this);
	}
	
	private void setFonts() {
		((Button)findViewById(R.id.title_button_start)).setTypeface(StaticResources.getFont(this, Font.KREEW));
		((Button)findViewById(R.id.title_button_rate)).setTypeface(StaticResources.getFont(this, Font.KREEW));
		((Button)findViewById(R.id.title_button_stats)).setTypeface(StaticResources.getFont(this, Font.KREEW));
		((Button)findViewById(R.id.title_button_share)).setTypeface(StaticResources.getFont(this, Font.KREEW));
	}




	
	
	
}
