package com.twentyfourktapps.drunkzombies;

import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.ArrayList;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.twentyfourktapps.drunkzombies.DialogBanter.Scenario;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.obj.Turret.TurretType;
import com.twentyfourktapps.drunkzombies.obj.TurretSelector;
import com.twentyfourktapps.drunkzombies.obj.UpgradeButton;
import com.twentyfourktapps.drunkzombies.units.Bullet;
import com.twentyfourktapps.drunkzombies.util.BanterDismissListener;

public class ActivityUpgrades extends FragmentActivity implements OnClickListener, OnDismissListener, BanterDismissListener {

	final String TAG = "ActivityUpgrades";
	public boolean isInFront;
	ArrayList<UpgradeButton> allUpgradeButtons;
	UpgradeButton selectedUpgradeButton;
	String selectedUpgrade;
	int selectedUpgradeCost;
	SharedPreferences sp = null;
	
	ArrayList<UpgradeButton> bulletUpgradeButtons, turretUpgradeButtons, powerUpgradeButtons, bottleUpgradeButtons;
	
	DailyRewardHandler dailyRewardHandler;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upgradescreen);	
		sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		attachButtonListeners();		
		setHeaderLevels();
		
		dailyRewardHandler = new DailyRewardHandler(this);
		
		new DailyRewardThread().start();
		if(!SoundManager.isInitialized) SoundManager.initialize(getApplicationContext());
		
		
		checkForNewBanter();
	}
	@Override
	public void onPause() {
		super.onPause();
		isInFront = false;
	}
	@Override
	public void onResume() {
		super.onResume();
		setHeaderLevels();
		setFonts();
		isInFront = true;	
	}

	public void setHeaderLevels() {
		refresh();
		((TextView)findViewById(R.id.upgrades_textview_curwave)).setText(Integer.toString(PlayerLevels.curLevelWave));
		((TextView)findViewById(R.id.upgrades_textview_curcoins)).setText(Integer.toString(PlayerLevels.curLevelCoins));
		((TextView)findViewById(R.id.upgrades_textview_curtokens)).setText(Integer.toString(PlayerLevels.curLevelTokens));		
	}
	
	private void attachButtonListeners() {
		initializeTurretSelectors();
		
		
		findViewById(R.id.upgrades_button_start).setOnClickListener(this);
		findViewById(R.id.upgrades_button_bullets).setOnClickListener(this);
		findViewById(R.id.upgrades_button_turrets).setOnClickListener(this);
		findViewById(R.id.upgrades_button_bottles).setOnClickListener(this);
		findViewById(R.id.upgrades_button_powers).setOnClickListener(this);		
		findViewById(R.id.upgrades_button_guns).setOnClickListener(this);			
		findViewById(R.id.upgrades_linearlayout_coincount).setOnClickListener(this);		
		findViewById(R.id.upgrades_linearlayout_tokencount).setOnClickListener(this);
		
		//Upgrade Buttons
		bulletUpgradeButtons = new ArrayList<UpgradeButton>();
		powerUpgradeButtons = new ArrayList<UpgradeButton>();
		turretUpgradeButtons = new ArrayList<UpgradeButton>();
		bottleUpgradeButtons = new ArrayList<UpgradeButton>();
		
		allUpgradeButtons = new ArrayList<UpgradeButton>();
		bulletUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_strength));		
		bulletUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_agility));
		bulletUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_critical));
		bulletUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_poison));
		bulletUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_kick));
		bulletUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_multishot));
		
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretyellow));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretblue));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretgreen));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretyellow_agility));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretblue_agility));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretgreen_agility));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretgreen_damage));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretblue_damage));
		turretUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_turretyellow_damage));
		
		bottleUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_bottlecount_red));
		bottleUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_bottlecount_orange));
		bottleUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_bottlecount_green));
		bottleUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_bottlequality));
		bottleUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_bottlethickness));
		
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_yellow));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_blue));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_green));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_yellow_powerlevel));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_blue_powerlevel));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_green_powerlevel));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_yellow_radius));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_blue_radius));
		powerUpgradeButtons.add((UpgradeButton)findViewById(R.id.upgrades_upgradebutton_powers_green_radius));
		
		allUpgradeButtons.addAll(bulletUpgradeButtons);
		allUpgradeButtons.addAll(turretUpgradeButtons);
		allUpgradeButtons.addAll(bottleUpgradeButtons);
		allUpgradeButtons.addAll(powerUpgradeButtons);		
		
		for(UpgradeButton button : allUpgradeButtons) button.setOnClickListener(this);
		
	}
	private void setFonts() {		
		//Header Fonts
		((TextView)findViewById(R.id.upgrades_textview_label_wave)).setTypeface(StaticResources.getFont(this, Font.KREE));
		((TextView)findViewById(R.id.upgrades_textview_curwave)).setTypeface(StaticResources.getFont(this, Font.KREE));
		((TextView)findViewById(R.id.upgrades_textview_curcoins)).setTypeface(StaticResources.getFont(this, Font.KREE));
		((TextView)findViewById(R.id.upgrades_textview_curtokens)).setTypeface(StaticResources.getFont(this, Font.KREE));
		
		//Tabbed Buttons
		((Button)findViewById(R.id.upgrades_button_start)).setTypeface(StaticResources.getFont(this, Font.FEAST));
		((Button)findViewById(R.id.upgrades_button_bullets)).setTypeface(StaticResources.getFont(this, Font.KREEW));
		((Button)findViewById(R.id.upgrades_button_turrets)).setTypeface(StaticResources.getFont(this, Font.KREEW));
		((Button)findViewById(R.id.upgrades_button_bottles)).setTypeface(StaticResources.getFont(this, Font.KREEW));
		((Button)findViewById(R.id.upgrades_button_powers)).setTypeface(StaticResources.getFont(this, Font.KREEW));
		((Button)findViewById(R.id.upgrades_button_guns)).setTypeface(StaticResources.getFont(this, Font.KREEW));		
	}

	private void checkForNewBanter() {
		Context cxt = ActivityUpgrades.this;
		if(PlayerLevels.curLevelWave == 1 
		&& !DialogBanter.hasScenarioCompleted(cxt, Scenario.STORE_SCREEN)) {
			new DialogBanter().initialize(getResources(), Scenario.STORE_SCREEN, TurretType.PLAYER, TurretType.GREEN).show(getSupportFragmentManager(), "dialog_banter");
			return;
		}
		
		if(PlayerLevels.curLevelWave == 1
		&& !DialogBanter.hasScenarioCompleted(cxt, Scenario.CURRENCIES)
		&& DialogBanter.hasScenarioCompleted(cxt, Scenario.STORE_SCREEN)) {
			new DialogBanter().initialize(getResources(), Scenario.CURRENCIES, TurretType.PLAYER, TurretType.GREEN).show(getSupportFragmentManager(), "dialog_banter");
			return;
		}
		
		if(PlayerLevels.curLevelWave == 1
		&& !DialogBanter.hasScenarioCompleted(cxt, Scenario.FIGHT)
		&& DialogBanter.hasScenarioCompleted(cxt, Scenario.CURRENCIES)) {
			new DialogBanter().initialize(getResources(), Scenario.FIGHT, TurretType.PLAYER, TurretType.GREEN).show(getSupportFragmentManager(), "dialog_banter");
			return;
		}
		
		
		if(PlayerLevels.curLevelWave == 2
		&& !DialogBanter.hasScenarioCompleted(cxt, Scenario.DZ_CAPS)) {
			new DialogBanter().initialize(getResources(), Scenario.DZ_CAPS, TurretType.PLAYER, TurretType.GREEN).show(getSupportFragmentManager(), "dialog_banter");
			return;
		}
		
		if(PlayerLevels.curLevelWave == 2
		&& DialogBanter.hasScenarioCompleted(cxt, Scenario.DZ_CAPS)
		&& !DialogBanter.hasScenarioCompleted(cxt, Scenario.UPGRADES)) {
			new DialogBanter().initialize(getResources(), Scenario.UPGRADES, TurretType.YELLOW, TurretType.BLUE).show(getSupportFragmentManager(), "dialog_banter");
			return;
		}
		
		if(PlayerLevels.curLevelWave == 2
		&& DialogBanter.hasScenarioCompleted(cxt, Scenario.UPGRADES)
		&& !DialogBanter.hasScenarioCompleted(cxt, Scenario.POWERS)) {
			new DialogBanter().initialize(getResources(), Scenario.POWERS, TurretType.BLUE, TurretType.YELLOW).show(getSupportFragmentManager(), "dialog_banter");
			return;
		}
			
	}
	
	@Override
	public void onClick(View v) {
		final ViewFlipper contentFlipper = (ViewFlipper)findViewById(R.id.upgrades_viewflipper_content);
		int flipperPos = 0;
		
		switch(v.getId()) {
		case R.id.upgrades_linearlayout_coincount:
		case R.id.upgrades_linearlayout_tokencount: {
			startActivity(new Intent(ActivityUpgrades.this, ActivityPurchase.class));
			break;
		}
		case R.id.upgrades_button_start: {
			SoundManager.playSound(getApplicationContext(), SoundManager.POP2);						
			startActivity(new Intent(ActivityUpgrades.this, ActivityGame.class));			
			break;
		}
		
		//View Flipper buttons		
		case R.id.upgrades_button_guns: 
			flipperPos++;
			if(!DialogBanter.hasScenarioCompleted(ActivityUpgrades.this, Scenario.TURRETS)) {
				new DialogBanter().initialize(getResources(), Scenario.TURRETS, TurretType.BLUE, TurretType.YELLOW).show(getSupportFragmentManager(), "dialog_banter");
			}
		case R.id.upgrades_button_powers: flipperPos++;
		case R.id.upgrades_button_bottles: flipperPos++;
		case R.id.upgrades_button_turrets: flipperPos++;
		case R.id.upgrades_button_bullets: {
			SoundManager.playSound(getApplicationContext(), SoundManager.POP2);
			contentFlipper.setDisplayedChild(flipperPos);
			break;
		}
		
		case R.id.upgrades_turretselector_blue: 
		case R.id.upgrades_turretselector_green: 
		case R.id.upgrades_turretselector_yellow: 
			TurretSelector tS = (TurretSelector)findViewById(v.getId());
			if(!tS.isLocked) {
				if(tS.curSpot > 0) {
					tS.deSelect();
				} else {
					tS.select(getAvailableTurretSlot());
				}
			} else {
				DialogTurretDescription dialog = DialogTurretDescription.getInstance();
				dialog.initialize(tS.getTurretType());				
				dialog.show(getSupportFragmentManager(), DialogTurretDescription.TAG);	
				
			}
			break;
		
		case R.id.upgrades_upgradebutton_strength:		
		case R.id.upgrades_upgradebutton_agility:
		case R.id.upgrades_upgradebutton_critical:
		case R.id.upgrades_upgradebutton_poison:
		case R.id.upgrades_upgradebutton_kick:
		case R.id.upgrades_upgradebutton_multishot:			
		case R.id.upgrades_upgradebutton_turretyellow:
		case R.id.upgrades_upgradebutton_turretblue:
		case R.id.upgrades_upgradebutton_turretgreen:
		case R.id.upgrades_upgradebutton_turretyellow_agility:
		case R.id.upgrades_upgradebutton_turretblue_agility:
		case R.id.upgrades_upgradebutton_turretgreen_agility:
		case R.id.upgrades_upgradebutton_turretgreen_damage:
		case R.id.upgrades_upgradebutton_turretblue_damage:
		case R.id.upgrades_upgradebutton_turretyellow_damage:			
		case R.id.upgrades_upgradebutton_bottlecount_red:
		case R.id.upgrades_upgradebutton_bottlecount_orange:
		case R.id.upgrades_upgradebutton_bottlecount_green:
		case R.id.upgrades_upgradebutton_bottlequality:
		case R.id.upgrades_upgradebutton_bottlethickness:			
		case R.id.upgrades_upgradebutton_powers_yellow:
		case R.id.upgrades_upgradebutton_powers_blue:
		case R.id.upgrades_upgradebutton_powers_green:
		case R.id.upgrades_upgradebutton_powers_yellow_powerlevel:
		case R.id.upgrades_upgradebutton_powers_blue_powerlevel:
		case R.id.upgrades_upgradebutton_powers_green_powerlevel:
		case R.id.upgrades_upgradebutton_powers_yellow_radius:
		case R.id.upgrades_upgradebutton_powers_blue_radius:
		case R.id.upgrades_upgradebutton_powers_green_radius: {
			if(((UpgradeButton)findViewById(v.getId())).checkAvailability(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()))) {
				DialogUpgrade dialog = DialogUpgrade.getInstance();							
				dialog.show(getSupportFragmentManager(), DialogUpgrade.TAG);
				getSupportFragmentManager().executePendingTransactions();
				dialog.setUpgradeId((String)v.getTag());	
				dialog.getDialog().setOnDismissListener(this);
			}
		}
			
		}///SWITCH
			
	}



	public void refresh() {
		sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		PlayerLevels.init(ActivityUpgrades.this);
		for(UpgradeButton button : allUpgradeButtons) {
			button.refreshData(sp);
		}
	}

	private int getAvailableTurretSlot() {
		if(sp == null)
			sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		int spot1 = sp.getInt(Consts.SP_KEY_TURRET_SPOT_1, -1);
		int spot2 = sp.getInt(Consts.SP_KEY_TURRET_SPOT_2, -1);
		if(spot1 != Bullet.GREEN && spot1 != Bullet.BLUE && spot1 != Bullet.YELLOW) return 1;
		else if(spot2 != Bullet.GREEN && spot2 != Bullet.BLUE && spot2 != Bullet.YELLOW) return 2;
		
		return 0;
	}

	public void setAvailableTurretIndicators() {
		if(sp == null)
			sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		TextView topIndicator = (TextView)findViewById(R.id.upgrades_textview_top_turret_available);
		TextView bottomIndicator = (TextView)findViewById(R.id.upgrades_textview_bottom_turret_available);
		
		if(sp.getInt(Consts.SP_KEY_TURRET_SPOT_1, -1) > -1) 
			topIndicator.setVisibility(View.INVISIBLE);
		else 
			topIndicator.setVisibility(View.VISIBLE);		
		
		if(sp.getInt(Consts.SP_KEY_TURRET_SPOT_2, -1) > -1) 
			bottomIndicator.setVisibility(View.INVISIBLE);
		else 
			bottomIndicator.setVisibility(View.VISIBLE);
		
	}


	
	
	public void initializeTurretSelectors() {
		((TurretSelector)findViewById(R.id.upgrades_turretselector_blue)).initialise(Bullet.BLUE, this, sp);
		((TurretSelector)findViewById(R.id.upgrades_turretselector_green)).initialise(Bullet.GREEN, this, sp);
		((TurretSelector)findViewById(R.id.upgrades_turretselector_yellow)).initialise(Bullet.YELLOW, this, sp);	
	}
	
	public void checkForDailyReward(long curTime) {
		if(DialogDailyRewards.isRewardAvailable(ActivityUpgrades.this, curTime)) { 
			DialogDailyRewards dialog = DialogDailyRewards.getInstance();			
			dialog.show(getSupportFragmentManager(), DialogDailyRewards.TAG);
		}
		
	}
	private class DailyRewardThread extends Thread {
		
		@Override
		public void run() {
			String[] servers = new String[] {
					"time-a.nist.gov"
					, "time-b.nist.gov"
					, "time-c.nist.gov"
					, "time-d.nist.gov"
					, "nist1-nj.ustiming.org"
					, "nist1-ny2.ustiming.org"
					, "nist1-pa.ustiming.org"					
			};
			
			for(String server : servers) {
				try {			
					NTPUDPClient timeClient = new NTPUDPClient();
			        InetAddress inetAddress = InetAddress.getByName(server);
			        TimeInfo timeInfo = timeClient.getTime(inetAddress);
			        long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
			        
			        Message msg = new Message();
			        msg.obj = returnTime;
			        dailyRewardHandler.sendMessage(msg);
			        break;
				} catch(Exception e) { Log.e(TAG, "getTime error: " + e.toString()); }
			}
		}
	}
	private static class DailyRewardHandler extends Handler {
		final WeakReference<ActivityUpgrades> TARGET;
		
		DailyRewardHandler(ActivityUpgrades target) {
			TARGET = new WeakReference<ActivityUpgrades>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			final ActivityUpgrades THIS = TARGET.get();
			
			if(THIS.isInFront && msg.obj instanceof Long) 
				THIS.checkForDailyReward((Long)msg.obj);			
		}
	}
	
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		setHeaderLevels();		
		
		if(dialog instanceof DialogTurretDescription) {
			initializeTurretSelectors();
		}
	}
	
	@Override
	public void onBanterDismiss() {
		checkForNewBanter();
		
	}
}
