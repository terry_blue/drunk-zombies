package com.twentyfourktapps.drunkzombies;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.obj.BottleView;
import com.twentyfourktapps.drunkzombies.obj.BottleView.Bottle;
import com.twentyfourktapps.drunkzombies.obj.Player;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.PowerIcon;
import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.obj.Turret;
import com.twentyfourktapps.drunkzombies.obj.ZombieController;
import com.twentyfourktapps.drunkzombies.units.AbstractUnit;
import com.twentyfourktapps.drunkzombies.units.AbstractView;
import com.twentyfourktapps.drunkzombies.units.Bullet;
import com.twentyfourktapps.drunkzombies.units.BulletGroup;
import com.twentyfourktapps.drunkzombies.units.BulletMain;
import com.twentyfourktapps.drunkzombies.units.PowersMain;
import com.twentyfourktapps.drunkzombies.units.Zombie;
import com.twentyfourktapps.drunkzombies.units.ZombieGroup;
import com.twentyfourktapps.drunkzombies.units.ZombieMain;

public class FragmentGame extends Fragment implements OnTouchListener {

	final static String TAG = "FragmentGame";
	
	public static float zombieConsumePoint = 0.0f;
	public static float zombieSpawnPoint = 1000f;
	public static int fragmentHeight = 2;
	public static int fragmentWidth = 2;
	
	boolean isGameRunning, isPaused;
	boolean isBossLevel, isBossLaunched;
	//LinearLayout playerLayout, turretTopLayout, turretBottomLayout;
	
	int waveCoinsEarned = 0;
	int zombiesKilled = 0;
	int curCoins, curTokens, curWave;
	final int TOKENS_FOR_WIN = 1;
	//InitializedHandler initializedHandler;
	GameLoopThreadHandler loopThreadHandler;
	EndLevelHandler endLevelHandler;
	FragmentTimer timerFragment;
	BottleView bottles;
	
	
	BulletMain bulletsPlayer, bulletsTurrets;
	ArrayList<AbstractUnit> bulletSpawners;
	ArrayList<PowerIcon> powerIcons;
	ZombieMain zombieMain;
	PowersMain powerMain;
	
	Player player;
	Turret turretTop, turretBottom;
	ArrayList<Player> characters;
	
	ArrayList<AbstractView> theViews;
			
	PointF touchPoint;
	
	long levelStartTime;
	long tempElapsedTime;
	
	Random rand;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {	
		View view = inflater.inflate(R.layout.fragment_game, container);
		isGameRunning = false;
		isPaused = false;
		touchPoint = new PointF();
		
		Context context = view.getContext();
		((TextView)view.findViewById(R.id.gamefragment_textview_wavelabel)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((TextView)view.findViewById(R.id.gamefragment_textview_wavecount)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((TextView)view.findViewById(R.id.gamefragment_textview_coincount)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((TextView)view.findViewById(R.id.gamefragment_textview_tokencount)).setTypeface(StaticResources.getFont(context, Font.TREASURE));


		
		return view;
	}
	
	public void initialize() {				
		player = (Player)getView().findViewById(R.id.gamefragment_playerview);
		turretTop = (Turret)getView().findViewById(R.id.gamefragment_turret_top);
		turretBottom = (Turret)getView().findViewById(R.id.gamefragment_turret_bottom);
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getView().getContext());
		int topTurr = sp.getInt(Consts.SP_KEY_TURRET_SPOT_1, -1);
		int bottomTurr = sp.getInt(Consts.SP_KEY_TURRET_SPOT_2, -1);		
		player.initialize(Bullet.PLAYER); turretTop.initialize(topTurr); turretBottom.initialize(bottomTurr);
        timerFragment = (FragmentTimer)(((ActivityGame) getActivity()).game.getChildFragmentManager().findFragmentById(R.id.gamefragment_timer));
		if(!SoundManager.isInitialized) SoundManager.initialize(getActivity());
		if(!StaticResources.isInitialized) StaticResources.initialize(getActivity());
		if(!PlayerLevels.isInitialized) PlayerLevels.init(getActivity());
		if(!ZombieController.isInitialized) ZombieController.initialize(getActivity());

		isBossLevel = true;
		isBossLaunched = false;
		
		characters = new ArrayList<>();
		characters.add(player);
		characters.add(turretTop);
		characters.add(turretBottom);
		
		theViews = new ArrayList<>();
		bulletsPlayer = (BulletMain)getView().findViewById(R.id.gamefragment_bullets_player);
		theViews.add(bulletsPlayer);
		
		bulletsTurrets = (BulletMain)getView().findViewById(R.id.gamefragment_bullets_turrets);
		theViews.add(bulletsTurrets);
		
		zombieMain = (ZombieMain)getView().findViewById(R.id.gamefragment_zombies);
		theViews.add(zombieMain);		
		
		powerMain = (PowersMain)getView().findViewById(R.id.gamefragment_powers);
		theViews.add(powerMain);
			
		bottles = (BottleView)getView().findViewById(R.id.gamefragment_bottleview);		
		
		
		
		//Load player properties
		curCoins = PlayerLevels.curLevelCoins;
		curTokens = PlayerLevels.curLevelTokens;
		curWave = PlayerLevels.curLevelWave;		
		

		
		powerIcons = new ArrayList<>();
		powerIcons.add(((PowerIcon)getView().findViewById(R.id.gamefragment_powericon_blue)));
		powerIcons.add(((PowerIcon)getView().findViewById(R.id.gamefragment_powericon_green)));
		powerIcons.add(((PowerIcon)getView().findViewById(R.id.gamefragment_powericon_yellow)));
		
		for(PowerIcon icon : powerIcons) 
			icon.initialize(new SoftReference<>(powerMain));
	
		rand = new Random();	
		
		
		
		getView().findViewById(R.id.gamefragment_linearlayout_turrets).post(scaleTurretsRunnable);
	}
	
	public void update() {		
		timerFragment.update();
		rotateTurrets();
		turretTop.update();
		turretBottom.update();
		bottles.update();
		
		for(AbstractView aV : theViews) 
			aV.update();
		
		for(PowerIcon icon : powerIcons)
			icon.update();
		
		bulletSpawners = new ArrayList<>();
		bulletSpawners.addAll(bulletsPlayer.members);
		bulletSpawners.addAll(bulletsTurrets.members);
		bulletSpawners.addAll(powerMain.members);
		
		zombieConsumePoint = (player.getWidth() + bottles.getWidth()) + 10;
		zombieSpawnPoint = getView().getWidth() + 20;
				
		if(zombieMain.members.size() < ZombieController.getMaxSpawners() && !timerFragment.isExpired && ZombieController.spawnChance())			
			zombieMain.spawnZombies(false);
				
		for(AbstractUnit zombieSpawner : zombieMain.members) {
			for(AbstractUnit zombieGroup : zombieSpawner.getMembers()) {
				for(AbstractUnit bulletSpawner : bulletSpawners) {
					for(AbstractUnit bulletGroup : bulletSpawner.getMembers()) {
						if(bulletGroup instanceof BulletGroup && zombieGroup instanceof ZombieGroup) {
							if(RectF.intersects(((BulletGroup)bulletGroup).getGroupRect(), ((ZombieGroup)zombieGroup).getGroupRect())) {
								for(AbstractUnit bul : bulletGroup.getMembers()) {
									for(AbstractUnit zom : zombieGroup.getMembers()) {
										if(bul instanceof Bullet && zom instanceof Zombie) {
											Bullet bullet = (Bullet)bul; Zombie zombie = (Zombie)zom;
											if(RectF.intersects(bullet.destRect, zombie.destRect)) {												
												if(bullet.hit()) {
													boolean isCriticalHit = false;
													if(bullet.unitSelection == Bullet.PLAYER && rand.nextInt(100) < PlayerLevels.getCriticalChance())
														isCriticalHit = true;
													 
													if(zombie.hit(bullet.unitSelection, PlayerLevels.getBulletDamage(bullet.unitSelection), isCriticalHit, bullet.isHarmful)) {
														addToCoins(Consts.ZOMBIE_COIN_REWARD[zombie.unitSelection]);
														if(++zombiesKilled % 30 == 0) {
															SoundManager.playYea();
														}
														
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		
		for(Player charac : characters) {
			if(charac.toFire())
				fire(charac.curTurretColor, charac.getFiringPoint(), touchPoint, charac.rotationDeg);
		}		
		
		bottles.totalBottleHp -= zombieMain.getZombieDrain();
		if(bottles.totalBottleHp <= 0) endLevel(false);
		
		((TextView)getView().findViewById(R.id.gamefragment_textview_hpleft)).setText(Double.toString(bottles.totalBottleHp));
		
		
	}
	public void start() {
		setHeaderLevels();

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		int wavesAttempted = sp.getInt(Consts.SP_KEY_STATS_WAVES_ATTEMPTED, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(Consts.SP_KEY_STATS_WAVES_ATTEMPTED, wavesAttempted + 1);
		editor.apply();

		levelStartTime = System.currentTimeMillis();
		tempElapsedTime = 0;
		isGameRunning = true;
		isPaused = false;

		timerFragment.start();

        if(getActivity() instanceof ActivityGame) {
            loopThreadHandler = new GameLoopThreadHandler(this);
            new GameLoopThread().start();
            ((ActivityGame) getActivity()).checkForBanter();
        }

        player.invalidate();
	}
	private void setHeaderLevels() {
		((TextView)getView().findViewById(R.id.gamefragment_textview_wavecount)).setText(Integer.toString(PlayerLevels.curLevelWave));
		((TextView)getView().findViewById(R.id.gamefragment_textview_coincount)).setText(Integer.toString(PlayerLevels.curLevelCoins));
		((TextView)getView().findViewById(R.id.gamefragment_textview_tokencount)).setText(Integer.toString(PlayerLevels.curLevelTokens));
	}
	
	
	boolean hasLevelEnded = false;
	public void endLevel(boolean isWin)  {
		if(!hasLevelEnded) {
			if(isWin)  {
				SoundManager.playWooHoo();
				SoundManager.playSound(SoundManager.WIN_SOUND);
			}
			else  {
				SoundManager.playSound(SoundManager.IS_THAT_ALL);
				SoundManager.playSound(SoundManager.LOSE_SOUND);
			}
			endLevelHandler = new EndLevelHandler(this);
			
			hasLevelEnded = true;
			isPaused = true;
			isGameRunning = false;
			
			if(isWin) {
				curWave++;
				curTokens += TOKENS_FOR_WIN;
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
				SharedPreferences.Editor editor = sp.edit();
				editor.putInt(Consts.SP_KEY_STATS_TOKENS_EANRED, sp.getInt(Consts.SP_KEY_STATS_TOKENS_EANRED, 0) + TOKENS_FOR_WIN);
				editor.apply();
				
			}
			
			EndLevelThread endLevelThread = new EndLevelThread(isWin);
			endLevelThread.start();
			
			
			savePlayerLevels();			
		}
	}
	private static class EndLevelHandler extends Handler {
		private final WeakReference<FragmentGame> TARGET;
		EndLevelHandler(FragmentGame target) {
			TARGET = new WeakReference<>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			FragmentGame thisTarget = TARGET.get();
			
			int bottleCount = 0;
			for(Bottle bottle : thisTarget.bottles.allBottles) {
				if(bottle.isLive)
                    bottleCount++;
			}
			
			boolean isWin = (Boolean)msg.obj;
			
			int bonus = (Consts.BONUS_PER_WAVE * PlayerLevels.curLevelWave) + (Consts.BONUS_PER_ZOMBIE * thisTarget.zombiesKilled) + (Consts.BONUS_PER_BOTTLE * bottleCount);
			thisTarget.curCoins += bonus;
			Intent intent = new Intent(thisTarget.getView().getContext(), ActivityEndOfLevel.class);
			intent.putExtra(Consts.BundleKey_zombiesKilled, thisTarget.zombiesKilled);
			intent.putExtra(Consts.BundleKey_victoryStatus, isWin);
			intent.putExtra(Consts.BundleKey_bonus_amount, bonus);
			intent.putExtra(Consts.BundleKey_bottlesSaved, bottleCount);
			thisTarget.getActivity().startActivity(intent);
			
			thisTarget.getActivity().finish();
		}
		
	}
	private class EndLevelThread extends Thread {
		boolean win = false;
		public EndLevelThread(boolean isWin) {
			win = isWin;
		}
		
		@Override
		public void run() {
			try { sleep(1500); } catch(Exception e) { }
			
			if(win)  //Add a bonus bottle cap				
				PlayerLevels.storeNewLevels(Player.KEY_TOKENS, PlayerLevels.curLevelTokens + 1);			
			
			Message msg = new Message();
			msg.obj = win;
			endLevelHandler.sendMessage(msg);
		}
	}
	
	public void pause() {
		tempElapsedTime = System.currentTimeMillis() - levelStartTime;
		isPaused = true;
		timerFragment.pause();
	}
	public void resume() {
		new GameLoopThread().start();
		levelStartTime = System.currentTimeMillis();
		isPaused = false;
		timerFragment.resume();
	}
	
	long nextUpdateTime = 0;
	private class GameLoopThread extends Thread {
		final long gapTime = 1000 / Consts.FPS;
		
		@Override
		public void run() {				
				while(!isPaused) {
					nextUpdateTime = System.currentTimeMillis() + gapTime;
					
					Message msg1 = new Message();
					msg1.obj = "loop";
					loopThreadHandler.sendMessage(msg1);
					
					if(timerFragment.isExpired) {
						Message msg2 = new Message();
						msg2.obj = "end";
						loopThreadHandler.sendMessage(msg2);
					}					
					
					long sleepTime = nextUpdateTime - System.currentTimeMillis();
					if(sleepTime > 0) try { sleep(sleepTime); } catch(Exception e) { }
				}			
		}
	}
	private static class GameLoopThreadHandler extends Handler {
		final private WeakReference<FragmentGame> mTarget;
		
		public GameLoopThreadHandler(FragmentGame target) {
			mTarget = new WeakReference<>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			FragmentGame thisTarget = mTarget.get();
			
			if(msg.obj.equals("loop"))
				thisTarget.update();
            else if(msg.obj.equals("end")) {
				 
				if(!thisTarget.isBossLaunched) {
					if(PlayerLevels.curLevelWave % 10 == 0) {
						//Spawn boss
						SoundManager.playSound(thisTarget.getActivity(), SoundManager.BOSS_ALERT);
						thisTarget.zombieMain.spawnBoss();						
					}
					thisTarget.isBossLaunched = true;
				}
				if(thisTarget.zombieMain.members.size() < 1 && thisTarget.isBossLaunched) {
					thisTarget.endLevel(true);
				}
			}			
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		touchPoint.x = event.getX();
		touchPoint.y = event.getY();
		
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			player.rotateTo(touchPoint);
			player.isLive = true;
			break;
			
		case MotionEvent.ACTION_MOVE:
			player.rotateTo(touchPoint);
			player.isLive = true;
			break;
			
		case MotionEvent.ACTION_UP:
			player.isLive = false;
			break;
		}
		return true;
	}

	private void fire(int TURRETtype, PointF startPoint, PointF destPoint, double rotationDeg) {
		if(TURRETtype == Bullet.PLAYER) {
			bulletsPlayer.fire(TURRETtype, startPoint, destPoint, rotationDeg, true);
			SoundManager.playSound(getActivity(), SoundManager.POP);
			
		} else if(zombieMain.getClosestZombie() != null)
				bulletsTurrets.fire(TURRETtype, startPoint, zombieMain.getClosestZombie(), rotationDeg, true);		
	}
	
	public void addToCoins(int toAdd) {
		curCoins += toAdd;
		waveCoinsEarned += toAdd;		
		((TextView)getView().findViewById(R.id.gamefragment_textview_coincount)).setText(Integer.toString(curCoins));
	}
	public void rotateTurrets() {
		PointF destPointF = zombieMain.getClosestZombie();
		turretTop.rotateTo(destPointF);
		turretBottom.rotateTo(destPointF);
		
		/*
		if(destPointF != null) {
			//Top turret
			float diffX = destPointF.x - (turretTopLayout.getLeft() + (turretTopLayout.getWidth() / 2));
			float diffY = destPointF.y - (turretTopLayout.getTop() +  (turretTopLayout.getHeight() / 2));
			double radRotation = Math.atan2(diffY, diffX);
			if(turretTop.curTurretColor != -1) turretTop.rotate(Math.toDegrees(radRotation));
			
			//Bottom Turret
			diffX = destPointF.x - (turretBottomLayout.getLeft() + (turretBottomLayout.getWidth() / 2));
			diffY = destPointF.y - (turretBottomLayout.getTop() + (turretBottomLayout.getHeight() / 2));
			radRotation = Math.atan2(diffY, diffX);
			if(turretBottom.curTurretColor != -1) turretBottom.rotate(Math.toDegrees(radRotation));
		}
		*/
		
		
	}	
	
	
	public void savePlayerLevels() {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(Player.KEY_COINS, curCoins);
		editor.putInt(Player.KEY_TOKENS, curTokens);
		editor.putInt(Player.KEY_WAVE, curWave);
		
		long timeElapsed = (System.currentTimeMillis() - levelStartTime) + tempElapsedTime;
		long newTotalTime = sp.getLong(Consts.SP_KEY_STATS_TIME_PLAYED, 0) + timeElapsed;
		editor.putLong(Consts.SP_KEY_STATS_TIME_PLAYED, newTotalTime);
		
		int curCoinsEarned = sp.getInt(Consts.SP_KEY_STATS_COINS_EARNED, 0);
		editor.putInt(Consts.SP_KEY_STATS_COINS_EARNED, curCoinsEarned + waveCoinsEarned);
		
		long curZombiesStopped = sp.getLong(Consts.SP_KEY_STATS_TOTAL_ZOMBIES_KILLED, 0);
		editor.putLong(Consts.SP_KEY_STATS_TOTAL_ZOMBIES_KILLED, curZombiesStopped + zombiesKilled);
		
		long totalBottlesLost = sp.getLong(Consts.SP_KEY_STATS_BOTTLES_LOST, 0);
		editor.putLong(Consts.SP_KEY_STATS_BOTTLES_LOST, totalBottlesLost + bottles.getBottleLostCount());
		
		editor.apply();
	}	
	
	
	
	Runnable scaleTurretsRunnable = new Runnable() {
		@Override
		public void run() {
			final float RATIO = 1.61803398875f;			
			LinearLayout turretsLayout = (LinearLayout)getView().findViewById(R.id.gamefragment_linearlayout_turrets);
			fragmentHeight = turretsLayout.getHeight();	
			fragmentWidth = getView().getWidth();
			float playerHeight = ((float)fragmentHeight / 2f) / ((1f / RATIO) + 0.5f);
			float turretHeight = (1f / RATIO) * playerHeight;

            player.setMinimumWidth(fragmentWidth / 8);

			turretTop.setScale(turretHeight / (float) turretTop.getBmpHeight());
			player.setScale(playerHeight / (float)player.getBmpHeight());
			turretBottom.setScale(turretHeight / (float)turretBottom.getBmpHeight());
		}
		
	};
}
