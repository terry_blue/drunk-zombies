package com.twentyfourktapps.drunkzombies;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

import com.twentyfourktapps.drunkzombies.obj.StaticResources;


public class ActivitySplash extends Activity {

	String TAG = "ActivitySplash";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		//DEBUG//
		
		
		//END DEBUG//		
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());				
		SharedPreferences.Editor editor = sp.edit();
			
		editor.putString("Lisa", "9eUBUI0kj+JDigHHfQKQIDAQAB");
		editor.putString("Marge", "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgx8amPIxrDKD6QaQu1cBnTDxAjtNrrFXPvRghp0nNYuF3/XMyj3o6Cal9/7YtAJq9j4Qe1WvJ8Vt3z");
		editor.commit();		
		
		AnimationListener aniListener = new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {				
				startActivity(new Intent(ActivitySplash.this, ActivityTitleScreen.class));				
				finish();
				
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				
			}

			@Override
			public void onAnimationStart(Animation arg0) {
				
			}
			
		};
		
		AlphaAnimation animation = new AlphaAnimation(0, 1);
		animation.setDuration(1000);
		animation.setRepeatMode(Animation.REVERSE);
		animation.setRepeatCount(1);
		animation.setFillAfter(true);
		animation.setAnimationListener(aniListener);
		
		findViewById(R.id.splash_imageview_logo).startAnimation(animation);
		
		new Thread() {
			
			@Override
			public void run() {
				StaticResources.initialize(getApplicationContext());
				
			}
		}.start();
	}

	
}
