package com.twentyfourktapps.drunkzombies;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;

public class DialogRateMe extends DialogFragment implements OnClickListener {

	
	final String MARKET_ADDRESS = "market://details?id=com.twentyfourktapps.zombiealarmclock";
	
	public static DialogRateMe instance = null;
	final static String TAG = "DialogRateMe";
	final static String SP_KEY_RATE_ME_CLICKED = "sp_key_dialog_rateme_clicked";
	final static String SP_KEY_RATE_ME_IS_TO_SHOW_TEST_COUNT = "sp_key_rateme_test_count";
	
	public static DialogRateMe getInstance() {
		if(instance == null) 
			instance = new DialogRateMe();
		
		return instance;
	}
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog_theme);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_rateme, container);
		
		view.findViewById(R.id.rateme_button_cancel).setOnClickListener(this);
		view.findViewById(R.id.rateme_button_rateme).setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		setFonts();
		
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		SharedPreferences .Editor edit = sp.edit();
		
		edit.commit();
		
		Log.e(TAG, "test count = " + sp.getInt(SP_KEY_RATE_ME_IS_TO_SHOW_TEST_COUNT, 0));
	}


	@Override
	public void onClick(View v) {

		switch(v.getId()) {
		
		case R.id.rateme_button_rateme: 
			Uri uri = Uri.parse(MARKET_ADDRESS);
			startActivity(new Intent(Intent.ACTION_VIEW, uri));
			
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
			SharedPreferences.Editor edit = sp.edit();
			edit.putBoolean(SP_KEY_RATE_ME_CLICKED, true);
			edit.commit();
			
		case R.id.rateme_button_cancel:			
			dismiss();			
		}
		
		
	}


	private void setFonts() {
		Context context = getView().getContext();
		((TextView)getView().findViewById(R.id.rateme_textview_prompt)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((Button)getView().findViewById(R.id.rateme_button_cancel)).setTypeface(StaticResources.getFont(context, Font.KREEW));
		((Button)getView().findViewById(R.id.rateme_button_rateme)).setTypeface(StaticResources.getFont(context, Font.KREEW));
	}



	public static boolean isToShow(Context cxt) {
		final int countBetweenShows = 5;
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(cxt);
				
		return (!sp.getBoolean(SP_KEY_RATE_ME_CLICKED, false) 
				&& (sp.getInt(SP_KEY_RATE_ME_IS_TO_SHOW_TEST_COUNT, 0) % countBetweenShows == 0));				
	}


}
