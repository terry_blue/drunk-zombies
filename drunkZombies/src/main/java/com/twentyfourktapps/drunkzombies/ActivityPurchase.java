package com.twentyfourktapps.drunkzombies;

import java.util.ArrayList;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.tapjoy.TapjoyConnect;
import com.twentyfourktapps.drunkzombies.billing.Base64;
import com.twentyfourktapps.drunkzombies.billing.IabHelper;
import com.twentyfourktapps.drunkzombies.billing.IabHelper.OnIabPurchaseFinishedListener;
import com.twentyfourktapps.drunkzombies.billing.IabHelper.QueryInventoryFinishedListener;
import com.twentyfourktapps.drunkzombies.billing.IabResult;
import com.twentyfourktapps.drunkzombies.billing.Inventory;
import com.twentyfourktapps.drunkzombies.billing.Purchase;
import com.twentyfourktapps.drunkzombies.billing.SkuDetails;
import com.twentyfourktapps.drunkzombies.obj.Player;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.util.ServerUtil;
import com.twentyfourktapps.drunkzombies.util.TapjoyUtil;
import com.twentyfourktapps.drunkzombies.util.Util;

public class ActivityPurchase extends FragmentActivity implements OnClickListener{

	
	final String TAG = "ActivityPurchase";
	IabHelper mHelper = null;
	boolean isAfterFirstOnPause;
	
	final static String SKU_BUNDLE_GOLD_SMALL = "package_gold_small";
	final static String SKU_BUNDLE_GOLD_MED = "package_gold_med";
	final static String SKU_BUNDLE_GOLD_LARGE = "package_gold_large2";
	final static String SKU_BUNDLE_TOKEN_SMALL = "package_bottlecaps_small";
	final static String SKU_BUNDLE_TOKEN_MED = "package_bottlecaps_med";
	final static String SKU_BUNDLE_TOKEN_LARGE = "package_bottlecaps_large2";
	
	final int GAIN_GOLD_SMALL;
	final int GAIN_GOLD_MED;
	final int GAIN_GOLD_LARGE;
	final int GAIN_TOKEN_SMALL;
	final int GAIN_TOKEN_MED;
	final int GAIN_TOKEN_LARGE;
	
	public ActivityPurchase() {
		GAIN_GOLD_SMALL = Consts.price_TURRET_BLUE * 10;
		GAIN_GOLD_MED = Consts.price_TURRET_GREEN  * 10;
		GAIN_GOLD_LARGE = Consts.price_TURRET_YELLOW * 10;
		GAIN_TOKEN_SMALL = Consts.price_TURRET_BLUE;
		GAIN_TOKEN_MED = Consts.price_TURRET_GREEN;
		GAIN_TOKEN_LARGE = Consts.price_TURRET_YELLOW;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_purchase);
		TapjoyUtil.connect(getApplicationContext());
		isAfterFirstOnPause = false;
		
		
		mHelper = new IabHelper(ActivityPurchase.this, Base64.encode(Util.getAppLiKey(getApplicationContext()).getBytes()));
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			   public void onIabSetupFinished(IabResult result) {
			      if (!result.isSuccess()) {
			         // Oh noes, there was a problem.
			         Log.e(TAG, "Problem setting up In-app Billing: " + result);
			      }            
			         // Hooray, IAB is fully set up!  
			      
			   }
			});
		
		setClickListeners();
		
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		setTexts();
		
		//The callback after returning from the Tapjoy offerwall
		try {
			if(isAfterFirstOnPause) {
				DialogFetching dialog = DialogFetching.getInstance();
				dialog.show(getSupportFragmentManager(), DialogFetching.TAG);
			}
		} catch(Exception e) {
			Log.e(TAG, "onResume error: "+ e.toString());
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		isAfterFirstOnPause = true;
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		if(mHelper != null) mHelper.dispose();
		mHelper = null;
	}

	
	private void setClickListeners() {
		findViewById(R.id.purchasescreen_button_tapjoy_offerwall_tokens).setOnClickListener(this);
		findViewById(R.id.purchasescreen_button_tapjoy_offerwall_coins).setOnClickListener(this);
		
		findViewById(R.id.purchase_gold_small).setOnClickListener(this);
		findViewById(R.id.purchase_gold_med).setOnClickListener(this);
		findViewById(R.id.purchase_gold_large).setOnClickListener(this);
		findViewById(R.id.purchase_bottlecaps_small).setOnClickListener(this);
		findViewById(R.id.purchase_bottlecaps_med).setOnClickListener(this);
		findViewById(R.id.purchase_bottlecaps_large).setOnClickListener(this);	
	}
	
	private void setTexts() {
		TextView smallGoldTV = (TextView)findViewById(R.id.purchase_gold_small);
		TextView medGoldTV = (TextView)findViewById(R.id.purchase_gold_med);
		TextView largeGoldTV = (TextView)findViewById(R.id.purchase_gold_large);
		TextView smallTokenTV = (TextView)findViewById(R.id.purchase_bottlecaps_small);
		TextView medTokenTV = (TextView)findViewById(R.id.purchase_bottlecaps_med);
		TextView largeTokenTV = (TextView)findViewById(R.id.purchase_bottlecaps_large);
		
		smallGoldTV.setText("x"+Integer.toString(GAIN_GOLD_SMALL));
		medGoldTV.setText("x"+Integer.toString(GAIN_GOLD_MED));
		largeGoldTV.setText("x"+Integer.toString(GAIN_GOLD_LARGE));
		smallTokenTV.setText("x"+Integer.toString(GAIN_TOKEN_SMALL));
		medTokenTV.setText("x"+Integer.toString(GAIN_TOKEN_MED));
		largeTokenTV.setText("x"+Integer.toString(GAIN_TOKEN_LARGE));
		
		smallGoldTV.setTypeface(StaticResources.getFont(getApplicationContext(), Font.TREASURE));
		medGoldTV.setTypeface(StaticResources.getFont(getApplicationContext(), Font.TREASURE));
		largeGoldTV.setTypeface(StaticResources.getFont(getApplicationContext(), Font.TREASURE));
		smallTokenTV.setTypeface(StaticResources.getFont(getApplicationContext(), Font.TREASURE));
		medTokenTV.setTypeface(StaticResources.getFont(getApplicationContext(), Font.TREASURE));
		largeTokenTV.setTypeface(StaticResources.getFont(getApplicationContext(), Font.TREASURE));
	}
	

	@Override
	public void onClick(View v) {
		
		OnIabPurchaseFinishedListener mPurchaseFinishedListener = new OnIabPurchaseFinishedListener() {

			@Override
			public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				
				int coinsEarned = sp.getInt(Consts.SP_KEY_STATS_COINS_EARNED, 1);
				int tokensEarned = sp.getInt(Consts.SP_KEY_STATS_TOKENS_EANRED, 1);
				int curCoins = PlayerLevels.curLevelCoins;
				int curTokens = PlayerLevels.curLevelTokens;				
				
				int tokensMod = 0; int coinsMod = 0;
				if(result.isFailure()) {
					Log.e(TAG, "onIabPurchaseFailure");
				} else if(purchase.getSku().equals(SKU_BUNDLE_GOLD_SMALL)) {
					coinsMod += GAIN_GOLD_SMALL;					
				} else if(purchase.getSku().equals(SKU_BUNDLE_GOLD_MED)) {
					coinsMod += GAIN_GOLD_MED;				
				} else if(purchase.getSku().equals(SKU_BUNDLE_GOLD_LARGE)) {					
					coinsMod += GAIN_GOLD_LARGE;			
				} else if(purchase.getSku().equals(SKU_BUNDLE_TOKEN_SMALL)) {
					tokensMod += GAIN_TOKEN_SMALL;			
				} else if(purchase.getSku().equals(SKU_BUNDLE_TOKEN_MED)) {
					tokensMod += GAIN_TOKEN_MED;			
				} else if(purchase.getSku().equals(SKU_BUNDLE_TOKEN_LARGE)) {
					tokensMod += GAIN_TOKEN_LARGE;		
				}
				
				if(coinsMod > 0 || tokensMod > 0) {
					Log.i(TAG, purchase.getSku() + " purchased");
					mHelper.consumeAsync(purchase, null);					
					
					//Save data to shared preferences
					SharedPreferences.Editor editor = sp.edit();
					editor.putInt(Consts.SP_KEY_STATS_TOKENS_EANRED, tokensEarned);
					editor.putInt(Consts.SP_KEY_STATS_COINS_EARNED, coinsEarned);
					editor.commit();
					
					PlayerLevels.storeNewLevels(Player.KEY_COINS, curCoins + coinsMod);
					PlayerLevels.storeNewLevels(Player.KEY_TOKENS, curTokens + tokensMod);		
					
					
					new SendDataThread().start();
					onBackPressed();
				}
				
			}
			
		};
		
		String currencyID = TapjoyUtil.CURRENCY_ID_COIN;
		final String[] SKUS = new String[] { SKU_BUNDLE_TOKEN_LARGE, SKU_BUNDLE_TOKEN_MED, SKU_BUNDLE_TOKEN_SMALL, SKU_BUNDLE_GOLD_LARGE, SKU_BUNDLE_GOLD_MED, SKU_BUNDLE_GOLD_SMALL  };
		int pos = 0;
		switch(v.getId()) {
		case R.id.purchasescreen_button_tapjoy_offerwall_tokens: 			
			currencyID = TapjoyUtil.CURRENCY_ID_TOKEN;
		case R.id.purchasescreen_button_tapjoy_offerwall_coins: {
			try {
				if(Util.isUserLoggedIn(getApplicationContext()))
					TapjoyConnect.getTapjoyConnectInstance().showOffersWithCurrencyID(currencyID, false);
				else TapjoyConnect.getTapjoyConnectInstance().showOffers();
			} catch(Exception e) {
				Log.e(TAG, "error showing Tapjoy data: " + e.toString());
				TapjoyUtil.connect(getApplicationContext());
				Toast.makeText(getApplicationContext(), "Error connecting to Tapjoy, try again later", Toast.LENGTH_SHORT).show();
			}
			break;
		}
		
		
		case R.id.purchase_gold_small: pos++;
		case R.id.purchase_gold_med: pos++;
		case R.id.purchase_gold_large: pos++;		
		case R.id.purchase_bottlecaps_small: pos++;
		case R.id.purchase_bottlecaps_med: pos++;
		case R.id.purchase_bottlecaps_large: {
			try {
				mHelper.launchPurchaseFlow(this, SKUS[pos], 2424, mPurchaseFinishedListener, "developerPayload");				
			} catch(Exception e) {
				Log.e(TAG, "purchase error: " + e.toString());
			}
		}
		}		
		
		
	}
	
	

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) { 
	   if (requestCode == 2424) {           
		  Log.e(TAG, "onActivityResult");
		  mHelper.handleActivityResult(requestCode, resultCode, data);		  
	   }
	}
	
	private class SendDataThread extends Thread {		
		
		@Override
		public void run() {
			ServerUtil.sendDZdata(getApplicationContext());
		}
	}
}
