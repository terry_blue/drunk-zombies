package com.twentyfourktapps.drunkzombies;

import java.lang.ref.SoftReference;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.obj.Turret.TurretType;
import com.twentyfourktapps.drunkzombies.util.BanterDismissListener;

public class DialogBanter extends DialogFragment implements OnClickListener, OnDismissListener {

	final static String TAG = "DialogBanter";
	
	public static enum Scenario { BANTER, STORE_SCREEN, CURRENCIES, FIGHT, TIMER, FIRING, BOTTLES, CONGRATZ, DZ_CAPS, UPGRADES, POWERS, POWERS_CHARGING, TURRETS }
	private Scenario curScenario;
	
	public static final String HTP_KEY_STORE_SCREEN = "htp_key_store_screen";
	public static final String HTP_KEY_CURRENCIES = "htp_key_currencies";
	public static final String HTP_KEY_FIGHT = "htp_key_fight";
	public static final String HTP_KEY_TIMER = "htp_key_timer";
	public static final String HTP_KEY_FIRING = "htp_key_firing";
	public static final String HTP_KEY_BOTTLES = "htp_key_bottles";
	public static final String HTP_KEY_CONGRATZ = "htp_key_congratz";
	public static final String HTP_KEY_DZ_CAPS = "htp_key_dz_caps";
	public static final String HTP_KEY_UPGRADES = "htp_key_upgrades";
	public static final String HTP_KEY_POWERS = "htp_key_powers";
	public static final String HTP_KEY_POWERS_CHARGING = "htp_key_powers_charging";
	public static final String HTP_KEY_TURRETS = "htp_key_turrets";
	
	final int LETTER_DELAY = 90;
	String[] textSpeech = null;
	int arrayPos = 0;
	SpeechTextHandler speechTextHandler;
	boolean isSpeechThreadRunning;
	TurretType speaker1, speaker2;
	
	
	
	public DialogBanter initialize(Resources res, Scenario scenario, TurretType char1, TurretType char2) {		
		curScenario = scenario;
		speaker1 = char1;
		speaker2 = char2;
		textSpeech = null;
		speechTextHandler = new SpeechTextHandler(this);
		
		arrayPos = 0;
		isSpeechThreadRunning = false;		
		
		switch(scenario) {
		case STORE_SCREEN: textSpeech = res.getStringArray(R.array.htp_storescreen); break;
		case CURRENCIES: textSpeech = res.getStringArray(R.array.htp_currencies); break;
		case FIGHT: textSpeech = res.getStringArray(R.array.htp_fight); break;
		case TIMER: textSpeech = res.getStringArray(R.array.htp_timer); break;
		case FIRING: textSpeech = res.getStringArray(R.array.htp_firing); break;
		case BOTTLES: textSpeech = res.getStringArray(R.array.htp_bottles); break;
		case CONGRATZ: textSpeech = res.getStringArray(R.array.htp_congratz); break;
		case DZ_CAPS: textSpeech = res.getStringArray(R.array.htp_dz_caps); break;
		case UPGRADES: textSpeech = res.getStringArray(R.array.htp_upgrades); break;
		case POWERS: textSpeech = res.getStringArray(R.array.htp_powers); break;
		case POWERS_CHARGING: textSpeech = res.getStringArray(R.array.htp_powers_charging); break;
		case TURRETS: textSpeech = res.getStringArray(R.array.htp_turrets); break;
		case BANTER: textSpeech = getBanterArray(res); break;
		}
		
		if(textSpeech != null)
			new SpeechTextThread(textSpeech[arrayPos]).start();
		
		return this;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setCancelable(false);
		View view = inflater.inflate(R.layout.dialog_banter, container);
		
        
        view.findViewById(R.id.banter_button_next).setOnClickListener(this);
		view.findViewById(R.id.banter_button_skip).setOnClickListener(this);		
		
		Resources res = getActivity().getResources();
		
		//Set the character images
		Bitmap bmp = null;
		
		switch(speaker1) {
		case PLAYER: bmp = BitmapFactory.decodeResource(res, R.drawable.turret_johnny); break;
		case GREEN: bmp = BitmapFactory.decodeResource(res, R.drawable.turret_green); break;
		case BLUE: bmp = BitmapFactory.decodeResource(res, R.drawable.turret_blue); break;
		case YELLOW: bmp = BitmapFactory.decodeResource(res, R.drawable.turret_yellow); break;
		}			
		Drawable drawable = new BitmapDrawable(res, bmp);
		drawable.setBounds(0, 0, bmp.getWidth(), bmp.getHeight());
		
		((TextView)(view.findViewById(R.id.banter_textview_char1))).setCompoundDrawables(drawable, null, null, null);		
		
		Bitmap bmp2 = null;
		switch(speaker2) {
		case PLAYER: bmp2 = flipImage(BitmapFactory.decodeResource(res, R.drawable.turret_johnny)); break;
		case GREEN: bmp2 = flipImage(BitmapFactory.decodeResource(res, R.drawable.turret_green)); break;
		case BLUE: bmp2 = flipImage(BitmapFactory.decodeResource(res, R.drawable.turret_blue)); break;
		case YELLOW: bmp2 = flipImage(BitmapFactory.decodeResource(res, R.drawable.turret_yellow)); break;
		}		
		
		drawable = new BitmapDrawable(res, bmp2);
		drawable.setBounds(0, 0, bmp2.getWidth(), bmp2.getHeight());
		((TextView)(view.findViewById(R.id.banter_textview_char2))).setCompoundDrawables(null, null, drawable, null);
		
		getDialog().setCanceledOnTouchOutside(false);
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog_theme);
	}

	@Override
	public void onStart() {
		super.onStart();
				
		setFonts();
		getDialog().setOnDismissListener(this);
	}
	
	@Override
	public void onClick(View v) {
		
		switch(v.getId()) {
		case R.id.banter_button_next:
			if(isSpeechThreadRunning) {
				isSpeechThreadRunning = false;
				TextView textView = (arrayPos % 2 == 0) ? (TextView)(getView().findViewById(R.id.banter_textview_char1)) : (TextView)(getView().findViewById(R.id.banter_textview_char2));
				textView.setText(textSpeech[arrayPos]);				
			} else {				
				arrayPos++;
				if(arrayPos < textSpeech.length)
					new SpeechTextThread(textSpeech[arrayPos]).start();
				else { 
					dismiss();
					completeScenario();
				}
					
				
				if(arrayPos % 2 == 0) 
					((ViewSwitcher)(getView().findViewById(R.id.banter_viewswitcher_content))).showNext();
				else
					((ViewSwitcher)(getView().findViewById(R.id.banter_viewswitcher_content))).showPrevious();
			}
			
			break;
		case R.id.banter_button_skip:			
			dismiss();
			completeScenario();
		}
		
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		if(getActivity() instanceof BanterDismissListener) {
			((BanterDismissListener)getActivity()).onBanterDismiss();
		}
	}
	
	public void addCharToString(char letter) {
		View view = getView();
		if(view != null) {
			TextView textView = (arrayPos % 2 == 0) ? (TextView)(view.findViewById(R.id.banter_textview_char1)) : (TextView)(view.findViewById(R.id.banter_textview_char2));
			String newText = textView.getText().toString() + letter;
			textView.setText(newText);
		}
	}	
	
	
	private Bitmap flipImage(Bitmap src) {
		Matrix matrix = new Matrix();
		matrix.preScale(-1.0f, 1.0f);
		
		return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
	}
	
	
	public String[] getBanterArray(Resources res) {
		int[] banterIdsArray = new int[] { R.array.banter_banter1, R.array.banter_banter2, R.array.banter_banter3, R.array.banter_banter4, R.array.banter_banter5
										, R.array.banter_banter6, R.array.banter_banter7, R.array.banter_banter8, R.array.banter_banter9, R.array.banter_banter10
										, R.array.banter_banter11, R.array.banter_banter12, R.array.banter_banter13, R.array.banter_banter14, R.array.banter_banter15
										, R.array.banter_banter16, R.array.banter_banter17, R.array.banter_banter18, R.array.banter_banter19, R.array.banter_banter20, R.array.banter_banter21 };
		
		if(PlayerLevels.curLevelWave % 2 == 0 && PlayerLevels.curLevelWave < banterIdsArray.length * 2) 
			return res.getStringArray(banterIdsArray[PlayerLevels.curLevelWave / 2]);		
		
		return null;
	}
	public void completeScenario() {		
		String key = getFlagKeyForScenario(curScenario);
		if(key != null) {
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
			SharedPreferences.Editor edit = sp.edit();
			edit.putBoolean(key, true);
			edit.commit();
		}
	}
	
	public static boolean hasScenarioCompleted(Context context, Scenario scene) { 
		String key = getFlagKeyForScenario(scene);
		if(key != null) {
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
			return sp.getBoolean(key, false);
		}		
		return false;
	}
	
	public static String getFlagKeyForScenario(Scenario scene) {
		switch(scene) {
		case STORE_SCREEN: return HTP_KEY_STORE_SCREEN;
		case CURRENCIES: return HTP_KEY_CURRENCIES;
		case FIGHT: return HTP_KEY_FIGHT;
		case TIMER: return HTP_KEY_TIMER;
		case FIRING: return HTP_KEY_FIRING;
		case BOTTLES: return HTP_KEY_BOTTLES;
		case CONGRATZ: return HTP_KEY_CONGRATZ;
		case DZ_CAPS: return HTP_KEY_DZ_CAPS;
		case UPGRADES: return HTP_KEY_UPGRADES;
		case POWERS: return HTP_KEY_POWERS;
		case POWERS_CHARGING: return HTP_KEY_POWERS_CHARGING;
		case TURRETS: return HTP_KEY_TURRETS;
		}
	
		return null;
	}
	
	
	
	
	private void setFonts() {
		Context context = getDialog().getContext();
		((TextView)getView().findViewById(R.id.banter_textview_char1)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((TextView)getView().findViewById(R.id.banter_textview_char2)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((TextView)getView().findViewById(R.id.banter_button_next)).setTypeface(StaticResources.getFont(context, Font.KREE));
		((TextView)getView().findViewById(R.id.banter_button_skip)).setTypeface(StaticResources.getFont(context, Font.KREE));
	}
	
	
	private class SpeechTextThread extends Thread {
		final private String text;
		int charPos;
		
		SpeechTextThread(String speech) {
			text = speech;
			charPos = 0;
		}
		
		@Override
		public void run() {
			isSpeechThreadRunning = true;
			while(charPos < text.length() && isSpeechThreadRunning) {
				Message msg = new Message();
				msg.obj = text.charAt(charPos++);
				speechTextHandler.sendMessage(msg);
				try { sleep(LETTER_DELAY); } catch(Exception e) {  }
			}
			isSpeechThreadRunning = false;
				
		}
	}
	
	private static class SpeechTextHandler extends Handler {
		final private SoftReference<DialogBanter> TARGET;
		
		SpeechTextHandler(DialogBanter target) {
			TARGET = new SoftReference<DialogBanter>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			final DialogBanter THIS = TARGET.get();
			if(msg.obj instanceof Character) {
				THIS.addCharToString((Character)msg.obj);
			}
			
		}
	}
}
