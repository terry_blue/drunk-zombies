package com.twentyfourktapps.drunkzombies;

import com.twentyfourktapps.drunkzombies.DialogBanter.Scenario;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.Turret.TurretType;
import com.twentyfourktapps.drunkzombies.util.BanterDismissListener;
import com.twentyfourktapps.drunkzombies.util.MusicService;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.view.WindowManager;

import java.lang.ref.WeakReference;

public class ActivityGame extends FragmentActivity implements BanterDismissListener {

	FragmentGame game;

	final String TAG = "ActivityGame";
    PrepGameTask prepGameTask;
    //DelayHandler delayHandler;
	
	private boolean mIsBound = false;
	public MusicService mServ;
	private ServiceConnection Scon;

		void doBindService() {
	 		bindService(new Intent(this, MusicService.class), Scon, Context.BIND_AUTO_CREATE);
			mIsBound = true;
		}

		void doUnbindService() {
			if(mIsBound) {
				unbindService(Scon);
				stopService(new Intent(ActivityGame.this, MusicService.class));
	      		mIsBound = false;
			}
		}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_game);

       // delayHandler = new DelayHandler(this);
		Scon = new ServiceConnection() {

			@Override
			public void onServiceConnected(ComponentName name, IBinder binder) {
				mServ = ((MusicService.ServiceBinder)binder).getService();
			}

			@Override
			public void onServiceDisconnected(ComponentName name) {
				mServ = null;
			}
		};
		
		game = (FragmentGame)getSupportFragmentManager().findFragmentById(R.id.gameactivity_gamefragment);
        prepGameTask = new PrepGameTask();

        findViewById(R.id.gameactivity_gamefragment).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            if(prepGameTask.isReady)
                prepGameTask.execute();
            }
        });

        findViewById(R.id.gamefragment_bullets_player).setOnTouchListener(game);
	}
	
	public void resumeGame() {
		game.resume();
		DialogFragment pauseDialog = (DialogFragment)getSupportFragmentManager().findFragmentByTag(DialogPause.TAG);
		pauseDialog.setCancelable(true);
		pauseDialog.dismiss();
		if(mServ != null)
			mServ.resumeMusic();
	}
	
	@Override
	public void onBackPressed() {
		if(game != null)
			pauseGame();
		if(mServ != null) {			
			mServ.pauseMusic();
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		if(game.isGameRunning)
			pauseGame();
		if(mServ != null)
			mServ.pauseMusic();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(mServ != null)
			mServ.stopMusic();		
		doUnbindService();
	}
	
	public void pauseGame() {
		if(!game.isPaused) {
            game.pause();
			DialogPause pause = DialogPause.getInstance();					
			if(!pause.isAdded())
                pause.show(getSupportFragmentManager(), DialogPause.TAG);
		}
	}
	
	public boolean checkForBanter() {
		Context context = ActivityGame.this;
		
		if(PlayerLevels.curLevelWave == 1
		&& !DialogBanter.hasScenarioCompleted(context, Scenario.TIMER)) {
			pauseGame();
			new DialogBanter().initialize(getResources(), Scenario.TIMER, TurretType.PLAYER, TurretType.BLUE).show(getSupportFragmentManager(), "dialog_banter");
			return true;
		}
		
		else if(PlayerLevels.curLevelWave == 1
		&& DialogBanter.hasScenarioCompleted(context, Scenario.TIMER)
		&& !DialogBanter.hasScenarioCompleted(context, Scenario.FIRING)) {
			pauseGame();
			new DialogBanter().initialize(getResources(), Scenario.FIRING, TurretType.PLAYER, TurretType.BLUE).show(getSupportFragmentManager(), "dialog_banter");
			return true;
		}
		
		else if(PlayerLevels.curLevelWave == 1
		&& DialogBanter.hasScenarioCompleted(context, Scenario.FIRING)
		&& !DialogBanter.hasScenarioCompleted(context, Scenario.BOTTLES)) {
			pauseGame();
			new DialogBanter().initialize(getResources(), Scenario.BOTTLES, TurretType.PLAYER, TurretType.GREEN).show(getSupportFragmentManager(), "dialog_banter");
			return true;
		}
		
		else if(PlayerLevels.curLevelWave == 2
		&& !DialogBanter.hasScenarioCompleted(context, Scenario.POWERS_CHARGING)) {
			pauseGame();
			new DialogBanter().initialize(getResources(), Scenario.POWERS_CHARGING, TurretType.PLAYER, TurretType.YELLOW).show(getSupportFragmentManager(), "dialog_banter");
			return true;
		}
			
		
		return false;
	}
	

	private static class DelayHandler extends Handler {
		final private WeakReference<ActivityGame> TARGET;
		
		DelayHandler(ActivityGame target) {
			TARGET = new WeakReference<ActivityGame>(target);
		}
		
		@Override
		public void handleMessage(Message msg) {
			ActivityGame thisTarget = TARGET.get();			
			
			thisTarget.game.initialize();
			
			thisTarget.doBindService();
			Intent music = new Intent(thisTarget, MusicService.class);
			music.setClass(thisTarget, MusicService.class);
			thisTarget.startService(music);
            thisTarget.game.start();
		}
	}
	private class DelayThread extends Thread {
		
		@Override
		public void run() {
			SoundManager.playSound(getBaseContext(), SoundManager.BRING_IT);
            try { sleep(500); } catch (Exception e) { Log.e(TAG, "no sleep: "+e.toString()); }

			//delayHandler.sendMessage(new Message());
		}
	}


	private class PrepGameTask extends AsyncTask<Void, Void, Void> {
        boolean isReady = true;

        @Override
        protected void onPreExecute() {
            isReady = false;
        }
		@Override
		protected Void doInBackground(Void... params) {
            isReady = false;
            doBindService();
            startService(new Intent(getApplicationContext(), MusicService.class));
			SoundManager.playSound(getBaseContext(), SoundManager.BRING_IT);
            try { Thread.sleep(100); } catch(Exception e) { Log.e(TAG, "sleep error"); }

			game.initialize();

			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			game.start();
		}
		
	}

	
	@Override
	public void onBanterDismiss() {
		if(!checkForBanter())
			resumeGame();
		
	}
}
