package com.twentyfourktapps.drunkzombies.obj;

import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.units.SingleUnit.UnitType;

public class ZombieController {

	final static String TAG = "ZombieController";
	long nextSpawnTime;	
	
	static float curDifficulty; //final modifier, expressed as a decimal (0.0 - 1.0)
	public static boolean isInitialized = false;
	
	
	
	public static void initialize(Context context) {
		setDifficulty(context);
		
		isInitialized = true;
	}
	
	public static int getGroupsPerSpawner() {
		if(PlayerLevels.curLevelWave < 10) return 1;
		return (PlayerLevels.curLevelWave % 10) + 1;
	}
	
	public static final int ZOMBIE_CAP = 50;
	public static int getZombiesPerGroup() {
		if(PlayerLevels.curLevelWave < 5)
			return 1;
		float extremeMax = (PlayerLevels.curLevelWave < ZOMBIE_CAP) ? PlayerLevels.curLevelWave : ZOMBIE_CAP;
		float max = curDifficulty * extremeMax;
		float min = max * curDifficulty;
		if(max < 1) max = 1;
		Random rand = new Random();		
		int toReturn = rand.nextInt((int)max) + (int)min;
		return (toReturn > 0) ? toReturn : 1;
	}
	public static int getZombieType() {		
		int waveMod = 0;
		switch(PlayerLevels.curLevelWave % 10) {		
		case 0: waveMod++;
		case 9: waveMod++;
		case 8: waveMod++;
		case 7:
		case 6: waveMod++;
		case 5: waveMod++;
		case 4: waveMod++;
		case 3:
		case 2: waveMod++;
		case 1: waveMod++;
		}
		
		return new Random().nextInt(waveMod); 
	}
	public static int getBossZombieType() {
		return (PlayerLevels.curLevelWave % 10) % 3;
	}
	public static int getBossSpawnCount() {
		return (PlayerLevels.curLevelWave % 10) / 3;
	}
	public static long getSpawnerTimeDelay() {
		long k = Consts.ZOMBIE_SPAWNER_TIME_DELAY;
		return (long) (k * (((PlayerLevels.curLevelWave * curDifficulty) * 2) / 100)) ;
	}
	public static int getMaxSpawners() {
		if(PlayerLevels.curLevelWave < 40) return 1;
		return (PlayerLevels.curLevelWave / 50) + 1;
	}
	
	public static boolean spawnChance() {
		Random rand = new Random();
		return rand.nextInt(2147483642) < (((2147483642) / 2) + (PlayerLevels.curLevelWave * curDifficulty));
	}
	
	public static boolean getIsBlockSpawn() {
		Random rand = new Random();
		return (rand.nextBoolean());
		
	}
	public static double getNormalZombieSpeed(UnitType unitType, int zombieType) {
		double k = 1;
		switch(unitType) {
		case ZOMBIE: k = Consts.ZOMBIE_MOVE_SPEED[zombieType]; break; 
		case BOSS: k = Consts.BOSS_MOVE_SPEED[zombieType]; break;
		case POW:
		case BULLET:
		case SPEW:
		}
		
		//Log.e(TAG, "zombieSpeed = " + Double.toString(k + (k * (((playerLevels.curLevelWave * curDifficulty) * 2) / 100))));
		return k + (k * (((PlayerLevels.curLevelWave * curDifficulty) * 2) / 100));
	}
	public static double getZombieConsumeRate(UnitType unitType, int zombieType) {
		double k = 1;
		switch(unitType) {
		case ZOMBIE: k = Consts.ZOMBIE_CONSUME_RATE[zombieType]; break;		
		case BOSS: k = Consts.BOSS_CONSUME_RATE[zombieType]; break;
		case POW:
		case BULLET:
		case SPEW:
		}		 
		return k + (k * (((PlayerLevels.curLevelWave * curDifficulty) * 2) / 100));
	}
	public static long getZombieStartConsumingDelay(int zombieType) {
		long k = Consts.ZOMBIE_CONSUME_START_DELAY[zombieType];		
		return k + (long)((double)k * (double)Consts.uLevel_BOTTLE_THICKNESS[PlayerLevels.curBottleThickness]);
	}
	
	public static int getZombieStartHp(UnitType unitType, int zombieType) {
		int k = 1;
		switch(unitType) {
		case ZOMBIE: k = Consts.ZOMBIE_START_HP[zombieType]; break;
		case BOSS: k = Consts.BOSS_START_HP[zombieType]; break;
		case POW:
		case BULLET:
		case SPEW:
		}
		
		return k + (int)((float)k * ((((float)PlayerLevels.curLevelWave * curDifficulty) * 2f) / 100f));
	}	
	
	
	public static int getBossSpawnGroupSize() {		
		int max = (int)(((float)PlayerLevels.curLevelWave / 10f) * curDifficulty);
		int min = (int)Math.ceil(((float)max / 2f) * curDifficulty);		
		Random rand = new Random();
		int size = rand.nextInt(max);		
		return (size < min) ? min : size;
	}
	public static int getBossActionTickCount() {		
		return (int)(30f * curDifficulty); //TODO tie into wave/difficulty methods
	}
	public static int getBossCountdownTickCount() {
		int tickCount = (int)((curDifficulty * (float)PlayerLevels.curLevelWave) + 5000f);
		int floor = (int)Math.floor(tickCount);
		// tickCount = (int)Math.ceil(floor);
		
		return (int)((float)Consts.FPS * curDifficulty);
	}
	
	final static float START_DIFFICULTY = 0.2f;
	public static void setDifficulty(Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context); 
		curDifficulty = sp.getFloat(Consts.SP_KEY_DIFFICULTY, 0.5f);	
	}
	
	public static void adjustDifficulty(Context context, boolean win) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor edit = sp.edit();
		if(win) {
			if(curDifficulty < 1) 
				curDifficulty += 0.01f;
		} else {
			if(curDifficulty > 0.4)
				curDifficulty -= 0.01f;
		}
		
		edit.putFloat(Consts.SP_KEY_DIFFICULTY, curDifficulty);		
		edit.commit();		
	}
	
}
