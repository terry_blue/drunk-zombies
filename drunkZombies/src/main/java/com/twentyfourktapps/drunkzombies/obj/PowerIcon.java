package com.twentyfourktapps.drunkzombies.obj;

import java.lang.ref.SoftReference;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.twentyfourktapps.drunkzombies.R;
import com.twentyfourktapps.drunkzombies.units.Bullet;
import com.twentyfourktapps.drunkzombies.units.PowersMain;
import com.twentyfourktapps.drunkzombies.units.SingleUnit.TickCount;
import com.twentyfourktapps.drunkzombies.util.Util;

public class PowerIcon extends RelativeLayout implements OnTouchListener {

	final String TAG = "PowerIcon";
	public int curPowerIcon = 0; 
	
	final static int BASE_CHARGE_TIME = 400;
	final static int CHARGE_CONSTANT = 10;	
	
	boolean isInitialized = false;
	public boolean isAvailable, isReady, isSelected = false;
	Bitmap iconBmp;
	Rect srcRect, destRect, overlaySrcRect, overlayDestRect;
	RectF oval;
	float sweepAngle = 0;
	TickCount chargingTC;
	
	Paint iconPaint, timerPaint;
	SoftReference<PowersMain> powerMain;
	
	public PowerIcon(Context context, AttributeSet attrs) {
		super(context, attrs);		
		setWillNotDraw(false);
		
		iconPaint = new Paint();
		timerPaint = new Paint();
		timerPaint.setColor(Color.YELLOW);
		
		destRect = new Rect(0, 0, 0, 0);	
		overlayDestRect = new Rect(0,0,0,0);
		oval = new RectF(destRect);		
	}
	
	public void initialize(SoftReference<PowersMain> powersMain) {
		powerMain = powersMain;
		setOnTouchListener(this);
		
		boolean[] availability = new boolean[] { PlayerLevels.curPowerYellow > 0, PlayerLevels.curPowerGreen > 0, PlayerLevels.curPowerBlue > 0 };
		int[] drawableIds = new int[] { R.drawable.powericon_peanut, R.drawable.powericon_coktailbottle, R.drawable.powericon_icecube };
		int[] typeIds = new int[] { Bullet.YELLOW_POWER, Bullet.GREEN_POWER, Bullet.BLUE_POWER };
		int[] cooldownLevels = new int[] { PlayerLevels.curPowerYellow, PlayerLevels.curPowerGreen, PlayerLevels.curPowerBlue };
		int pos = 0;
		
		switch(getId()) {
		case R.id.gamefragment_powericon_blue: pos++;
		case R.id.gamefragment_powericon_green: pos++;
		case R.id.gamefragment_powericon_yellow: 
			Bitmap b = BitmapFactory.decodeResource(getResources(), drawableIds[pos]);
			curPowerIcon = typeIds[pos];
			iconBmp = Bitmap.createScaledBitmap(b, b.getWidth() / 2, b.getWidth() / 2, true);
			chargingTC = new TickCount(BASE_CHARGE_TIME - (int)((float)cooldownLevels[pos] * 11.25f));
			isAvailable = availability[pos];
			startCharging();
			isInitialized = true;
		}
		
		overlaySrcRect = new Rect(0, 0, StaticResources.getPowerIconOverlay(getContext()).getWidth(), StaticResources.getPowerIconOverlay(getContext()).getHeight());
	}
	
	

	
	@Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int desiredHSpec = MeasureSpec.makeMeasureSpec(100, MeasureSpec.EXACTLY);
		final int desiredWSpec = MeasureSpec.makeMeasureSpec(100, MeasureSpec.EXACTLY);
		
		setMeasuredDimension(desiredWSpec, desiredHSpec);
		
		destRect.set(0, 0, getWidth(), getHeight());
		overlayDestRect.set(0, 0, getWidth(), getHeight());
		oval.set(0, 0, getWidth(), getHeight());
		float padding = 8;
		destRect.inset((int)Util.convertDpToPixel(padding, getContext()), (int)Util.convertDpToPixel(padding, getContext()));
    }
	
	public void update() {
		if(!isReady) {
			chargingTC.update();		
			sweepAngle = chargingTC.getPercentComplete() * 360f;
			
			if(chargingTC.isLive && chargingTC.isExpired()) {
				isReady = true;
			}
			
			invalidate();
		}
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		if(isInitialized&& isAvailable) {
			canvas.drawArc(oval, -90, sweepAngle, true, timerPaint);
			canvas.drawBitmap(iconBmp, srcRect, destRect, timerPaint);
			canvas.drawBitmap(StaticResources.getPowerIconOverlay(getContext()), overlaySrcRect, overlayDestRect, timerPaint);
		}
	}
    
	
	public void startCharging() {
		isReady = false;
		chargingTC.start();
	}
	public void select() {
		isSelected = true;
		invalidate();
	}
	public void deselect() {
		isSelected = false;
		invalidate();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(isReady) {
			final PowersMain TARGET = powerMain.get();
			switch(event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				select();				
				break;
			case MotionEvent.ACTION_MOVE:
				TARGET.updateTargetCentrePoint(event.getX() + getLeft() + ((LinearLayout)getParent()).getLeft()
												, event.getY() + getTop() + ((LinearLayout)getParent()).getTop());
				TARGET.startTargeting();				
				break;
			case MotionEvent.ACTION_UP:
				deselect();
				TARGET.stopTargeting();
				PointF adjTargetPoint = new PointF(event.getX() + getLeft() + ((LinearLayout)getParent()).getLeft() + (TARGET.targetBitmap.getWidth() / 2)
													, event.getY() + getTop() + ((LinearLayout)getParent()).getTop() + (TARGET.targetBitmap.getHeight() / 2));
				TARGET.fire(curPowerIcon, adjTargetPoint, adjTargetPoint, 0, true);
				startCharging();				
				break;
			}
		}
		return true;
	}
}

