package com.twentyfourktapps.drunkzombies.obj;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.ActivityUpgrades;
import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.R;
import com.twentyfourktapps.drunkzombies.obj.Turret.TurretType;
import com.twentyfourktapps.drunkzombies.units.Bullet;
import com.twentyfourktapps.drunkzombies.util.ServerUtil;


public class TurretSelector extends RelativeLayout {

	int curTURRET = -1;
	public boolean isLocked = true;
	public int curSpot;
	int cost;
	ActivityUpgrades activity;
	
	public TurretSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.turret_selector, this);		
	}
	
	public void initialise(int TURRET, ActivityUpgrades ref, SharedPreferences sp) {
		curTURRET = TURRET;
		activity = ref;
		
		setOnClickListener(ref);
		
		if(!isInEditMode()) {			
			ImageView charIV = (ImageView)findViewById(R.id.turretselector_imageview_char);
			
			
			if(curTURRET == sp.getInt(Consts.SP_KEY_TURRET_SPOT_1, -2)) {
				curSpot = 1;
				select(1);
			}
			else if(curTURRET == sp.getInt(Consts.SP_KEY_TURRET_SPOT_2, -2)) {
				curSpot = 2;
				select(2);
			}
			else {
				curSpot = -1;
				select(0);
			}
			
			switch(curTURRET) {
			case Bullet.BLUE: {
				charIV.setImageBitmap(StaticResources.getTurretBitmap(getContext(), getTurretType()));
				if(sp.getBoolean(Consts.SP_KEY_TURRET_BLUE_BOUGHT, false)) setLocked(false);
				else setLocked(true);
				cost = Consts.price_TURRET_BLUE;
				break;
			}
			case Bullet.GREEN: {
				charIV.setImageBitmap(StaticResources.getTurretBitmap(getContext(), getTurretType()));
				if(sp.getBoolean(Consts.SP_KEY_TURRET_GREEN_BOUGHT, false)) setLocked(false);
				else setLocked(true);
				cost = Consts.price_TURRET_GREEN;
				break;
			}
			case Bullet.YELLOW: {				
				charIV.setImageBitmap(StaticResources.getTurretBitmap(getContext(), getTurretType()));
				if(sp.getBoolean(Consts.SP_KEY_TURRET_YELLOW_BOUGHT, false)) setLocked(false);
				else setLocked(true);
				cost = Consts.price_TURRET_YELLOW;
				break;
			}
			}
			
			((TextView)findViewById(R.id.turretselector_textview_price)).setText(Integer.toString(cost));	
		}
	}
	
	
	public void setLocked(boolean locked) {
		isLocked = locked;
		
		ImageView lockedIV = (ImageView)findViewById(R.id.turretselector_imageview_locked);
		LinearLayout buyLL = (LinearLayout)findViewById(R.id.turretselector_linearlayout_price_container);
		
		if(isLocked){
			lockedIV.setVisibility(View.VISIBLE);
			buyLL.setVisibility(View.VISIBLE);
		}
		else {
			lockedIV.setVisibility(View.GONE);
			buyLL.setVisibility(View.GONE);
		}
	}
	public void select(int spotNum) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		SharedPreferences.Editor editor = sp.edit();
		LinearLayout rootLayout = (LinearLayout)findViewById(R.id.turretselector_linearlayout_root);
		
		switch(spotNum) {
		case 1: {
			editor.putInt(Consts.SP_KEY_TURRET_SPOT_1, curTURRET);
			rootLayout.setBackgroundResource(R.drawable.border_selected_top);
			curSpot = 1;
			break;
		}
		case 2: {
			editor.putInt(Consts.SP_KEY_TURRET_SPOT_2, curTURRET);
			rootLayout.setBackgroundResource(R.drawable.border_selected_bottom);
			curSpot = 2;
			break;
		}
		default: {
			rootLayout.setBackgroundResource(0);
			if(curSpot == 1){
				editor.putInt(Consts.SP_KEY_TURRET_SPOT_1, -1);
			}
			else if(curSpot == 2) {
				editor.putInt(Consts.SP_KEY_TURRET_SPOT_2, -1);
			}
			curSpot = 0;
		}
		}
		
		editor.commit();
		
		activity.setAvailableTurretIndicators();
	}
	public void deSelect() {
		LinearLayout rootLayout = (LinearLayout)findViewById(R.id.turretselector_linearlayout_root);
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		SharedPreferences.Editor editor = sp.edit();
		if(curSpot == 1) editor.putInt(Consts.SP_KEY_TURRET_SPOT_1, -1);
		else if(curSpot == 2) editor.putInt(Consts.SP_KEY_TURRET_SPOT_2, -1);
		editor.commit();
		rootLayout.setBackgroundResource(R.drawable.border_none);
		activity.setAvailableTurretIndicators();
		curSpot = 0;
		
	}	

	public void buy() {	
		
		if(isLocked) {
			String[] boughtKeys = new String[] { Consts.SP_KEY_TURRET_YELLOW_BOUGHT, Consts.SP_KEY_TURRET_GREEN_BOUGHT, Consts.SP_KEY_TURRET_BLUE_BOUGHT };
			int[] tokenCounts = new int[] { Consts.price_TURRET_YELLOW, Consts.price_TURRET_GREEN, Consts.price_TURRET_BLUE };
			int pos = 0;
			switch(curTURRET) {
				case Bullet.BLUE: pos++;
				case Bullet.GREEN: pos++;
				case Bullet.YELLOW: 
			}
			
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
			SharedPreferences.Editor editor = sp.edit();
			
			editor.putInt(Player.KEY_TOKENS, PlayerLevels.curLevelTokens - tokenCounts[pos]);
			editor.putBoolean(boughtKeys[pos], true);
			editor.commit();			
			
			setLocked(false);
			activity.setHeaderLevels();
			ServerUtil.sendDZdata(getContext());				
		}
	}	

	public TurretType getTurretType() {
		switch(curTURRET) {
		case Bullet.BLUE: return TurretType.BLUE;
		case Bullet.GREEN: return TurretType.GREEN;
		case Bullet.YELLOW: return TurretType.YELLOW;
		}
		
		return null;
	}
}
