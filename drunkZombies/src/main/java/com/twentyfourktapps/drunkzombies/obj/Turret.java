package com.twentyfourktapps.drunkzombies.obj;

import android.content.Context;
import android.util.AttributeSet;

import com.twentyfourktapps.drunkzombies.units.Bullet;

public class Turret extends Player {

	public static enum TurretType { PLAYER, GREEN, BLUE, YELLOW };
	PlayerLevels playerLevels;
	
	
	double curRotationDeg, destRotationDeg;
	double rotationSpeed = 0.25;
	
	public Turret(Context context, AttributeSet attrs) {
		super(context, attrs);
		if(!isInEditMode()) {
			playerLevels = new PlayerLevels(context);
		}
		
	}

	@Override
	public void initialize(int TURRETtype) {
		super.initialize(TURRETtype);
		rotationSpeed = playerLevels.getTurretRotateSpeed(curTurretColor);
		switch(curTurretColor) {
		case -1:  //Empty turret space
			isLive = false;
			break;
		
		case Bullet.GREEN:		
		case Bullet.BLUE:
		case Bullet.YELLOW:			
			isLive = true;
			break;
		
		}	
	}
	
	public void update() {
		if((int)rotationDeg < (int)destRotationDeg) rotationDeg += rotationSpeed;
		else if ((int)rotationDeg > (int)destRotationDeg) rotationDeg -= rotationSpeed;
	}
	
	@Override
	public void rotate(double inDegs) {
		destRotationDeg = inDegs;		
		
		double degDiff = rotationDeg - destRotationDeg;
		
		if(degDiff < 3 && degDiff > -3) {
			super.rotate(inDegs);
		} 
	}
	
	@Override
	public boolean toFire() {
		double degDiff = rotationDeg - destRotationDeg;
		return (((degDiff < 1 && degDiff > -1) || curTurretColor == Bullet.YELLOW) && super.toFire());
		
			/*
		if(nextFireTime < System.currentTimeMillis() && isLive
				&& ((degDiff < 1 && degDiff > -1) || curTurretColor == Bullet.YELLOW)) {
			nextFireTime = gapBetweenFire + System.currentTimeMillis();
			return true;
		}
		return false;
		*/
	}
}
