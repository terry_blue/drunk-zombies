package com.twentyfourktapps.drunkzombies.obj;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.R;
import com.twentyfourktapps.drunkzombies.obj.Turret.TurretType;
import com.twentyfourktapps.drunkzombies.units.Bullet;
import com.twentyfourktapps.drunkzombies.units.SingleUnit.UnitType;
import com.twentyfourktapps.drunkzombies.units.Zombie;
import com.twentyfourktapps.drunkzombies.util.Util;
import com.twentyfourktapps.libs.obj.AnimationSpriteSheet;

public class StaticResources {

	public static String TAG = "StaticResources";
	
	public static boolean isInitialized = false;
	
	//Animation Sprite Sheets
	private static AnimationSpriteSheet[] bulletSpriteSheets = null;
	private static AnimationSpriteSheet[] zombieSpriteSheets = null;
	private static AnimationSpriteSheet[] bossSpriteSheets = null;	
	private static AnimationSpriteSheet powSpriteSheet = null;
	private static AnimationSpriteSheet spewCloudSheet = null;	
	
	//Drawables
	private static Drawable coin, coin_dz = null;
	
	//Bitmaps
	private static Bitmap turretGreenFull, turretBlueFull, turretYellowFull = null;
	private static Bitmap powerIconOverlay = null;
	public static Bitmap getPowerIconOverlay(Context context) {
		if(!isInitialized)
			initialize(context);
		
		return powerIconOverlay;
	}
	
	//Typefaces(fonts)
	public static enum Font { FEAST, KREE, KREEW, TREASURE };
	private static Typeface feastTTF, kreewTTF, kreeTTF, treasureTTF = null;
	public static Typeface getFont(Context context, Font font) {
		if(!isInitialized)
			initialize(context);
		
		switch(font) {
		case FEAST: return feastTTF;
		case KREE: return kreeTTF;
		case KREEW: return kreewTTF;
		case TREASURE: return treasureTTF;
		}
		
		return null;
	}
	
	
	public static void initialize(Context cxt) {
		Log.i(TAG, "initialize");
		if(isInitialized) releaseBitmaps(); 
		Resources res = cxt.getResources();
		AssetManager assets = cxt.getAssets();
		
		bulletSpriteSheets = new AnimationSpriteSheet[9];
		bulletSpriteSheets[Bullet.BLUE] = new AnimationSpriteSheet(res, R.drawable.lasor3, 1);
		bulletSpriteSheets[Bullet.GREEN] = new AnimationSpriteSheet(res, R.drawable.rpg_bullet, 1);
		bulletSpriteSheets[Bullet.GREEN_BOOM] = new AnimationSpriteSheet(res, R.drawable.fire2, 1);
		bulletSpriteSheets[Bullet.YELLOW] = new AnimationSpriteSheet(res, R.drawable.fire2, 1);
		bulletSpriteSheets[Bullet.PLAYER] = new AnimationSpriteSheet(res, R.drawable.bullet_ak, 1);
		bulletSpriteSheets[Bullet.BLUE_POWER] = new AnimationSpriteSheet(res, R.drawable.powericon_icecube, 1);
		bulletSpriteSheets[Bullet.GREEN_POWER] = new AnimationSpriteSheet(res, R.drawable.powericon_coktailbottle, 1);
		bulletSpriteSheets[Bullet.YELLOW_POWER] = new AnimationSpriteSheet(res, R.drawable.powericon_peanut, 1);		
		
		zombieSpriteSheets = new AnimationSpriteSheet[9];
		zombieSpriteSheets[Zombie.BOB] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_001, 8);
		zombieSpriteSheets[Zombie.RACHEL] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_002, 8);
		zombieSpriteSheets[Zombie.GLOB] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_003, 8);
		zombieSpriteSheets[Zombie.CHEF] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_004, 8);
		zombieSpriteSheets[Zombie.PRINCESS] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_005, 8);
		zombieSpriteSheets[Zombie.HARRY] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_006, 8);
		zombieSpriteSheets[Zombie.KID_DED] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_007, 8);
		zombieSpriteSheets[Zombie.TWERP] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_008, 8);
		zombieSpriteSheets[Zombie.FRANK] = new AnimationSpriteSheet(res, R.drawable.zombie_sprite_009, 8);		
		
		bossSpriteSheets = new AnimationSpriteSheet[3];
		bossSpriteSheets[0] = new AnimationSpriteSheet(res, R.drawable.zombie_boss_2, 8);
		bossSpriteSheets[1] = new AnimationSpriteSheet(res, R.drawable.zombie_boss_3, 8);
		bossSpriteSheets[2] = new AnimationSpriteSheet(res, R.drawable.zombie_boss_1, 8);
		
		powSpriteSheet = new AnimationSpriteSheet(res, R.drawable.pow, 1);
		spewCloudSheet = new AnimationSpriteSheet(res, R.drawable.cloud, 1);
		
		//Bitmaps
		turretGreenFull = BitmapFactory.decodeResource(res, R.drawable.turret_green);
		turretBlueFull = BitmapFactory.decodeResource(res, R.drawable.turret_blue);
		turretYellowFull = BitmapFactory.decodeResource(res, R.drawable.turret_yellow);
		powerIconOverlay = BitmapFactory.decodeResource(res, R.drawable.overlay_dash_orange1);
		
		//Drawables
		coin = ContextCompat.getDrawable(cxt, R.drawable.coin);
		coin_dz = ContextCompat.getDrawable(cxt, R.drawable.coin_dz);
		
		//Typefaces
		feastTTF = Typeface.createFromAsset(assets, "fonts/feast.ttf");
		kreewTTF = Typeface.createFromAsset(assets, "fonts/kreew.ttf");
		kreeTTF = Typeface.createFromAsset(assets, "fonts/kree.ttf");
		treasureTTF = Typeface.createFromAsset(assets, "fonts/treasure.ttf");
		
		isInitialized = true;
	}
	
	public static Bitmap getBitmap(Context context, UnitType unitType, int index) {
		if(!isInitialized)
			initialize(context);
		
		switch(unitType) {
		case ZOMBIE: return zombieSpriteSheets[index].BITMAP;
		case BOSS: return bossSpriteSheets[index].BITMAP;
		case BULLET: return bulletSpriteSheets[index].BITMAP;
		case POW: return powSpriteSheet.BITMAP;
		case SPEW: return spewCloudSheet.BITMAP;
		}
		
		return null;
	}
	public static Bitmap getTurretBitmap(Context context, TurretType type) {
		if(!isInitialized)
			initialize(context);
		
		switch(type) {
		case YELLOW: return turretYellowFull;
		case BLUE: return turretBlueFull;
		case GREEN: return turretGreenFull;
		}
		
		return null;
	}
	
	public static Drawable getCoin(Context context) {
		if(!isInitialized)
			initialize(context);		
		return coin;
	}
	public static Drawable getDZCoin(Context context) {
		if(!isInitialized)
			initialize(context);
		return coin_dz;
	}
	
	
	
	public static int getFrameWidth(Context context, UnitType type, int index) {
		if(!isInitialized)
			initialize(context);
		
		switch(type) {
		case ZOMBIE: return (int)Util.convertPixelsToDp(zombieSpriteSheets[index].getFrameWidth(), context);
		case BOSS: return (int)Util.convertPixelsToDp(bossSpriteSheets[index].getFrameWidth(), context);
		case BULLET: return (int)Util.convertPixelsToDp(bulletSpriteSheets[index].getFrameWidth(), context);
		case POW: return(int)Util.convertPixelsToDp(powSpriteSheet.getFrameWidth(), context);
		case SPEW: return(int)Util.convertPixelsToDp(spewCloudSheet.getFrameWidth(), context);
		}		
		
		return 0;
	}
	
	public static int getBitmapHeight(Context context, UnitType type, int index) {
		if(context == null) Log.e(TAG, "context = null");
		if(!isInitialized)
			initialize(context);
		
		switch(type) {
		case ZOMBIE: return (int)Util.convertPixelsToDp(zombieSpriteSheets[index].BITMAP.getHeight(), context);
		case BOSS: return (int)Util.convertPixelsToDp(bossSpriteSheets[index].BITMAP.getHeight(), context);
		case BULLET: return (int)Util.convertPixelsToDp(bulletSpriteSheets[index].BITMAP.getHeight(), context);
		case POW: return (int)Util.convertPixelsToDp(powSpriteSheet.BITMAP.getHeight(), context);
		case SPEW: return (int)Util.convertPixelsToDp(spewCloudSheet.BITMAP.getHeight(), context);
		}
		
		return 0;
	}
	
	public static int getMaxFrameCount(Context context, UnitType unitType, int index) {
		if(!isInitialized)
			initialize(context);
			
		switch(unitType) {
		case ZOMBIE: return zombieSpriteSheets[index].MAX_FRAMES;
		case BOSS: return bossSpriteSheets[index].MAX_FRAMES;
		case BULLET: return bulletSpriteSheets[index].MAX_FRAMES;
		case POW: return powSpriteSheet.MAX_FRAMES;
		case SPEW: return spewCloudSheet.MAX_FRAMES;
		}		
		
		return 0;
	}
	
	public static Rect getSourceRect(Context context, UnitType type, int index, int frame) {
		if(!isInitialized)
			initialize(context);
		
		switch(type) {
		case ZOMBIE: return zombieSpriteSheets[index].getSrcRectForFrame(frame);
		case BOSS: return bossSpriteSheets[index].getSrcRectForFrame(frame);
		case BULLET: return bulletSpriteSheets[index].getSrcRectForFrame(frame);
		case POW: return powSpriteSheet.getSrcRectForFrame(frame);
		case SPEW: return spewCloudSheet.getSrcRectForFrame(frame);
		}
		
		return null;
	}
	
	public static void releaseBitmaps() {
		isInitialized = false;
		ArrayList<AnimationSpriteSheet> allSpriteSheets = new ArrayList<>();
		if(zombieSpriteSheets != null) { for(AnimationSpriteSheet sS : zombieSpriteSheets) allSpriteSheets.add(sS); }
		if(bossSpriteSheets != null) { for(AnimationSpriteSheet sS : bossSpriteSheets) allSpriteSheets.add(sS); }
		if(bulletSpriteSheets != null) { for(AnimationSpriteSheet sS : bulletSpriteSheets) allSpriteSheets.add(sS); }
		
		for(AnimationSpriteSheet spriteSheet : allSpriteSheets) {
			if(spriteSheet != null) 
				spriteSheet.releaseBitmap();
		}
		if(powSpriteSheet != null) { powSpriteSheet.releaseBitmap(); }
		if(spewCloudSheet != null) { spewCloudSheet.releaseBitmap(); }
		
	}
}
