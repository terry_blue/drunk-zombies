package com.twentyfourktapps.drunkzombies.obj;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.R;
import com.twentyfourktapps.drunkzombies.units.Bullet;
import com.twentyfourktapps.drunkzombies.util.Util;

public class PlayerLevels {

	final static String TAG = "PlayerLevels";
	
	
	
	public static boolean isInitialized = false;
	
	public static int curLevelWave;
	public static int curLevelCoins;
	public static int curLevelTokens;
	public static int curStrength;
	public static int curAgility;
	public static int curCritical;
	public static int curPoison;
	public static int curKick;
	public static int curMultiShot;
	public static int curBottlesRed, curBottlesOrange, curBottlesGreen;
	public static int curBottleQuality;
	public static int curBottleThickness;
	
	public static int curTurretBlue, curTurretGreen, curTurretYellow;
	public static int curTurretBlueROF, curTurretGreenROF, curTurretYellowROF;
	public static int curTurretBlueDamage, curTurretGreenDamage, curTurretYellowDamage;
	public static int curPowerBlueDamage, curPowerGreenDamage, curPowerYellowDamage;
	public static int curPowerBlue, curPowerGreen, curPowerYellow;
	public static int curPowerBlueRadius, curPowerGreenRadius, curPowerYellowRadius;
	
	public static SharedPreferences sp;
	
	
	public static int baseBulletDamage;
	
	final boolean IS_LOGGED_IN;
	
	
	
	public static void init(Context c) {
		sp = PreferenceManager.getDefaultSharedPreferences(c);		
		
		curLevelWave = sp.getInt(Player.KEY_WAVE, 1);
		curLevelCoins = sp.getInt(Player.KEY_COINS, 1000);
		curLevelTokens = sp.getInt(Player.KEY_TOKENS, 10);
		
		curStrength = sp.getInt(Player.KEY_LEVEL_STRENGTH, 1);
		curAgility = sp.getInt(Player.KEY_LEVEL_AGILITY, 1);
		curCritical = sp.getInt(Player.KEY_LEVEL_CRITICAL, 0);
		curPoison = sp.getInt(Player.KEY_LEVEL_POISON, 0);
		curKick = sp.getInt(Player.KEY_LEVEL_KICK, 0);
		curMultiShot = sp.getInt(Player.KEY_LEVEL_MULTI, 0);
		
		
		curTurretBlue = sp.getInt(Player.KEY_LEVEL_TURRET_BLUE, 0);
		curTurretGreen = sp.getInt(Player.KEY_LEVEL_TURRET_GREEN, 0);
		curTurretYellow = sp.getInt(Player.KEY_LEVEL_TURRET_YELLOW, 0);
		curTurretBlueROF = sp.getInt(Player.KEY_LEVEL_TURRET_BLUE_ROF, 0);
		curTurretGreenROF = sp.getInt(Player.KEY_LEVEL_TURRET_GREEN_ROF, 0);
		curTurretYellowROF = sp.getInt(Player.KEY_LEVEL_TURRET_YELLOW_ROF, 0);
		curTurretBlueDamage = sp.getInt(Player.KEY_LEVEL_TURRET_BLUE_DAMAGE, 0);
		curTurretGreenDamage = sp.getInt(Player.KEY_LEVEL_TURRET_GREEN_DAMAGE, 0);
		curTurretYellowDamage = sp.getInt(Player.KEY_LEVEL_TURRET_YELLOW_DAMAGE, 0);
		
		curPowerBlueDamage = sp.getInt(Player.KEY_LEVEL_POWER_BLUE_DAMAGE, 0);
		curPowerGreenDamage = sp.getInt(Player.KEY_LEVEL_POWER_GREEN_DAMAGE, 0);
		curPowerYellowDamage = sp.getInt(Player.KEY_LEVEL_POWER_YELLOW_DAMAGE, 0);
		curPowerBlue = sp.getInt(Player.KEY_LEVEL_POWER_BLUE, 0);
		curPowerGreen = sp.getInt(Player.KEY_LEVEL_POWER_GREEN, 0);
		curPowerYellow = sp.getInt(Player.KEY_LEVEL_POWER_YELLOW, 0);
		curPowerBlueRadius = sp.getInt(Player.KEY_LEVEL_POWER_BLUE_RADIUS, 0);
		curPowerGreenRadius = sp.getInt(Player.KEY_LEVEL_POWER_GREEN_RADIUS, 0);
		curPowerYellowRadius = sp.getInt(Player.KEY_LEVEL_POWER_YELLOW_RADIUS, 0);
		
		
		curBottlesRed = sp.getInt(Player.KEY_LEVEL_BOTTLE_COUNT_RED, 0);
		curBottlesOrange =sp.getInt(Player.KEY_LEVEL_BOTTLE_COUNT_ORANGE, 0);
		curBottlesGreen = sp.getInt(Player.KEY_LEVEL_BOTTLE_COUNT_GREEN, 0);
		curBottleQuality = sp.getInt(Player.KEY_LEVEL_BOTTLE_QUALITY, 0);
		curBottleThickness = sp.getInt(Player.KEY_LEVEL_BOTTLE_THICKNESS, 0);
		
		
		baseBulletDamage = Consts.uLevel_DAMAGE[curStrength];
		isInitialized = true;
	}
	
	public PlayerLevels(Context c) {
		IS_LOGGED_IN = Util.isUserLoggedIn(c);		
		if(!isInitialized)
			init(c);
	}
	
	public static int getLevel(String playerKey) {
		if(playerKey.equals(Player.KEY_LEVEL_AGILITY)) return curAgility;
		else if(playerKey.equals(Player.KEY_LEVEL_STRENGTH)) return curStrength;
		else if(playerKey.equals(Player.KEY_LEVEL_CRITICAL)) return curCritical;
		else if(playerKey.equals(Player.KEY_LEVEL_KICK)) return curKick;
		else if(playerKey.equals(Player.KEY_LEVEL_POISON)) return curPoison;
		else if(playerKey.equals(Player.KEY_LEVEL_MULTI)) return curMultiShot;
		else if(playerKey.equals(Player.KEY_LEVEL_BOTTLE_COUNT_RED)) return curBottlesRed;
		else if(playerKey.equals(Player.KEY_LEVEL_BOTTLE_COUNT_ORANGE)) return curBottlesOrange;
		else if(playerKey.equals(Player.KEY_LEVEL_BOTTLE_COUNT_GREEN)) return curBottlesGreen;
		else if(playerKey.equals(Player.KEY_LEVEL_BOTTLE_QUALITY)) return curBottleQuality;
		else if(playerKey.equals(Player.KEY_LEVEL_BOTTLE_THICKNESS)) return curBottleThickness;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_BLUE)) return curTurretBlue;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_GREEN)) return curTurretGreen;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_YELLOW)) return curTurretYellow;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_BLUE_ROF)) return curTurretBlueROF;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_GREEN_ROF)) return curTurretGreenROF;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_YELLOW_ROF)) return curTurretYellowROF;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_BLUE_DAMAGE)) return curTurretBlueDamage;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_GREEN_DAMAGE)) return curTurretGreenDamage;
		else if(playerKey.equals(Player.KEY_LEVEL_TURRET_YELLOW_DAMAGE)) return curTurretYellowDamage;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_BLUE_DAMAGE)) return curPowerBlueDamage;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_GREEN_DAMAGE)) return curPowerGreenDamage;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_YELLOW_DAMAGE)) return curPowerYellowDamage;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_BLUE)) return curPowerBlue;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_GREEN)) return curPowerGreen;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_YELLOW)) return curPowerYellow;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_BLUE_RADIUS)) return curPowerBlueRadius;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_GREEN_RADIUS)) return curPowerGreenRadius;
		else if(playerKey.equals(Player.KEY_LEVEL_POWER_YELLOW_RADIUS)) return curPowerYellowRadius;
		else if(playerKey.equals(Player.KEY_COINS)) return curLevelCoins;
		else if(playerKey.equals(Player.KEY_TOKENS)) return curLevelTokens;
		return -1;
	}
	
	
	public static String getKey(int vID) {
		switch(vID) {
		case R.id.upgrades_upgradebutton_strength: return Player.KEY_LEVEL_STRENGTH;
		case R.id.upgrades_upgradebutton_agility: return Player.KEY_LEVEL_AGILITY;
		case R.id.upgrades_upgradebutton_critical: return Player.KEY_LEVEL_CRITICAL;
		case R.id.upgrades_upgradebutton_poison: return Player.KEY_LEVEL_POISON;
		case R.id.upgrades_upgradebutton_kick: return Player.KEY_LEVEL_KICK;
		case R.id.upgrades_upgradebutton_multishot: return Player.KEY_LEVEL_MULTI;
		case R.id.upgrades_upgradebutton_bottlequality: return Player.KEY_LEVEL_BOTTLE_QUALITY;
		case R.id.upgrades_upgradebutton_bottlethickness: return Player.KEY_LEVEL_BOTTLE_THICKNESS;
		case R.id.upgrades_upgradebutton_bottlecount_red: return Player.KEY_LEVEL_BOTTLE_COUNT_RED;
		case R.id.upgrades_upgradebutton_bottlecount_orange: return Player.KEY_LEVEL_BOTTLE_COUNT_ORANGE;
		case R.id.upgrades_upgradebutton_bottlecount_green: return Player.KEY_LEVEL_BOTTLE_COUNT_GREEN;
		case R.id.upgrades_upgradebutton_turretblue: return Player.KEY_LEVEL_TURRET_BLUE;
		case R.id.upgrades_upgradebutton_turretgreen: return Player.KEY_LEVEL_TURRET_GREEN;
		case R.id.upgrades_upgradebutton_turretyellow: return Player.KEY_LEVEL_TURRET_YELLOW;
		case R.id.upgrades_upgradebutton_turretblue_agility: return Player.KEY_LEVEL_TURRET_BLUE_ROF;
		case R.id.upgrades_upgradebutton_turretgreen_agility: return Player.KEY_LEVEL_TURRET_GREEN_ROF;
		case R.id.upgrades_upgradebutton_turretyellow_agility: return Player.KEY_LEVEL_TURRET_YELLOW_ROF;
		case R.id.upgrades_upgradebutton_turretblue_damage: return Player.KEY_LEVEL_TURRET_BLUE_DAMAGE;
		case R.id.upgrades_upgradebutton_turretgreen_damage: return Player.KEY_LEVEL_TURRET_GREEN_DAMAGE;
		case R.id.upgrades_upgradebutton_turretyellow_damage: return Player.KEY_LEVEL_TURRET_YELLOW_DAMAGE;
		case R.id.upgrades_upgradebutton_powers_blue: return Player.KEY_LEVEL_POWER_BLUE;
		case R.id.upgrades_upgradebutton_powers_blue_powerlevel: return Player.KEY_LEVEL_POWER_BLUE_DAMAGE;
		case R.id.upgrades_upgradebutton_powers_blue_radius: return Player.KEY_LEVEL_POWER_BLUE_RADIUS;
		case R.id.upgrades_upgradebutton_powers_green: return Player.KEY_LEVEL_POWER_GREEN;
		case R.id.upgrades_upgradebutton_powers_green_powerlevel: return Player.KEY_LEVEL_POWER_GREEN_DAMAGE;
		case R.id.upgrades_upgradebutton_powers_green_radius: return Player.KEY_LEVEL_POWER_GREEN_RADIUS;
		case R.id.upgrades_upgradebutton_powers_yellow: return Player.KEY_LEVEL_POWER_YELLOW;
		case R.id.upgrades_upgradebutton_powers_yellow_powerlevel: return Player.KEY_LEVEL_POWER_YELLOW_DAMAGE;
		case R.id.upgrades_upgradebutton_powers_yellow_radius: return Player.KEY_LEVEL_POWER_YELLOW_RADIUS;
		
		}
		return null;
	}
	
	
	public static int getBulletsPerGroup(int BULLETtype) {
		if(isInitialized) {
			switch(BULLETtype) {
			case Bullet.PLAYER: 
				return (int)Consts.uLevel_MULTISHOT[curMultiShot];			
			}
		}
		return 1;
		
	}
	public static int getCriticalChance() {
		return Consts.uLevel_CRITICAL[curCritical];
	}
	public static float getBulletDamage(int BULLETtype) {
		float bulletDamage  = 0;
		switch(BULLETtype) {
		case Bullet.BLUE: {
			bulletDamage = Consts.uLevel_TURRETBLUE_DAMAGE[curTurretBlueDamage];
			break;
		}
		case Bullet.GREEN_BOOM:
		case Bullet.GREEN: {
			bulletDamage = Consts.uLevel_TURRETGREEN_DAMAGE[curTurretGreenDamage];
			break;
		}
		case Bullet.YELLOW: {
			bulletDamage = Consts.uLevel_TURRETYELLOW_DAMAGE[curTurretYellowDamage];
			break;
		}
		case Bullet.PLAYER: {
			bulletDamage = Consts.uLevel_DAMAGE[curStrength];
			
			//Adjustment for multi shot
			if(curMultiShot > 0) {
				float curDamageAdj = (Consts.uLevel_MULTISHOT[curMultiShot] + 0.005f) - (int)Consts.uLevel_MULTISHOT[curMultiShot];
				bulletDamage = (int)(bulletDamage * curDamageAdj);
				//Log.e(TAG, "Strength value = " + Float.toString(Consts.uLevel_DAMAGE[curStrength]) + ", damageAdj = " + Float.toString(curDamageAdj) + ", new baseBulletDamage = " + Integer.toString(baseBulletDamage));
			}
			break;
		}
		case Bullet.BLUE_POWER: {
			bulletDamage = Consts.uLevel_POWERBLUE_DAMAGE[curPowerBlueDamage];
			break;
		}
		case Bullet.GREEN_POWER: {
			bulletDamage = Consts.uLevel_POWERGREEN_DAMAGE[curPowerGreenDamage];
			break;
		}
		case Bullet.YELLOW_POWER: {
			bulletDamage = Consts.uLevel_POWERYELLOW_DAMAGE[curPowerYellowDamage];
			break;
		}
		}
		
		return bulletDamage;
	}
	
	/*
	//DEPRECIATED
	public void storeNewLevels(String key, int newAmount) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = sp.edit();
		
		editor.putInt(key, newAmount);
		editor.commit();
	}
	*/
	
	public static void storeNewLevels(String key, int amount) {
		SharedPreferences.Editor edit = sp.edit();
		
		edit.putInt(key, amount);
		edit.commit();
	}
	
	public float getTurretRotateSpeed(int TURRETtype) {
		
		return 3;
	}
	
	public static int getShardCount(int TURRET) {
		switch(TURRET) {
		case Bullet.GREEN_BOOM: return curTurretGreen;
		case Bullet.YELLOW_POWER: return (int)Consts.uLevel_POWERYELLOW_LENGTH[curPowerYellow];
		case Bullet.BLUE_POWER:
		case Bullet.GREEN_POWER: return 30;
		
		}
		return 0;
	}
	public static int getMaxBulletTicks(int TURRET) {
		switch(TURRET) {
		case Bullet.YELLOW_POWER: return 2147483642;
		case Bullet.GREEN_POWER: return (curPowerGreenRadius + 1) * 10;
		case Bullet.BLUE_POWER: return (curPowerBlueRadius + 1) * 10;
		case Bullet.GREEN_BOOM: return curTurretGreen;
		}
		
		return 2147483642;
	}
	public static int getMaxExplodeTicks(int TURRET) {
		switch(TURRET) {
		case Bullet.YELLOW_POWER: return (curPowerYellowRadius + 1) * 10;
		case Bullet.GREEN_POWER: return (curPowerGreenRadius + 1) * 10;
		case Bullet.BLUE_POWER: return (curPowerBlueRadius + 1) * 10;
		case Bullet.YELLOW: return Consts.uLevel_TURRETYELLOW[curTurretYellow];
		}
		
		return 1;
	}
	public static float getBulletExplodeRadius(int TURRET) {
		switch(TURRET) {
		case Bullet.BLUE_POWER: return Consts.uLevel_POWERBLUE_RADIUS[curPowerBlueRadius];
		case Bullet.GREEN_POWER: return Consts.uLevel_POWERGREEN_RADIUS[curPowerGreenRadius];
		case Bullet.YELLOW_POWER: return Consts.uLevel_POWERYELLOW_RADIUS[curPowerYellowRadius];
		}
		return 1;
	}
	
	
	public static void addToCoins(int amountToAdd) {
		storeNewLevels(Player.KEY_COINS, curLevelCoins + amountToAdd);
	}
	public static void addToTokens(int amountToAdd) {
		storeNewLevels(Player.KEY_TOKENS, curLevelTokens + amountToAdd);
	}
}
