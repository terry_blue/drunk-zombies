package com.twentyfourktapps.drunkzombies.obj;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.R;
import com.twentyfourktapps.drunkzombies.units.Bullet;
import com.twentyfourktapps.libs.obj.BidirectionalMap;

public class Player extends View {

	final String TAG = "Player";
	
	public double rotationDeg;
	
	public int curTurretColor;
	float scale = 1;
	Bitmap[] bmpLayers;
	Matrix matrix, normalMatrix;
	boolean isScaled = false;
	public PointF centrePoint, bmpRotatePoint, rotatePointRatio, firePointRatio;
	PointF actualRotatePoint, actualFireStartPoint;
	float rotateOffsetDeg, firingRadius, rotateHyp;
	
	long nextFireTime, gapBetweenFire;
	public boolean isLive;
	
	public final static String KEY_COINS = "player_coins";
	public final static String KEY_TOKENS = "payer_tokens";
	public final static String KEY_WAVE = "player_wave";
	
	public final static String KEY_LEVEL_STRENGTH = "player_level_strength";
	public final static String KEY_LEVEL_AGILITY = "player_level_agility";
	public final static String KEY_LEVEL_CRITICAL = "player_level_critical";
	public final static String KEY_LEVEL_POISON = "player_level_poison";
	public final static String KEY_LEVEL_KICK = "player_level_kick";
	public final static String KEY_LEVEL_MULTI = "player_level_multishot";
	
	public final static String KEY_LEVEL_TURRET_BLUE = "player_level_turret_blue";
	public final static String KEY_LEVEL_TURRET_GREEN = "player_level_turret_green";
	public final static String KEY_LEVEL_TURRET_YELLOW = "player_level_turret_yellow";
	public final static String KEY_LEVEL_TURRET_BLUE_ROF = "player_level_turret_blue_rof";
	public final static String KEY_LEVEL_TURRET_GREEN_ROF = "player_level_turret_green_rof";
	public final static String KEY_LEVEL_TURRET_YELLOW_ROF = "player_level_turret_yellow_rof";
	public final static String KEY_LEVEL_TURRET_BLUE_DAMAGE = "player_level_turret_blue_damage";
	public final static String KEY_LEVEL_TURRET_GREEN_DAMAGE = "player_level_turret_green_damage";
	public final static String KEY_LEVEL_TURRET_YELLOW_DAMAGE = "player_level_turret_yellow_damage"; 
	public final static String KEY_LEVEL_TURRET_BLUE_TURNSPEED = "player_level_turret_blue_turnspeed";
	public final static String KEY_LEVEL_TURRET_GREEN_TURNSPEED = "player_level_turret_green_turnspeed";
	public final static String KEY_LEVEL_TURRET_YELLOW_TURNSPEED = "player_level_turret_yellow_turnspeed";
	
	public final static String KEY_LEVEL_POWER_BLUE_DAMAGE = "player_level_power_blue_damage";
	public final static String KEY_LEVEL_POWER_BLUE = "player_level_power_blue_length";
	public final static String KEY_LEVEL_POWER_BLUE_RADIUS = "player_level_power_blue_radius";
	public final static String KEY_LEVEL_POWER_BLUE_COUNT = "player_level_power_blue_count";
	public final static String KEY_LEVEL_POWER_GREEN_DAMAGE = "player_level_power_green_damage";
	public final static String KEY_LEVEL_POWER_GREEN = "player_level_power_green_length";
	public final static String KEY_LEVEL_POWER_GREEN_RADIUS = "player_level_power_green_radius";
	public final static String KEY_LEVEL_POWER_GREEN_COUNT = "player_level_power_green_count";
	public final static String KEY_LEVEL_POWER_YELLOW_DAMAGE = "player_level_power_yellow_damage";
	public final static String KEY_LEVEL_POWER_YELLOW = "player_level_power_yellow_length";
	public final static String KEY_LEVEL_POWER_YELLOW_RADIUS = "player_level_power_yellow_radius";
	public final static String KEY_LEVEL_POWER_YELLOW_COUNT = "player_level_power_yellow_count";
	
	public final static String KEY_LEVEL_BOTTLE_COUNT_RED = "player_level_bottle_count_red";
	public final static String KEY_LEVEL_BOTTLE_COUNT_ORANGE = "player_level_bottle_count_orange";
	public final static String KEY_LEVEL_BOTTLE_COUNT_GREEN = "player_level_bottle_count_green";
	public final static String KEY_LEVEL_BOTTLE_THICKNESS = "player_level_bottle_thickness";
	public final static String KEY_LEVEL_BOTTLE_QUALITY = "player_level_bottle_quality";
	
	public Player(Context context, AttributeSet attrs) {
		super(context, attrs);
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        bmpLayers = new Bitmap[3];     
        
		matrix = new Matrix();
		normalMatrix = new Matrix();
		centrePoint = new PointF();
		actualRotatePoint = new PointF();
		actualFireStartPoint = new PointF();
	}

	
	
	public void initialize(int TURRETtype) {
        Log.e(TAG, "turretType = "+TURRETtype);
		curTurretColor = TURRETtype;
		rotateHyp = 1;
		PointF[] rotateRatios = new PointF[] { new PointF(56f/250f, 218f/421f), new PointF(0.277777778f, 0.474074074074f), new PointF(0.3285714286f, 0.44285701052f), new PointF(0.325f, 0.437037037f) };
		PointF[] fireStartPoints = new PointF[] { new PointF(225f/250f, 266f/421f), new PointF(0.8f, 0.5703703704f), new PointF(0.5333333333f, 0.5809523809f), new PointF(0.88f, 0.55555f) };
		float[] fireRates = new float[] { Consts.uLevel_AGILITY[PlayerLevels.curAgility], Consts.uLevel_TURRETGREEN_ROF[PlayerLevels.curTurretGreenROF], Consts.uLevel_TURRETBLUE_ROF[PlayerLevels.curTurretBlueROF], /*Consts.uLevel_TURRETYELLOW_ROF[PlayerLevels.curTurretYellowROF]*/ 10 };
		int[] bmp0ids = new int[] { R.drawable.char_jonhnny_arma, R.drawable.char_green_arma, R.drawable.char_blue_arma, R.drawable.char_yellow_arma };
		int[] bmp1ids = new int[] { R.drawable.char_jonhnny_body, R.drawable.char_green_body, R.drawable.char_blue_body, R.drawable.char_yellow_body };
		int[] bmp2ids = new int[] { R.drawable.char_jonhnny_armb, R.drawable.char_green_armb, R.drawable.char_blue_armb, R.drawable.char_yellow_armb };
		int pos = 0;
		
		switch(TURRETtype) {
		case Bullet.YELLOW: pos++;
		case Bullet.BLUE: pos++;
		case Bullet.GREEN: pos++;
		case Bullet.PLAYER: 
			bmpLayers[0] = BitmapFactory.decodeResource(getResources(), bmp0ids[pos]);
			bmpLayers[1] = BitmapFactory.decodeResource(getResources(), bmp1ids[pos]);
			bmpLayers[2] = BitmapFactory.decodeResource(getResources(), bmp2ids[pos]);
			rotatePointRatio = rotateRatios[pos];
			firePointRatio = fireStartPoints[pos];
			bmpRotatePoint = new PointF((float)bmpLayers[0].getWidth() * rotateRatios[pos].x, (float)bmpLayers[0].getHeight() * rotateRatios[pos].y);
			gapBetweenFire = (long)(1000f / fireRates[pos]);
			nextFireTime = System.currentTimeMillis() + gapBetweenFire;				
			
			actualRotatePoint.set((float)getWidth() * rotatePointRatio.x, (float)getHeight() * rotatePointRatio.y);
			actualFireStartPoint.set((float)getWidth() * firePointRatio.x, (float)getHeight() * firePointRatio.y);			
			rotateHyp = (float)Math.sqrt(Math.pow(actualFireStartPoint.x - actualRotatePoint.x, 2) + Math.pow(actualFireStartPoint.y - actualRotatePoint.y, 2));			
			rotateOffsetDeg = (float)Math.toDegrees(Math.atan2(actualFireStartPoint.y - actualRotatePoint.y, actualFireStartPoint.x - actualRotatePoint.x));
			
			break;
		default:
			bmpLayers[0] = null;
			bmpLayers[1] = null;
			bmpLayers[2] = null;
		}		
	}

	
	public static BidirectionalMap<String, Integer> getKeysMap() {
		BidirectionalMap<String, Integer> map = new BidirectionalMap<String, Integer>();
		map.put(KEY_LEVEL_STRENGTH, 1);
		map.put(KEY_LEVEL_AGILITY, 2);
		map.put(KEY_LEVEL_CRITICAL, 3);
		map.put(KEY_LEVEL_POISON, 4);
		map.put(KEY_LEVEL_KICK, 5);
		map.put(KEY_LEVEL_MULTI, 6);
		map.put(KEY_LEVEL_TURRET_YELLOW, 7);
		map.put(KEY_LEVEL_TURRET_GREEN, 8);
		map.put(KEY_LEVEL_TURRET_BLUE, 9);
		map.put(KEY_LEVEL_TURRET_YELLOW_DAMAGE, 10);
		map.put(KEY_LEVEL_TURRET_GREEN_DAMAGE, 11);
		map.put(KEY_LEVEL_TURRET_BLUE_DAMAGE, 12);
		map.put(KEY_LEVEL_TURRET_YELLOW_ROF, 13);
		map.put(KEY_LEVEL_TURRET_GREEN_ROF, 14);
		map.put(KEY_LEVEL_TURRET_BLUE_ROF, 15);
		map.put(KEY_LEVEL_BOTTLE_QUALITY, 16);
		map.put(KEY_LEVEL_BOTTLE_THICKNESS, 17);
		map.put(KEY_LEVEL_BOTTLE_COUNT_RED, 18);
		map.put(KEY_LEVEL_BOTTLE_COUNT_ORANGE, 19);
		map.put(KEY_LEVEL_BOTTLE_COUNT_GREEN, 20);
		map.put(KEY_LEVEL_POWER_YELLOW_DAMAGE, 21);
		map.put(KEY_LEVEL_POWER_GREEN_DAMAGE, 22);
		map.put(KEY_LEVEL_POWER_BLUE_DAMAGE, 23);
		map.put(KEY_LEVEL_POWER_YELLOW, 24);
		map.put(KEY_LEVEL_POWER_GREEN, 25);
		map.put(KEY_LEVEL_POWER_BLUE, 26);
		map.put(KEY_LEVEL_POWER_YELLOW_RADIUS, 27);
		map.put(KEY_LEVEL_POWER_GREEN_RADIUS, 28);
		map.put(KEY_LEVEL_POWER_BLUE_RADIUS, 29);		
		return map;
	}
	

	public void draw(@NonNull Canvas canvas) {
		if(isScaled) {

            /*
			Paint p = new Paint();
			p.setColor(Color.GREEN);
			p.setAlpha(80);
			canvas.drawCircle(actualRotatePoint.x + getLeft(), actualRotatePoint.y + getTop(), rotateHyp, p);
            */
			if(bmpLayers[0] != null) canvas.drawBitmap(bmpLayers[0], matrix, null);
			if(bmpLayers[1] != null) canvas.drawBitmap(bmpLayers[1], normalMatrix, null);
			if(bmpLayers[2] != null) canvas.drawBitmap(bmpLayers[2], matrix, null);
			
		}
	}
	
	public void rotate(double inDegs) {
		rotationDeg = inDegs + rotateOffsetDeg;	
		matrix.setScale(scale, scale);
		matrix.preRotate((float)rotationDeg, bmpRotatePoint.x, bmpRotatePoint.y);	
						
		invalidate();
	}
	
	public void rotateTo(PointF point) {
		if(point != null) {
			View parent = (View)getParent();
			float diffX = point.x - ((float)parent.getLeft() + actualRotatePoint.x);
			float diffY = point.y - ((float)parent.getTop() + actualRotatePoint.y);
			if(curTurretColor != -1)
				rotate(Math.toDegrees(Math.atan2(diffY, diffX)));
		}
	}
	
	public void setScale(float factor) {
        Log.e(TAG, "setScale(factor = "+factor+")");
		scale = factor;
		matrix.setScale(factor, factor);
		normalMatrix.setScale(factor, factor);
		isScaled = true;
		postInvalidate();
	}		

	public int getBmpHeight() {
		if(bmpLayers[0] != null) return bmpLayers[0].getHeight();
		else return 0;
	}
	public int getBmpWidth() {
		if(bmpLayers[0] != null) return bmpLayers[0].getWidth();
		else return 0;
	}
	public float getRatio(float newHeight) {
		return newHeight / (float)bmpLayers[0].getHeight();
	}
	
	public PointF getFiringPoint() {		
		return new PointF(((float)(Math.cos(Math.toRadians(rotationDeg)) * (double)rotateHyp) + actualRotatePoint.x + (float)((View)getParent()).getLeft())
						, ((float)(Math.sin(Math.toRadians(rotationDeg)) * (double)rotateHyp) + actualRotatePoint.y + (float)((View)getParent()).getTop()));
		
	}	
	
	public boolean toFire() {
		long curTime = System.currentTimeMillis();
		if(isLive && nextFireTime < curTime) {
			nextFireTime = curTime + gapBetweenFire;
			return true;
		}
		return false;
	}
	
	@Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int w = (bmpLayers[0] != null) ?  (int)((float)bmpLayers[0].getWidth() * scale): 0;
		int h = (bmpLayers[0] != null) ?  (int)((float)bmpLayers[0].getHeight() * scale) : 0;
		
		final int desiredHSpec = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY);
		final int desiredWSpec = MeasureSpec.makeMeasureSpec(w, MeasureSpec.EXACTLY);
		
		setMeasuredDimension(desiredWSpec, desiredHSpec);
    }
	

	@Override
	protected void onSizeChanged(int w, int h, int oldW, int oldH) {
		
		if(rotatePointRatio != null && firePointRatio != null) {
            Log.e(TAG, "w="+w+", h="+h);
			actualRotatePoint.set((float)w * rotatePointRatio.x, (float)h * rotatePointRatio.y);
			actualFireStartPoint.set((float)w * firePointRatio.x, (float)h * firePointRatio.y);			
			rotateHyp = (float)Math.sqrt(Math.pow(actualFireStartPoint.x - actualRotatePoint.x, 2) + Math.pow(actualFireStartPoint.y - actualRotatePoint.y, 2));			
			//                                                                                                                                                                                                                                                                                                                                                                                                               rotateOffsetDeg = (float)Math.toDegrees(Math.atan2(actualFireStartPoint.y - actualRotatePoint.y, actualFireStartPoint.x - actualRotatePoint.x));
		}
	}
}
