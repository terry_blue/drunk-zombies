package com.twentyfourktapps.drunkzombies.obj;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.R;
import com.twentyfourktapps.drunkzombies.data.DataBaseHelper;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;

public class UpgradeButton extends RelativeLayout {

	
	public enum UpgradeType { BULLETS, POWERS, BOTTLES, TURRETS };
	
	public UpgradeButton(Context context, AttributeSet attrs) {
		super(context, attrs);		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.linearlayout_upgradesbutton , this);
		
		if(!isInEditMode()) {				
			((TextView)findViewById(R.id.upgradebutton_textview_title)).setTypeface(StaticResources.getFont(context, Font.FEAST));
		}

	}
	
	
	public void refreshData(SharedPreferences sp) {
		DataBaseHelper dbH = new DataBaseHelper(getContext());
		Cursor dataCursor = dbH.getUpgradeButtonDataForId((String)getTag());
		
		if(dataCursor.moveToFirst()) {			
			TextView titleTV = (TextView)findViewById(R.id.upgradebutton_textview_title);
			titleTV.setText(dataCursor.getString(dataCursor.getColumnIndex(DataBaseHelper.COL_TITLE)));
			
			ImageView iconIV = (ImageView)findViewById(R.id.upgradebutton_imageview_icon);
			LinearLayout backgroundLL = (LinearLayout)findViewById(R.id.upgradebutton_linearlayout_background_selector);
			if(checkAvailability(sp)) {
				iconIV.setVisibility(View.VISIBLE);
				if(dataCursor.getString(dataCursor.getColumnIndex(DataBaseHelper.COL_COST_TYPE)).equals("CAP")) {
					iconIV.setImageDrawable(StaticResources.getDZCoin(getContext()));
					backgroundLL.setBackgroundResource(R.drawable.selector_upgradebutton_green);
				} else {
					iconIV.setImageDrawable(StaticResources.getCoin(getContext()));
					backgroundLL.setBackgroundResource(R.drawable.selector_upgradebutton_yellow);
				}
			} else { //Upgrade not available
				iconIV.setVisibility(View.INVISIBLE);
				backgroundLL.setBackgroundResource(R.drawable.selector_upgradebutton_unavailable);
				
			}
		}

        dataCursor.close();
		dbH.close();
	}



	
	public boolean checkAvailability(SharedPreferences sp) {
		int curLevel = PlayerLevels.getLevel(PlayerLevels.getKey(getId()));
		
		switch(getId()) {
		
		//Bullets Column A
		case R.id.upgrades_upgradebutton_strength:
		case R.id.upgrades_upgradebutton_agility:
			return true;	
			
			
		//Bullets Column B
		case R.id.upgrades_upgradebutton_critical:
		case R.id.upgrades_upgradebutton_poison:
		case R.id.upgrades_upgradebutton_kick: 		
			return (curLevel < PlayerLevels.curAgility && curLevel < PlayerLevels.curStrength);
			
		
		//Bullets Column C
		case R.id.upgrades_upgradebutton_multishot:
			return (curLevel < PlayerLevels.curKick
					&& curLevel < PlayerLevels.curPoison
					&& curLevel < PlayerLevels.curCritical);
		
		
		
		
		//Turrets Column A
		case R.id.upgrades_upgradebutton_turretyellow:
			return sp.getBoolean(Consts.SP_KEY_TURRET_YELLOW_BOUGHT, false);
		case R.id.upgrades_upgradebutton_turretgreen:
			return sp.getBoolean(Consts.SP_KEY_TURRET_GREEN_BOUGHT, false);
		case R.id.upgrades_upgradebutton_turretblue:
			return sp.getBoolean(Consts.SP_KEY_TURRET_BLUE_BOUGHT, false);
		
			
		//Turrets Column B
		case R.id.upgrades_upgradebutton_turretyellow_damage:
			return curLevel < PlayerLevels.curTurretYellow;
		case R.id.upgrades_upgradebutton_turretgreen_damage:
			return curLevel < PlayerLevels.curTurretGreen;
		case R.id.upgrades_upgradebutton_turretblue_damage:
			return curLevel < PlayerLevels.curTurretBlue;
			
		//Turrets Column C
		case R.id.upgrades_upgradebutton_turretyellow_agility:
			return curLevel < PlayerLevels.curTurretYellowDamage;
		case R.id.upgrades_upgradebutton_turretgreen_agility:
			return curLevel < PlayerLevels.curTurretGreenDamage;
		case R.id.upgrades_upgradebutton_turretblue_agility:
			return curLevel < PlayerLevels.curTurretBlueDamage;	
			
				
		//Bottles Column A
		case R.id.upgrades_upgradebutton_bottlecount_red:
			return true;
		case R.id.upgrades_upgradebutton_bottlecount_orange:
			return curLevel < PlayerLevels.curBottlesRed;
		case R.id.upgrades_upgradebutton_bottlecount_green:
			return curLevel < PlayerLevels.curBottlesOrange;
		
		//Bottles Column B
		case R.id.upgrades_upgradebutton_bottlequality:
		case R.id.upgrades_upgradebutton_bottlethickness:
			return curLevel < PlayerLevels.curBottlesGreen;
			
			
			
		//Powers Column A
		case R.id.upgrades_upgradebutton_powers_green:
			return true;
		case R.id.upgrades_upgradebutton_powers_yellow:
			return curLevel < PlayerLevels.curPowerGreen;
		case R.id.upgrades_upgradebutton_powers_blue:
			return curLevel < PlayerLevels.curPowerYellow;
			
		//Powers Column B
		case R.id.upgrades_upgradebutton_powers_green_powerlevel:
			return curLevel < PlayerLevels.curPowerGreen;
		case R.id.upgrades_upgradebutton_powers_yellow_powerlevel:
			return curLevel < PlayerLevels.curPowerYellow;
		case R.id.upgrades_upgradebutton_powers_blue_powerlevel:
			return curLevel < PlayerLevels.curPowerBlue;
			
		//Powers Column C
		case R.id.upgrades_upgradebutton_powers_green_radius:
			return curLevel < PlayerLevels.curPowerGreenDamage;
		case R.id.upgrades_upgradebutton_powers_yellow_radius:
			return curLevel < PlayerLevels.curPowerYellowDamage;
		case R.id.upgrades_upgradebutton_powers_blue_radius:
			return curLevel < PlayerLevels.curPowerBlueDamage;
	}
		return false;
	
}
}
