package com.twentyfourktapps.drunkzombies.obj;

import java.util.Random;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import android.util.SparseIntArray;

import com.twentyfourktapps.drunkzombies.R;

public class SoundManager {

	final static  String TAG = "SoundManager";
	
	//Sound IDs
	public final static int KACHING = 1;
	public final static int LOSE_SOUND = 2;
	public final static int WIN_SOUND = 3;
	public final static int POP = 4;
	public final static int POP2 = 5;
	public final static int SHOTGUN_COCK = 6;
	public final static int GLASS = 7;
	public final static int ICE = 8;
	public final static int SUPERSOUND = 9;
	public final static int BOSS_ALERT = 10;
	
	public final static int BRING_IT = 11;
	
	public final static int DRUNK_ZOMBIES = 12;
	public final static int TAKE_THAT = 13;
	public final static int TAKE_THAT2 = 14;
	public final static int[] TAKE_THAT_ARRAY = new int[] { TAKE_THAT, TAKE_THAT2 }; 
	public final static int IS_THAT_ALL = 15;
	public final static int WOOHOO1 = 16;
	public final static int WOOHOO2 = 17;
	public final static int WOOHOO3 = 18;
	public final static int WOOHOO4 = 19;
	public final static int WOOHOO5 = 20;
	public final static int WOOHOO6 = 21;
	public final static int WOOHOO7 = 22;
	public final static int WOOHOO8 = 23;
	public final static int WOOHOO9 = 24;
	public final static int WOOHOO10 = 25;
	public final static int[] WOOHOO_ARRAY = new int[] { WOOHOO1, WOOHOO2, WOOHOO3, WOOHOO4, WOOHOO5, WOOHOO6, WOOHOO7, WOOHOO8, WOOHOO9, WOOHOO10};
	public final static int YEA1 = 26;
	public final static int YEA2 = 27;
	public final static int YEA3 = 28;
	public final static int YEA4 = 29;
	public final static int YEA5 = 30;
	public final static int[] YEA_ARRAY = new int[] { YEA1, YEA2, YEA3, YEA4, YEA5 };
	
	public static boolean isInitialized = false;
	public static Context context = null;
	
	
	public static SoundPool soundPool;
	public static SparseIntArray soundPoolMap;
	public static boolean isSoundEnabled = true;
	
	public static void initialize(Context cxt) {
		context = cxt;
		soundPool = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);
		soundPoolMap = new SparseIntArray();
		
		//SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		
		initializeSound(cxt, KACHING, R.raw.kaching, 1);
		initializeSound(cxt, LOSE_SOUND, R.raw.lose, 1);
		initializeSound(cxt, WIN_SOUND, R.raw.win, 1);
		initializeSound(cxt, POP, R.raw.pop, 1);
		initializeSound(cxt, POP2, R.raw.cork, 1);
		initializeSound(cxt, SHOTGUN_COCK, R.raw.shotgun, 1);
		initializeSound(cxt, GLASS, R.raw.glass, 1);
		initializeSound(cxt, ICE, R.raw.ice3, 1);
		initializeSound(cxt, SUPERSOUND, R.raw.supersound, 1);
		initializeSound(cxt, BRING_IT, R.raw.bring_it, 1);
		initializeSound(cxt, BOSS_ALERT, R.raw.alert, 1);
		
		initializeSound(cxt, DRUNK_ZOMBIES, R.raw.drunk_zombies, 1);
		initializeSound(cxt, TAKE_THAT, R.raw.takethat1, 1);
		initializeSound(cxt, TAKE_THAT2, R.raw.takethat2, 1);
		initializeSound(cxt, IS_THAT_ALL, R.raw.is_that_all, 1);
		initializeSound(cxt, WOOHOO1, R.raw.woohoo1, 1);
		initializeSound(cxt, WOOHOO2, R.raw.woohoo2, 1);
		initializeSound(cxt, WOOHOO3, R.raw.woohoo3, 1);
		initializeSound(cxt, WOOHOO4, R.raw.woohoo4, 1);
		initializeSound(cxt, WOOHOO5, R.raw.woohoo5, 1);
		initializeSound(cxt, WOOHOO6, R.raw.woohoo6, 1);
		initializeSound(cxt, WOOHOO7, R.raw.woohoo7, 1);
		initializeSound(cxt, WOOHOO8, R.raw.woohoo8, 1);
		initializeSound(cxt, WOOHOO9, R.raw.woohoo9, 1);
		initializeSound(cxt, WOOHOO10, R.raw.woohoo10, 1);
		initializeSound(cxt, YEA1, R.raw.yea1, 1);
		initializeSound(cxt, YEA2, R.raw.yea2, 1);
		initializeSound(cxt, YEA3, R.raw.yea3, 1);
		initializeSound(cxt, YEA4, R.raw.yea4, 1);
		initializeSound(cxt, YEA5, R.raw.yea5, 1);
		
		isInitialized = true;
	}
	
	
	private static void initializeSound(Context context, int soundID,  int rawId, int priority) {
		soundPoolMap.put(soundID, soundPool.load(context, rawId, priority));
	}
	
	public static int playSound(Context context, int sound) {
		int playStreamId = -2;
		if (isInitialized) {
			/* Updated: The next 4 lines calculate the current volume in a scale of 0.0 to 1.0 */
		    AudioManager mgr = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		    float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_SYSTEM);
		    float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);    
		    float volume = streamVolumeCurrent / streamVolumeMax;
		    
		    /* Play the sound with the correct volume */	
		   do {
		   playStreamId = soundPool.play(soundPoolMap.get(sound), volume, volume, 1, 0, 1f);
		   } while(playStreamId == -2);
		    
		}
		
		else { 
			Log.e(TAG, "playSound() not initialized");
		}
		
		return playStreamId;
	}
	
	public static int playSound(int sound) {
		int playStreamId = -2;
		if(isInitialized) {
			/* Updated: The next 4 lines calculate the current volume in a scale of 0.0 to 1.0 */
		    AudioManager mgr = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		    float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_SYSTEM);
		    float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);    
		    float volume = streamVolumeCurrent / streamVolumeMax;
		    
		    /* Play the sound with the correct volume */	
		   do {
		   playStreamId = soundPool.play(soundPoolMap.get(sound), volume, volume, 1, 0, 1f);
		   } while(playStreamId == -2);
		   
		} else { 
			Log.e(TAG, "playSound() not initialized");
		}
		
		return playStreamId;
		
	}	
	
	
	public static int playTakeThat() {
		Random rand = new Random();
		return playSound(TAKE_THAT_ARRAY[rand.nextInt(TAKE_THAT_ARRAY.length)]);
	}
	
	public static int playWooHoo() {
		Random rand = new Random();
		return playSound(WOOHOO_ARRAY[rand.nextInt(WOOHOO_ARRAY.length)]);
	}
	
	public static int playYea() {
		Random rand = new Random();
		return playSound(YEA_ARRAY[rand.nextInt(YEA_ARRAY.length)]);
	}
	
}
