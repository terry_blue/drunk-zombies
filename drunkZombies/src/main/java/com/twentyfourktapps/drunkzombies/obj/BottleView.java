package com.twentyfourktapps.drunkzombies.obj;

import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.R;

public class BottleView extends View {

	final String TAG = "BottleView";
	final int MAX_BOTTLE_DESIGNS = 11;
	final public int startBottleCount;
	Bitmap tableTop, bottleBmp;
	Rect[] srcRects;
	Rect bottleSrcRect;
	//Bottle[] bottles;
	public ArrayList<Bottle> bottlesRed, bottlesOrange, bottlesGreen, allBottles;
	//int numOfBottles;
	//boolean isBottleDestRectsSet = false;
	
	public double totalBottleHp, redBottleHp, orangeBottleHp, greenBottleHp;
	
	Rect tableRect;
	Paint tablePaint;
	
	public BottleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		tableTop = BitmapFactory.decodeResource(getResources(), R.drawable.tabletop);
		bottleBmp = BitmapFactory.decodeResource(getResources(), R.drawable.bottle);
		tablePaint = new Paint();
		tablePaint.setColor(getResources().getColor(R.color.table_top));
		tableRect = new Rect();
		bottleSrcRect = new Rect(0,0, bottleBmp.getWidth(), bottleBmp.getHeight());
		if(!isInEditMode()) {
			bottlesRed = new ArrayList<>(); bottlesOrange = new ArrayList<>(); bottlesGreen = new ArrayList<>(); allBottles = new ArrayList<>();
			
			//add red bottles to the table
			for(int i = 0; i < PlayerLevels.curBottlesRed; i++) {
				Bottle bottle = new Bottle(Bottle.BOTTLE_RED);
				bottlesRed.add(bottle);
				allBottles.add(bottle);
			}
			for(int i = 0; i < PlayerLevels.curBottlesOrange; i++) {
				Bottle bottle = new Bottle(Bottle.BOTTLE_ORANGE);
				bottlesOrange.add(bottle);
				allBottles.add(bottle);
			}
			for(int i = 0; i < PlayerLevels.curBottlesGreen; i++) {
				Bottle bottle = new Bottle(Bottle.BOTTLE_GREEN);
				bottlesGreen.add(bottle);
				allBottles.add(bottle);
			}
			startBottleCount = allBottles.size();

			redBottleHp = (Consts.uLevel_BOTTLE_COUNT_RED[PlayerLevels.curBottlesRed] * Consts.BOTTLE_HP_RED);
			orangeBottleHp = (Consts.uLevel_BOTTLE_COUNT_ORANGE[PlayerLevels.curBottlesOrange] * Consts.BOTTLE_HP_ORANGE);
			greenBottleHp = (Consts.uLevel_BOTTLE_COUNT_GREEN[PlayerLevels.curBottlesGreen] * Consts.BOTTLE_HP_GREEN);
			totalBottleHp = redBottleHp + orangeBottleHp + greenBottleHp;
			
		} else startBottleCount = 0;
		
	}
	


	public void update() {
		int bCount = 1;
		if(totalBottleHp < redBottleHp) {
			for(Bottle bottle : bottlesRed) {
				if(totalBottleHp > (Consts.BOTTLE_HP_RED * bCount)) bottle.isLive = true;
				else {
					if(bottle.isLive = true) invalidate();
					bottle.isLive = false;
		
				}
				bCount++;
			}
		}
		if(totalBottleHp < (redBottleHp + orangeBottleHp)) {
			bCount = 1;
			for(Bottle bottle : bottlesOrange) {
				if(totalBottleHp > redBottleHp + (Consts.BOTTLE_HP_ORANGE * bCount)) bottle.isLive = true;
				else {
					if(bottle.isLive = true) invalidate();
					bottle.isLive = false;
				}
				bCount++;
			}
		}
		if(totalBottleHp < (redBottleHp + orangeBottleHp + greenBottleHp)) {
			bCount = 1;
			for(Bottle bottle : bottlesGreen) {
				if(totalBottleHp >= (redBottleHp + orangeBottleHp + (Consts.BOTTLE_HP_GREEN * bCount)))
                    bottle.isLive = true;
				else {
					if(bottle.isLive = true)
                        invalidate();
					bottle.isLive = false;
				}
				bCount++;
			}
		}
		
	}
	@Override
	public void onDraw(Canvas canvas) {
		tableRect.bottom = canvas.getHeight();
		tableRect.right = canvas.getWidth() * 2;
		//canvas.drawRect(tableRect, tablePaint);
		
		if(!isInEditMode()) {
            setDestRects(canvas);
			for(Bottle bottle : bottlesRed) {
				if(bottle.isLive && bottle.destRect != null) canvas.drawBitmap(bottleBmp, bottleSrcRect, bottle.destRect, bottle.paint);
			}
			for(Bottle bottle : bottlesOrange) {
				if(bottle.isLive && bottle.destRect != null) canvas.drawBitmap(bottleBmp, bottleSrcRect, bottle.destRect, bottle.paint);
			}
			for(Bottle bottle : bottlesGreen) {
				if(bottle.isLive && bottle.destRect != null) canvas.drawBitmap(bottleBmp, bottleSrcRect, bottle.destRect, bottle.paint);
			}
		}
	}
	
	@Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		//final int desiredHSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), MeasureSpec.EXACTLY);
		final int desiredWSpec = MeasureSpec.makeMeasureSpec(tableTop.getWidth(), MeasureSpec.EXACTLY);
		setMeasuredDimension(desiredWSpec, getMeasuredHeight());
    }
    
	public int getBottleLostCount() {
		int curDead = 0;
		for(Bottle bottle : allBottles) {
			if(!bottle.isLive)
                curDead++;
		}
		
		return startBottleCount - curDead;
	}
	
	
	public void setDestRects(Canvas dest) {
		int totalBottleCount = allBottles.size();
		float drawHeight = (float)dest.getHeight();// - bottleBmp.getHeight();
		float drawGap = drawHeight / (float)totalBottleCount;
		
		for(int i = 0; i < allBottles.size(); i++)
			allBottles.get(i).setDestRect(0, drawGap * (float)i);
	}
	public void deductBottle() {
		try {
			//bottles[--numOfBottles].remove();
		} catch(Exception e) {
			Log.e(TAG, "deductBottle() error: " + e.toString());
		}
	}
	
	
	public class Bottle {
		
		public static final int BOTTLE_RED = 1;
		public static final int BOTTLE_ORANGE = 2;
		public static final int BOTTLE_GREEN = 3;
		
		public boolean isLive = false;
		int bottleTypeID = 0;
		RectF destRect;
		Paint paint;
		Paint fullnessPaint;
		
		public Bottle(int bottleType) {
			isLive = true;
			Random rand = new Random();
			bottleTypeID = rand.nextInt(MAX_BOTTLE_DESIGNS);
			fullnessPaint = new Paint();
			paint = new Paint();
			
			switch(bottleType) {
			case BOTTLE_RED: {
				paint.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.bottle_low), PorterDuff.Mode.SRC_ATOP));
				break;
			}
			case BOTTLE_ORANGE: {
				paint.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.bottle_med), PorterDuff.Mode.SRC_ATOP));
				break;
			}
			case BOTTLE_GREEN: {
				paint.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.bottle_high), PorterDuff.Mode.SRC_ATOP));
				break;
			}
			}
		}
		
		
		
		public void setDestRect(float left, float top) {
			destRect = new RectF(left, top, left+bottleBmp.getWidth(), top+bottleBmp.getHeight());
		}
		
		public void remove() {
			isLive = false;
		}
	}
}
