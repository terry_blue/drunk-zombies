package com.twentyfourktapps.drunkzombies.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.twentyfourktapps.libs.databases.DataBaseImporter;

public class DataBaseHelper extends DataBaseImporter {

	final String TAG = "DataBaseHelper";
	
	
	final public static String TABLE_UPGRADES_DATA_TABLE = "UpgradesDataTable";
	final public static String COL_ID = "_id";
	final public static String COL_TITLE = "Title";
	final public static String COL_DESC = "Desc";
	final public static String COL_COST = "Cost";
	final public static String COL_COST_TYPE = "CostType";
	final public static String COL_UPGRADE_TYPE = "UpgradeType";
	final public static String COL_LEVELS = "Levels";
	final public static String COL_AVAILABLE = "Available";
	final public static String[] COLS_UPGRADES_DATA = new String[] { COL_ID, COL_TITLE, COL_DESC, COL_COST, COL_COST_TYPE, COL_UPGRADE_TYPE, COL_LEVELS, COL_AVAILABLE };
	
	
	
	
	private static final String DB_NAME = "web157-dz_droid";
	private static final int DB_VERSION = 1;
	public DataBaseHelper(Context context) {
		super(context, DB_NAME, DB_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
	}



	public Cursor getUpgradeButtonDataForId(String id) {
		return sqLiteDb.query(TABLE_UPGRADES_DATA_TABLE, new String[] { COL_TITLE, COL_COST_TYPE, COL_AVAILABLE }, COL_ID+"="+id, null, null, null, null);
	}
	public Cursor getUpgradeDataForId(String id) {		
		return sqLiteDb.query(TABLE_UPGRADES_DATA_TABLE, COLS_UPGRADES_DATA, COL_ID+"="+id, null, null, null, null);
	}
	
	
	
	
}
