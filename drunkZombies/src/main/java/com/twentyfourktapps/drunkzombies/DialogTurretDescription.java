package com.twentyfourktapps.drunkzombies;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;
import com.twentyfourktapps.drunkzombies.obj.Turret.TurretType;

public class DialogTurretDescription extends DialogFragment implements OnClickListener{
	
	public static String TAG = "DialogTurretDescription";
	public static DialogTurretDescription instance = null;
	
	TurretType TURRET_CHOICE;
	private int cost;
	private String boughtKey;

	public static DialogTurretDescription getInstance() {
		if(instance == null) 
			instance = new DialogTurretDescription();
		
		return instance;
	}
	
	public void initialize(TurretType turretType) {
		TURRET_CHOICE = turretType;		
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		View view = inflater.inflate(R.layout.dialog_turretdescription, container);
		
		view.findViewById(R.id.turretdescription_button_back).setOnClickListener(this);
		view.findViewById(R.id.turretdescription_button_buy).setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		int[] imageIds = new int[] { R.drawable.turret_blue, R.drawable.turret_green, R.drawable.turret_yellow };
		int[] titleIds = new int[] { R.string.char_name_blue, R.string.char_name_green, R.string.char_name_yellow };
		int[] descIds = new int[] { R.string.char_desc_blue, R.string.char_desc_green, R.string.char_desc_yellow };
		int[] costs = new int[] { Consts.price_TURRET_BLUE, Consts.price_TURRET_GREEN, Consts.price_TURRET_YELLOW };
		String[] boughtKeys = new String[] { Consts.SP_KEY_TURRET_BLUE_BOUGHT, Consts.SP_KEY_TURRET_GREEN_BOUGHT, Consts.SP_KEY_TURRET_YELLOW_BOUGHT };
		int pos = 0;
		
		switch(TURRET_CHOICE) {
		case YELLOW: pos++;
		case GREEN: pos++;
		case BLUE: {
			cost = costs[pos];
			boughtKey = boughtKeys[pos];
			getDialog().setTitle(titleIds[pos]);
			
			TextView descTV = (TextView)getView().findViewById(R.id.turretdescription_textview_description); 
			descTV.setText(descIds[pos]);
			Drawable drawable = getResources().getDrawable(imageIds[pos]);
			descTV.setCompoundDrawables(drawable, null, null, null);		
		}
		}
		
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		if(sp.getBoolean(boughtKey, false)) 
			getView().findViewById(R.id.turretdescription_button_buy).setVisibility(View.INVISIBLE);
		else
			getView().findViewById(R.id.turretdescription_button_buy).setVisibility(View.VISIBLE);
		
		
		setFonts();
	}
	
	private void setFonts() {
		Context context = getView().getContext();
		
		((TextView)getView().findViewById(R.id.turretdescription_textview_description)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((Button)getView().findViewById(R.id.turretdescription_button_back)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
		((Button)getView().findViewById(R.id.turretdescription_button_buy)).setTypeface(StaticResources.getFont(context, Font.TREASURE));
	}

	@Override
	public void onClick(View v) {
		
		switch(v.getId()) {
		case R.id.turretdescription_button_buy:
			//DEBUG
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
			SharedPreferences.Editor edit = sp.edit();
			edit.putBoolean(boughtKey, true);
			edit.commit();
			//END DEBUG
			/*
			if(PlayerLevels.curLevelTokens >= cost) {
				//buy turret
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
				SharedPreferences.Editor edit = sp.edit();
				edit.putBoolean(boughtKey, true);
				edit.commit();
			} else {
				DialogMoneyPrompt.getInstance().show(getActivity().getSupportFragmentManager(), DialogMoneyPrompt.TAG);
			}
			*/
		case R.id.turretdescription_button_back:
			dismiss();
			break;
		
		}
		
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		if(getActivity() instanceof ActivityUpgrades) {
			ActivityUpgrades activity = (ActivityUpgrades)getActivity();
			activity.setHeaderLevels();
			activity.initializeTurretSelectors();
		}
	}
}
