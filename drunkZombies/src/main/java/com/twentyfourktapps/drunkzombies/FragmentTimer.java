package com.twentyfourktapps.drunkzombies;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;

public class FragmentTimer extends Fragment {

	final String TAG = "FragmentTimer";
	final static String BK_WAVE_LENGTH = "FragmentTimer.waveLength";
	final static String BK_TIME_REMAINING = "FragmentTimer.TimeRemaining";
	
	final long BASE_WAVELENGTH = 30000l;
	
	boolean isRunning = false;
	boolean isExpired = false;
	
	long waveLength;
	long waveFinTime;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		if(savedInstanceState != null && savedInstanceState.containsKey(BK_TIME_REMAINING)) {
			timeRemaining = savedInstanceState.getLong(BK_TIME_REMAINING);
			waveLength = savedInstanceState.getLong(BK_WAVE_LENGTH);
		}
		
		
		return inflater.inflate(R.layout.fragment_timer, container);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		if(isRunning) pause();
		outState.putLong(BK_WAVE_LENGTH, waveLength);
		outState.putLong(BK_TIME_REMAINING, timeRemaining);
	}	
	
	public void start() {
		isRunning = true;
		isExpired = false;
		waveLength = getWavelength();
		waveFinTime = System.currentTimeMillis() + waveLength;
	}
	
	long timeRemaining;
	public void pause() {
		timeRemaining = waveFinTime - System.currentTimeMillis();
		isRunning = false;
		
	}
	public void resume() {		
		waveFinTime = System.currentTimeMillis() + timeRemaining;
		if(System.currentTimeMillis() < waveFinTime) isRunning = true;		
	}
	
	public void update() {
		if(isRunning) {
			double timeLeftRatio = (double)(waveFinTime - System.currentTimeMillis()) / (double)waveLength;
			if(getView() != null) {
				ImageView iconIV = (ImageView)getView().findViewById(R.id.timerfragment_imageview_indicator);
				int maxDiff = getView().getWidth() - iconIV.getWidth();
				int offset = (int)(timeLeftRatio * (double)maxDiff);			
				
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				params.leftMargin = offset;
				
				iconIV.setLayoutParams(params);
				iconIV.invalidate();
			}
			
			if(System.currentTimeMillis() > waveFinTime) {
				isExpired = true;
				isRunning = false;
			}
		}
	}
	
	
	public long getWavelength() {
		return BASE_WAVELENGTH + (PlayerLevels.curLevelWave * 1000l);
	}
}
