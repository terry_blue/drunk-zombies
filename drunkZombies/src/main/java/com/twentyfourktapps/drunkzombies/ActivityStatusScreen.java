package com.twentyfourktapps.drunkzombies;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;

public class ActivityStatusScreen extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statusscreen);
		
		setFonts();
		setValues();
	}
	
	
	private void setValues() {
		PlayerLevels.init(ActivityStatusScreen.this);
		
		TextView totalTimeTV = (TextView)findViewById(R.id.statsscreen_textview_value_timeplayed);
		TextView upgradesBoughtTV = (TextView)findViewById(R.id.statsscreen_textview_value_upgradesbought);
		TextView wavesAttemptedTV = (TextView)findViewById(R.id.statsscreen_textview_value_wavesattempted);
		TextView wavesCompletedTV = (TextView)findViewById(R.id.statsscreen_textview_value_wavescompleted);
		TextView coinsEarnedTV = (TextView)findViewById(R.id.statsscreen_textview_value_coinsearned);
		TextView coinsSpentTV = (TextView)findViewById(R.id.statsscreen_textview_value_coinsspent);
		TextView tokensEarnedTV = (TextView)findViewById(R.id.statsscreen_textview_value_tokensearned);
		TextView tokensSpentTV = (TextView)findViewById(R.id.statsscreen_textview_value_tokensspent);
		TextView zombiesStoppedTV = (TextView)findViewById(R.id.statsscreen_textview_value_zombiesstopped);
		TextView bottlesLostTV = (TextView)findViewById(R.id.statsscreen_textview_value_bottleslost);
	
		
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		long totalTime = sp.getLong(Consts.SP_KEY_STATS_TIME_PLAYED, 0);
		long elapsedDays = totalTime / 86400000;
		long diff = totalTime % 86400000;
		long elapsedHours = diff / 3600000;
		diff = diff % 3600000;
		long elapsedMins = diff / 60000;
		diff = diff % 60000;
		long elapsedSecs = diff / 1000;
		String dayString = (elapsedDays != 1) ? " Days " : " Day ";
		String hourString = (elapsedHours != 1) ? " Hours " : " Hour ";
		String minString = (elapsedMins != 1) ? " Mins " : " Min ";
		String secString = (elapsedSecs != 1) ? " Secs." : " Sec.";
		
		String timePlayedString = "";
		if(totalTime >= 86400000) timePlayedString += Long.toString(elapsedDays) + dayString;
		if(totalTime >= 3600000) timePlayedString += Long.toString(elapsedHours) + hourString;
		if(totalTime >= 60000) timePlayedString += Long.toString(elapsedMins) + minString;
		timePlayedString += Long.toString(elapsedSecs) + secString;
		
		totalTimeTV.setText(timePlayedString);
		
		
		upgradesBoughtTV.setText(Integer.toString(sp.getInt(Consts.SP_KEY_STATS_UPGRADES_BOUGHT, 0)));
		wavesAttemptedTV.setText(Integer.toString(sp.getInt(Consts.SP_KEY_STATS_WAVES_ATTEMPTED, 0)));
		wavesCompletedTV.setText(Integer.toString(PlayerLevels.curLevelWave - 1));
		coinsEarnedTV.setText(Integer.toString(sp.getInt(Consts.SP_KEY_STATS_COINS_EARNED, 0)));
		coinsSpentTV.setText(Integer.toString(sp.getInt(Consts.SP_KEY_STATS_COINS_SPENT, 0)));
		
		
		
		zombiesStoppedTV.setText(Long.toString(sp.getLong(Consts.SP_KEY_STATS_TOTAL_ZOMBIES_KILLED, 0)));
		bottlesLostTV.setText(Long.toString(sp.getLong(Consts.SP_KEY_STATS_BOTTLES_LOST, 0)));
		
		tokensEarnedTV.setText(Integer.toString(sp.getInt(Consts.SP_KEY_STATS_TOKENS_EANRED, 0)));
		tokensSpentTV.setText(Integer.toString(sp.getInt(Consts.SP_KEY_STATS_TOKENS_SPENT, 0)));
		
	}
	private void setFonts() {
	
		TextView totalTimeLabel = (TextView)findViewById(R.id.statsscreen_textview_label_timeplayed);
		TextView upgradesBoughtLabel = (TextView)findViewById(R.id.statsscreen_textview_label_upgradesbought);
		TextView wavesAttemptedLabel = (TextView)findViewById(R.id.statsscreen_textview_label_wavesattempted);
		TextView wavesCompletedLabel = (TextView)findViewById(R.id.statsscreen_textview_label_wavescompleted);
		TextView coinsEarnedLabel = (TextView)findViewById(R.id.statsscreen_textview_label_coinsearned);
		TextView coinsSpentLabel = (TextView)findViewById(R.id.statsscreen_textview_label_coinsspent);
		TextView tokensEarnedLabel = (TextView)findViewById(R.id.statsscreen_textview_label_tokenseanred);
		TextView tokensSpentLabel = (TextView)findViewById(R.id.statsscreen_textview_label_tokensspent);
		TextView zombiesStoppedLabel = (TextView)findViewById(R.id.statsscreen_textview_label_zombieskilled);
		TextView bottlesLostLabel = (TextView)findViewById(R.id.statsscreen_textview_label_bottleslost);
		
		TextView totalTimeTV = (TextView)findViewById(R.id.statsscreen_textview_value_timeplayed);
		TextView upgradesBoughtTV = (TextView)findViewById(R.id.statsscreen_textview_value_upgradesbought);
		TextView wavesAttemptedTV = (TextView)findViewById(R.id.statsscreen_textview_value_wavesattempted);
		TextView wavesCompletedTV = (TextView)findViewById(R.id.statsscreen_textview_value_wavescompleted);
		TextView coinsEarnedTV = (TextView)findViewById(R.id.statsscreen_textview_value_coinsearned);
		TextView coinsSpentTV = (TextView)findViewById(R.id.statsscreen_textview_value_coinsspent);
		TextView tokensEarnedTV = (TextView)findViewById(R.id.statsscreen_textview_value_tokensearned);
		TextView tokensSpentTV = (TextView)findViewById(R.id.statsscreen_textview_value_tokensspent);
		TextView zombiesStoppedTV = (TextView)findViewById(R.id.statsscreen_textview_value_zombiesstopped);
		TextView bottlesLostTV = (TextView)findViewById(R.id.statsscreen_textview_value_bottleslost);
		
	
		Typeface kreeTTF = Typeface.createFromAsset(getAssets(), "fonts/kree.ttf");
		totalTimeTV.setTypeface(kreeTTF);
		upgradesBoughtTV.setTypeface(kreeTTF);
		wavesAttemptedTV.setTypeface(kreeTTF);
		wavesCompletedTV.setTypeface(kreeTTF);
		coinsEarnedTV.setTypeface(kreeTTF);
		coinsSpentTV.setTypeface(kreeTTF);
		tokensEarnedTV.setTypeface(kreeTTF);
		tokensSpentTV.setTypeface(kreeTTF);
		zombiesStoppedTV.setTypeface(kreeTTF);
		bottlesLostTV.setTypeface(kreeTTF);
		
		totalTimeLabel.setTypeface(kreeTTF);
		upgradesBoughtLabel.setTypeface(kreeTTF);
		wavesAttemptedLabel.setTypeface(kreeTTF);
		wavesCompletedLabel.setTypeface(kreeTTF);
		coinsEarnedLabel.setTypeface(kreeTTF);
		coinsSpentLabel.setTypeface(kreeTTF);
		tokensEarnedLabel.setTypeface(kreeTTF);
		tokensSpentLabel.setTypeface(kreeTTF);
		zombiesStoppedLabel.setTypeface(kreeTTF);
		bottlesLostLabel.setTypeface(kreeTTF);
		
	}
}
