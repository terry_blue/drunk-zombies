package com.twentyfourktapps.drunkzombies.units;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.AttributeSet;

import com.twentyfourktapps.drunkzombies.R;

public class PowersMain extends BulletMain {

	final String TAG = "PowersMain";
	final public Bitmap targetBitmap;
	public boolean isTargeting;
	public PointF targetCentre;
	
	public PowersMain(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		targetBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.target);
		if(!isInEditMode())
			targetCentre = new PointF();
		
	}
	
	@Override
	public void draw(Canvas canvas) {
		if(isTargeting) canvas.drawBitmap(targetBitmap, targetCentre.x - (targetBitmap.getWidth() / 2), targetCentre.y - (targetBitmap.getHeight() / 2), null);
		super.draw(canvas);
	}
	
	
	public void updateTargetCentrePoint(float x, float y) {
		targetCentre.x = x;
		targetCentre.y = y;
	}	
	
	public void startTargeting() {
		isTargeting = true;
	}
	
	public void stopTargeting() {
		isTargeting = false;
	}
	

}
