package com.twentyfourktapps.drunkzombies.units;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

import com.twentyfourktapps.drunkzombies.interfaces.UnitInterface;


public abstract class AbstractView extends View implements UnitInterface {
		
	final String TAG = "AbstractView";
	public ArrayList<AbstractUnit> members;	
	
	public AbstractView(Context context, AttributeSet attrs) {
		super(context, attrs);		
		members = new ArrayList<AbstractUnit>();		
	}
	
	
	
	@Override
	public void update() {
		Iterator<AbstractUnit> it = members.iterator();
		while(it.hasNext()) {
			AbstractUnit member = it.next();
			if(member.isLive) member.update();
			else it.remove();
		}			
		
		consumeInvalidMembers();
		invalidate();
	}
	
	@Override
	public void onDraw(Canvas canvas) {
		draw(canvas);
	}
	@Override
	public void draw(@NonNull Canvas canvas) {
		for(AbstractUnit member : members) {
			member.draw(canvas);
			
		}
	}
	
	@Override
	public void spawn() {
		
		
	}
	@Override
	public void consume() {
		
	}
	
	public void consumeInvalidMembers() {		
		if(members.size() == 0) consume();
		else {
			RectF gameRect = new RectF(0, 0, getWidth(), getHeight());
			for(AbstractUnit unit : members) 
				unit.consumeInvalidMembers(gameRect);
		}
		
		
		
	}
	
	

}
