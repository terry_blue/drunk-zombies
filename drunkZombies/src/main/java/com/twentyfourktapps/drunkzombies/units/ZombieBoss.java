package com.twentyfourktapps.drunkzombies.units;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.FragmentGame;
import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.ZombieController;
import com.twentyfourktapps.libs.obj.ColourTickChanger;



public class ZombieBoss extends Zombie {

	final String TAG = "ZombieBoss";
	
	final static public int BUBBA = 2;
	final static public int BUTCHER = 0;
	final static public int BIG_MAMMA = 1;
	
	TickCount countdownTC;
	TickCount actionTC;
	
	float bossConsumePointX;
	
	public ZombieBoss(Context context, AbstractUnit parent) {
		super(context, parent);
		actionTC = new TickCount(ZombieController.getBossActionTickCount());
		countdownTC = new TickCount(ZombieController.getBossCountdownTickCount());
		
	}

	public ZombieBoss spawnBoss(int uSelection, float yCoord) {
		spawnZombie(uSelection, yCoord);
		velocityY = velocityX / 1.5f;
		
		if(uSelection == BUBBA)
			bossConsumePointX = FragmentGame.fragmentWidth / 2f;
		else bossConsumePointX = FragmentGame.zombieConsumePoint;
		
		countdownTC.start();
		return this;
	}
	
	@Override
	public void update() {
		if(isLive) {
			countdownTC.update();
			actionTC.update();			
			
			if(countdownTC.isLive && countdownTC.isExpired()) {
				//Countdown complete, start zombie boss action
				speed = 0f;
				isAnimating = false;
				switch(unitSelection) {
				case BUBBA:
					members.add(new Spew(context, this).spawnSpew());
					break;
				case BIG_MAMMA:
					AbstractUnit aU = PARENT.PARENT;
					if(aU instanceof ZombieSpawner)
						((ZombieSpawner)aU).commenceSpawning(0, ZombieController.getBossSpawnGroupSize(), ZombieController.getGroupsPerSpawner(), 3000, true);
					break;				
				}
					
				actionTC.start();
				
			}
			
			if(actionTC.isLive && actionTC.isExpired()) {
				//action timer expires, restart countdown
				switch(unitSelection) {
				case BUBBA:
				case BUTCHER:
				case BIG_MAMMA:
					setNormalSpeed();
					isAnimating = true;
					break;
				}
				
				countdownTC = new TickCount(ZombieController.getBossCountdownTickCount());
				countdownTC.start();			
			}

			super.update();
		}
	}
	
	@Override
	public void draw(Canvas canvas) {
		for(AbstractUnit aU : members) {
			if(aU instanceof Spew)
				aU.draw(canvas);
		}
		super.draw(canvas);
	}
	
	@Override
	protected void setEndPoint(PointF startPoint) {
		endPoint.set(startPoint.x - (FragmentGame.fragmentWidth / 10f), 0);
	}
	
	@Override
	public void checkForScreenPoints() {
		if((destRect.top <= 0 && velocityY < 1)
		|| (destRect.bottom >= FragmentGame.fragmentHeight && velocityY > 0))
			setVelocity(velocityX, velocityY * -1f);
		
		if(destRect.right > FragmentGame.fragmentWidth && velocityX > 0) {
			setXVelocity(velocityX * -1);
			Log.e(TAG, "zombie ceheckForcreenPoints() ");
		}
			
		
		switch(unitSelection) {
		case BUTCHER:
			super.checkForScreenPoints();
			break;
		case BIG_MAMMA:
			if(destRect.left <= FragmentGame.fragmentWidth / 2f)
				setXVelocity(0);
			break;
		case BUBBA:
			if(destRect.left <= (FragmentGame.zombieConsumePoint - getMoveSpeed()) && velocityX < 0)
				setXVelocity(velocityX * -1);
			break;
		}		
			
		
	}	
	
	
	@Override
	public void setPoisonSpeed() {
		setNormalVelocity();
		setVelocity((velocityX / 3f ) * 2f, (velocityY / 3f) * 2f);
	}
	@Override
	public UnitType getUnitType() {
		return UnitType.BOSS;
	}
	@Override
	public void consume() {
		SoundManager.playTakeThat();
		super.consume();
	}
	
	@Override
	public RectF getAdjustedDestRect() {
		final float DIVISOR = 3;
		float height = (FragmentGame.fragmentHeight / DIVISOR) * getScaleModifier();
		float hRatio = StaticResources.getBitmapHeight(context, UnitType.BOSS, unitSelection);
		float width = StaticResources.getFrameWidth(context, UnitType.BOSS, unitSelection) * hRatio;
		return new RectF(startPoint.x, startPoint.y, startPoint.x + width, startPoint.y + height);
	}
	
	public float getScaleModifier() {
		return 1;
	}
	
	
	
	private class Spew extends SingleUnit {		
		
		ColourTickChanger spewColourTC;
		final int COLOUR_SPEW_START = 0x9961e82d;
		final int COLOUR_SPEW_END = 0xdda4e82d;
		
		public Spew(Context context, AbstractUnit parent) {
			super(context, parent);
			spewColourTC = new ColourTickChanger(COLOUR_SPEW_START, COLOUR_SPEW_END, 24);
		}
		
		public Spew spawnSpew() {
			setVelocity(new PointF(ZombieBoss.this.destRect.left, ZombieBoss.this.destRect.top), new PointF(FragmentGame.zombieConsumePoint, ZombieBoss.this.destRect.top));
			spawn(UnitType.SPEW, 0);
			destRect.left = ZombieBoss.this.destRect.left - 2;
			destRect.top = ZombieBoss.this.destRect.top + (ZombieBoss.this.destRect.height() / 3f);
			destRect.right = destRect.left + 2;
			destRect.bottom = destRect.top + 2;			
			return this;
		}
		
		final static float GROWTH = 5f;
		@Override
		public void update() {
			if(isLive && destRect.height() < 0) 
				consume();
			
			if(isLive) {
				if((destRect.height()  < ZombieBoss.this.destRect.height()) && destRect.left > FragmentGame.zombieConsumePoint) {
					destRect.top -= GROWTH;
					destRect.bottom += GROWTH;
					destRect.left -= (GROWTH * 2);
					
				} else if(destRect.left <= FragmentGame.zombieConsumePoint) {
					//isDraining = true;
					destRect.top += GROWTH;
					destRect.bottom -= GROWTH;
					destRect.right -= (GROWTH * 2);
					
				} else if(destRect.height() >= ZombieBoss.this.destRect.height()) {
					setNormalSpeed();
					super.update();
				}
			} 
			
		}
		
		@Override
		public void draw(Canvas canvas) {
			paint.setColorFilter(new PorterDuffColorFilter(spewColourTC.getColor(), PorterDuff.Mode.SRC_ATOP));
			super.draw(canvas);
		}
		
		
		@Override
		public UnitType getUnitType() {
			return UnitType.SPEW;
		}

		@Override
		public float getScaleModifier() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		public RectF getAdjustedDestRect() {
			final float DIVSOR = 3;
			float height = (FragmentGame.fragmentHeight / DIVSOR) * getScaleModifier();
			float hRatio = height / StaticResources.getBitmapHeight(context, UnitType.ZOMBIE, unitSelection);
			float width = StaticResources.getFrameWidth(context, UnitType.ZOMBIE, unitSelection) * hRatio;
			return new RectF(startPoint.x, startPoint.y, startPoint.x + width, startPoint.y + height);
		}
		
		
		
	}
}
