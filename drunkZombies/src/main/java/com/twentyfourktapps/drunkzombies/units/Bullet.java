package com.twentyfourktapps.drunkzombies.units;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.FragmentGame;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.SoundManager;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.util.Util;

public class Bullet extends SingleUnit {	
	
	final String TAG = "Bullet";
	public final static int GREEN = 0;
	public final static int GREEN_BOOM = 4;
	public final static int BLUE = 1;
	public final static int YELLOW = 2;
	public final static int PLAYER = 3;
	
	public final static int BLUE_POWER = 5;
	public final static int GREEN_POWER = 6;
	public final static int YELLOW_POWER = 7;
	
	
	final static float[] DIVISORS = new float[] { 6, 6, 6, 5, 40, 12, 30, 20 };
	public boolean isHarmful, isExploding;
	int cur_ticks, hp;
	
	public Bullet(Context context, AbstractUnit parent) {
		super(context, parent);
	}

	@Override
	public void update() {
		if(isLive) {
			if(isExploding) {
				if(cur_ticks  > PlayerLevels.getMaxExplodeTicks(unitSelection)) {
					consume();
				}
				else {
					float explodeOffsetX = destRect.width() / getExplodeOffsetScale();
					float explodeOffsetY = destRect.height() / getExplodeOffsetScale();					
					destRect.top -= explodeOffsetY; destRect.bottom += explodeOffsetY;
					destRect.left -= explodeOffsetX; destRect.right += explodeOffsetX;					
				}				
			}			
			
			if(cur_ticks > PlayerLevels.getMaxBulletTicks(unitSelection)) {
				consume();	
			}
				
			
			
			if(unitSelection == YELLOW_POWER) {
				rotation += rotationSpeed;
				if(hp <= 0)
					consume();
			}			
			
			cur_ticks++;
			super.update();
		}		
	}
	
	
	@Override
	public void draw(Canvas canvas) {
		/*
		Paint p = new Paint();
		p.setColor(Color.BLUE);
		p.setAlpha(80);
		canvas.drawRect(destRect, p);
		*/
		canvas.save();		
		canvas.rotate((float)rotation, destRect.left + (destRect.width() / 2f), destRect.top + (destRect.height() / 2f));
		canvas.drawBitmap(StaticResources.getBitmap(context, UnitType.BULLET, unitSelection), null, destRect, paint);
		canvas.restore();		
	}
	
	@Override
	public void consumeInvalidMembers(RectF gameRect) {
		if(unitSelection == YELLOW_POWER) {
			if((destRect.left < 0 && velocityX < 0)
			|| (destRect.right > gameRect.right && velocityX > 0))
				setXVelocity(velocityX * -1);
			if((destRect.top < 0 && velocityY < 0)
			|| (destRect.bottom > gameRect.bottom && velocityY > 0))
				setYVelocity(velocityY * -1);
		} else 
			super.consumeInvalidMembers(gameRect);
		
		
	}
	
	public Bullet fire(int bulletType, PointF startingPoint, PointF destPoint, double rotation) {	
		this.rotation = rotation;	
		unitSelection = bulletType;
		
		RectF dRect = getAdjustedDestRect();
		PointF adjStartPoint = new PointF(startingPoint.x - (dRect.width() / 2f), startingPoint.y - (dRect.height() / 2f));
		
		setVelocity(adjStartPoint, destPoint);
		spawn(UnitType.BULLET, bulletType);		
		
		isHarmful = true;	
		
		switch(bulletType) {
		case PLAYER:
		case GREEN_BOOM: 
			speed = Consts.BULLET_SPEED_PLAYER;
			break;
		case BLUE: speed = Consts.BULLET_SPEED_BLUE;  break;
		case GREEN: speed = Consts.BULLET_SPEED_GREEN; break;
		case YELLOW: speed = Consts.BULLET_SPEED_YELLOW; isExploding = true; break;	
			
		case BLUE_POWER:
		case GREEN_POWER:
			break;
		case YELLOW_POWER:
			hp = Consts.BULLET_YELLOW_POWER_START_HP[PlayerLevels.curPowerYellowDamage] * PlayerLevels.curPowerYellowDamage;
		}		
		
		//update();
		return this;
	}	
	
	public boolean hit() {
		switch(unitSelection) {
		case YELLOW_POWER:
			hp -= Consts.uLevel_POWERYELLOW_DAMAGE[PlayerLevels.curPowerYellowDamage];
			break;
		case GREEN:
			SoundManager.playSound(SoundManager.POP2);
			PointF sPoint = new PointF(destRect.centerX(), destRect.centerY());
			((BulletSpawner)PARENT.PARENT).members.add(new BulletGroup(context, PARENT.PARENT).fire(GREEN_BOOM, sPoint, sPoint, 0, true));
		case PLAYER:
			consume();
			break;
		}
		return isHarmful;
	}
	
	@Override
	public void consume() {
		super.consume();
		isExploding = false;
		cur_ticks = 0;
	}

	@Override
	public float getScaleModifier() {
		return 1;
	}

	@Override
	public RectF getAdjustedDestRect() {
		
		int pos = 0;
		switch(unitSelection) {
		case GREEN: pos++;
		case BLUE: pos++;
		case YELLOW: pos++;
		case PLAYER: pos++;
		case GREEN_BOOM: pos++;
		case BLUE_POWER: pos++;
		case GREEN_POWER: pos++;
		case YELLOW_POWER: {
			float height = (FragmentGame.fragmentHeight / DIVISORS[pos]);
			float hRatio = height / StaticResources.getBitmapHeight(context, UnitType.BULLET, unitSelection);
			float width = StaticResources.getFrameWidth(context, UnitType.BULLET, unitSelection) * hRatio;
			return new RectF(startPoint.x, startPoint.y, startPoint.x + width, startPoint.y + height);
		}
			
		}
		return null;
	}
	

	public float getExplodeOffsetScale() {
		switch(unitSelection) {
		case YELLOW: {
			if(cur_ticks < 10) 
				return 40f;
			else if(cur_ticks < 14)
				return 120f;
			else return 200f;
		}
		default: return 2f;
		}
	}

	
	public static PointF getSize(Context cxt, int uType) {
		int pos = 0;
		float[] divies = new float[] { DIVISORS[4], DIVISORS[5], DIVISORS[6], DIVISORS[7] }; 
		switch(uType) {
		case GREEN: pos++;
		case BLUE: pos++;
		case YELLOW: pos++;
		case PLAYER: 		
			float height = (FragmentGame.fragmentHeight / divies[pos]);
			float hRatio = height / StaticResources.getBitmapHeight(cxt, UnitType.BULLET, uType);
			float width = StaticResources.getFrameWidth(cxt, UnitType.BULLET, uType) * hRatio;
			return new PointF(width, height);
		}
		
		return null;
	}
	
}
