package com.twentyfourktapps.drunkzombies.units;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.FragmentGame;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.ZombieController;
import com.twentyfourktapps.libs.obj.ColourTickChanger;

public class Zombie extends SingleUnit {
	
	final String TAG = "Zombie";
	
	final public static int BOB = 0;
	final public static int RACHEL = 1;
	final public static int GLOB = 2;
	final public static int CHEF = 3;
	final public static int PRINCESS = 4;
	final public static int HARRY = 5;
	final public static int KID_DED = 6;
	final public static int TWERP = 7;
	final public static int FRANK = 8;
	
	final static int COLOUR_HIT = 0x88DD2222;
	final static int COLOUR_NONE = 0x00000000;
	final static int COLOUR_POISON = 0x5500FF11;
	
	final static int COLOUR_FREEZE_START = 0x772f97c8;
	final static int COLOUR_FREEZE_END = 0x992de8d9;
	final static int COLOUR_BURN_START = 0x77e84b2d;
	final static int COLOUR_BURN_END = 0x99ff6600;
	final static int COLOUR_POISON_START = 0x9900dd33;
	final static int COLOUR_POISON_END = 0xdd00ff11;
	
	ColourTickChanger freezeColourTC, burnColourTC, poisonColourTC;
	
	boolean isDraining, isPreparingToDrain;
	
	int hp;
	RectF hpForeRect, hpBackRect;
	
	TickCount freezeTC, burnTC, poisonTC, kickBackTC, prepDrainTC;
	final ArrayList<TickCount> tickCounts;
	
	
	
	public Zombie(Context context, AbstractUnit parent) {
		super(context, parent);
		hpForeRect = new RectF();
		hpBackRect = new RectF();
		
		freezeColourTC = new ColourTickChanger(COLOUR_FREEZE_START, COLOUR_FREEZE_END, 24);
		burnColourTC = new ColourTickChanger(COLOUR_BURN_START, COLOUR_BURN_END, 24);
		poisonColourTC = new ColourTickChanger(COLOUR_POISON_START, COLOUR_POISON_END, 24);
		
		tickCounts = new ArrayList<TickCount>();
		tickCounts.add(freezeTC = new TickCount(PlayerLevels.curPowerBlueDamage * 5));
		tickCounts.add(burnTC = new TickCount(PlayerLevels.curPowerGreenDamage * 5));
		tickCounts.add(poisonTC = new TickCount((int)(Consts.uLevel_POISON[PlayerLevels.curPoison] * 3f)));
		tickCounts.add(kickBackTC = new TickCount(Consts.uLevel_KICK[PlayerLevels.curKick] * 3));
		tickCounts.add(prepDrainTC = new TickCount(20));
	}

	public Zombie spawnZombie(int uSelection, float yCoord) {
		unitSelection = uSelection;
		hp = getStartHp();
		
		//Set Position
		startPoint.set(FragmentGame.zombieSpawnPoint, yCoord);
		setEndPoint(startPoint);
				
		speed = (float)getMoveSpeed();
		
		//Reset TickCounters
		for(TickCount tc : tickCounts) tc.reset();
		
		//Finally show unit
		spawn(getUnitType(), uSelection);
		
		//TODO Adjust the size of the destRect
		
		
		
		setNormalVelocity();
		
		maxAnimationFrames = getAnimatedFrameCount();
		isAnimating = true;
		
		return this;
	}
	
	@Override
	public void update() {
		super.update();
		if(isLive) {
			for(TickCount tc : tickCounts) tc.update();
			updateMembers();
			
			if(kickBackTC.isLive && kickBackTC.isExpired()) {
				isAnimating = true;
				if(poisonTC.isLive) setPoisonSpeed();
				else setNormalSpeed();				
				
			}
			
			if(burnTC.isLive && burnTC.isExpired()) {
				
			}
			
			if(poisonTC.isLive && poisonTC.isExpired()) {
				setNormalSpeed();
			}
			
			if(freezeTC.isLive && freezeTC.isExpired()) {
				setNormalSpeed();
				isAnimating = true;				
			} 
			
			
			
			if(isPreparingToDrain) { //TODO determine if it should start consuming
				isDraining = true;
			}
		
			checkForScreenPoints();		
		}
		
		//Update health bar
		if(hp < getStartHp()) {
			hpBackRect.set(destRect);
			hpBackRect.bottom = hpBackRect.top + 8;
			
			float border = 2f; //TODO use Util.getPixelsForDp... (requires a reference to a context)
			float totalHpLength = hpBackRect.width() - (border * 2f);
			float ratio = hp / (float)getStartHp();		
			hpForeRect.set(hpBackRect);
			hpForeRect.inset(border, border);
			hpForeRect.right = hpForeRect.left + (ratio * totalHpLength);			
		}
	}
	
	@Override
	public void draw(Canvas canvas) {
		if(freezeTC.isLive) paint.setColorFilter(new PorterDuffColorFilter(freezeColourTC.getColor(), PorterDuff.Mode.SRC_ATOP));
		else if(burnTC.isLive) paint.setColorFilter(new PorterDuffColorFilter(burnColourTC.getColor(), PorterDuff.Mode.SRC_ATOP));
		else if(poisonTC.isLive) paint.setColorFilter(new PorterDuffColorFilter(poisonColourTC.getColor(), PorterDuff.Mode.SRC_ATOP));
		else paint.setColorFilter(new PorterDuffColorFilter(COLOUR_NONE, PorterDuff.Mode.SRC_ATOP));
		super.draw(canvas);
	}	
	
	@Override
	public void consumeInvalidMembers(RectF gameRect) {
		
	}
	
	@Override
	public void consume() {
		isDraining = false;
		super.consume();
	}
	
	//Decide what to do when the zombie hits certain areas of the screen
	public void checkForScreenPoints() {
		if(destRect.left <= (FragmentGame.zombieConsumePoint - getMoveSpeed())) 
			setXVelocity(0);
	}
	
	
	final static int HP_RECT_RADIUS = 2;
	public void drawHpBar(Canvas canvas) {
		if(isLive && !hpBackRect.isEmpty() && hp < getStartHp()) {			
			paint.setColor(Color.BLACK);
			canvas.drawRoundRect(hpBackRect, HP_RECT_RADIUS, HP_RECT_RADIUS, paint);
			paint.setColor(Color.RED);
			canvas.drawRoundRect(hpForeRect, HP_RECT_RADIUS, HP_RECT_RADIUS, paint);
		}
	}
	
	public void drawPows(Canvas canvas) {
		for(AbstractUnit aU : members) {
			if(aU instanceof Pow && aU.isLive) aU.draw(canvas);
		}
	}
	
	//Returns true if zombie is killed as a result of damage
	public boolean hit(int bulletType, float damage, boolean isCriticalHit, boolean isBulletHarmful) {
		if(isBulletHarmful) {
			if(burnTC.isLive) damage *= 2f;
			paint.setColorFilter(new PorterDuffColorFilter(COLOUR_HIT, PorterDuff.Mode.SRC_ATOP));
			hp -= (isCriticalHit) ? damage * 2 : damage;
			if(hp <= 0) {
				consume();
				return true;
			}
			
			switch (bulletType) {
			case Bullet.PLAYER: {
				if(!poisonTC.isLive && PlayerLevels.curPoison > 0) { //TODO determine chances
					poisonTC.start();
					setPoisonSpeed();
				}
				if(!kickBackTC.isLive && PlayerLevels.curKick > 0) { //TODO determine chances
					speed *= -1;
					kickBackTC.start();			
				}
				break;
			}
			
			case Bullet.BLUE_POWER: {
				freezeTC.start();
				isAnimating = false;
				speed = 0f;
				break;
			}
			
			case Bullet.GREEN_POWER: {
				if(!burnTC.isLive) burnTC.start();
				break;
			}
			}
			
			if(isCriticalHit) {
				members.add(new Pow(context, this).spawn(new PointF(destRect.left, destRect.top)));
			}
			
		}
		return false;
	}
	
	public void setPoisonSpeed() {
		speed = SPEED / 2f;
	}
	
	
	protected void setEndPoint(PointF startPoint) {
		endPoint.set(FragmentGame.zombieConsumePoint, startPoint.y);
	}
	public UnitType getUnitType() {
		return UnitType.ZOMBIE;
	}
	
	public double getConsumeRate() {		
		if(isDraining) return ZombieController.getZombieConsumeRate(getUnitType(), unitSelection);
		else return 0;
	}
	public int getStartHp() {
		return ZombieController.getZombieStartHp(getUnitType(), unitSelection);
	}
	@Override
	public double getMoveSpeed() {
		return ZombieController.getNormalZombieSpeed(getUnitType(), unitSelection);
	}
	public int getAnimatedFrameCount() {
		return StaticResources.getMaxFrameCount(context, getUnitType(), unitSelection);
	}

	@Override
	public RectF getAdjustedDestRect() {
		final float DIVSOR = 4;
		float height = (FragmentGame.fragmentHeight / DIVSOR) * getScaleModifier();
		float hRatio = height / StaticResources.getBitmapHeight(context, UnitType.ZOMBIE, unitSelection);
		float width = StaticResources.getFrameWidth(context, UnitType.ZOMBIE, unitSelection) * hRatio;
		RectF newRect = new RectF(startPoint.x, startPoint.y, startPoint.x + width, startPoint.y + height);
		return newRect;
	}

	@Override
	public float getScaleModifier() {
		switch(unitSelection) {
		case BOB: return 0.8f;
		case RACHEL: return 0.7f;
		case GLOB: return 0.9f;
		case CHEF: return 1f;
		case PRINCESS: return 0.6f;
		case HARRY: return 0.8f; 
		case KID_DED: return 0.5f;
		case TWERP: return 0.9f;
		case FRANK: return 1f;
		}
		return 1;
	}
}
