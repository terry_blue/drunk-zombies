package com.twentyfourktapps.drunkzombies.units;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.AttributeSet;

import com.twentyfourktapps.drunkzombies.obj.ZombieController;
import com.twentyfourktapps.drunkzombies.util.Util;


public class ZombieMain extends AbstractView {

	final static String TAG = "ZombieMain";
	
	public ZombieMain(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	public double getZombieDrain() {
		double drainAmount = 0;
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieSpawner && aU.isLive)
				drainAmount += ((ZombieSpawner)aU).getGroupDrainRate();
		}
		return drainAmount;
	}
	
	public PointF getClosestZombie() {
		ArrayList<PointF> points = new ArrayList<PointF>();
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieSpawner && aU.isLive) 
				points.add(((ZombieSpawner)aU).getClosestZombie());			
		}
		if(points.isEmpty()) return null;
		else return Util.getLeftMostPoint(points);
	}
	
	public void spawnZombies(boolean isBossGroup) {
		members.add(new ZombieSpawner(getContext()).commenceSpawning(ZombieController.getZombieType(), ZombieController.getZombiesPerGroup(), ZombieController.getGroupsPerSpawner(), ZombieController.getSpawnerTimeDelay(), isBossGroup));
	}
	public void spawnBoss() {
		members.add(new ZombieSpawner(getContext()).spawnBoss());
	}
	
	public boolean hasZombiesLive() {
		for(AbstractUnit aU : members) {
			if(aU.isLive)
				return true;
		}
		return false;
	}
	
	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		//draw zombies
		for(AbstractUnit aU : members) 
			aU.draw(canvas);
		
		//draw hp bars
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieSpawner) 
				((ZombieSpawner)aU).drawHPbars(canvas);
		}
		
		//draw POWS
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieSpawner) 
				((ZombieSpawner)aU).drawPOWS(canvas);
		}
	}
	
}
