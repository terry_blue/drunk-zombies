package com.twentyfourktapps.drunkzombies.units;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.obj.ZombieController;
import com.twentyfourktapps.drunkzombies.util.Util;


public class ZombieSpawner extends AbstractUnit {

	final static String TAG = "ZombieSpawner";
	
	boolean isActive, bossSpawner;
	long nextSpawnTime = -1;
	long spawnDelay = -1;
	int leftToSpawn = 0;
	int zombieType, perGroup;
	
	
	public ZombieSpawner(Context context) {
		super(context, null);
	}

	
	@Override
	public void update() {
		if(isActive) {
			Iterator<AbstractUnit> it = members.iterator();
			while(it.hasNext()) {
				AbstractUnit member = it.next();
				if(member.isLive) member.update();
				else it.remove();
			}
			if(leftToSpawn == 0 && members.size() == 0) 
				consume();
			else if(leftToSpawn > 0 && System.currentTimeMillis() > nextSpawnTime) {
				
				//Log.e(TAG, "Spawn frim update, leftToSpawn = " + leftToSpawn + ", nextSpawnTime =" + nextSpawnTime +", curTime = " + System.currentTimeMillis());
				nextSpawnTime = System.currentTimeMillis() + spawnDelay;
				spawn(zombieType, perGroup, bossSpawner);
			}
		}
	}
	
	
	public ZombieSpawner commenceSpawning(int uSelection, int uCount, int groupCount, long delay, boolean isBoss) {
		isActive = true;
		zombieType = uSelection;
		perGroup = uCount;
		leftToSpawn = groupCount;
		spawnDelay = delay;
		nextSpawnTime = 0;
		bossSpawner = isBoss;
		nextSpawnTime = System.currentTimeMillis() + spawnDelay;
		spawn();
		return this;
	}
	
	public void spawn(int uSelection, int uCount, boolean isBoss) {	
		//Log.e(TAG, "spawn");
		members.add(new ZombieGroup(context, this).spawn(uSelection, uCount, ZombieController.getIsBlockSpawn(), isBoss));
		leftToSpawn--;						
	}
	
	public ZombieSpawner spawnBoss() {
		isActive = true;
		int bossCount = ZombieController.getBossSpawnCount();
		for(int i = 0; i < bossCount; i++) {
			members.add(new ZombieGroup(context, this).spawnBoss(ZombieController.getBossZombieType()));
		}
		
		spawn();
		return this;
	}

	@Override
	public void consume() {
		isActive = false;
		super.consume();
	}
	
	public double getGroupDrainRate() {
		double drainAmount = 0;
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieGroup && aU.isLive)
				drainAmount += ((ZombieGroup)aU).getGroupConsumption();				
		}
		return drainAmount;
	}
	
	public PointF getClosestZombie() {
		ArrayList<PointF> points = new ArrayList<PointF>();
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieGroup && aU.isLive)
				points.add(((ZombieGroup)aU).getClosestZombie());
		}
		if(points.isEmpty()) return null;
		else return Util.getLeftMostPoint(points);
	}
	
	public void drawPOWS(Canvas canvas) {
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieGroup) 
				((ZombieGroup)aU).drawPOWS(canvas);
		}
	}
	
	public void drawHPbars(Canvas canvas) {
		for(AbstractUnit aU : members) {
			if(aU instanceof ZombieGroup) 
				((ZombieGroup)aU).drawHPbars(canvas);
		}
	}

}
