package com.twentyfourktapps.drunkzombies.units;

import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.units.SingleUnit.UnitType;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;

public class BulletMain extends AbstractView {

	public BulletMain(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}	
	
	public void fire(int bulletType, PointF startPoint, PointF endPoint, double rotation, boolean isHarmful) {
		PointF adjStartPoint = new PointF(startPoint.x - StaticResources.getFrameWidth(getContext(), UnitType.BULLET, bulletType), startPoint.y - StaticResources.getBitmapHeight(getContext(), UnitType.BULLET, bulletType));				
		members.add(new BulletSpawner(getContext()).fire(bulletType, adjStartPoint, endPoint, rotation, isHarmful));				
	}	
	
}
