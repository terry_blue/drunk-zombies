package com.twentyfourktapps.drunkzombies.units;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;

import com.twentyfourktapps.drunkzombies.obj.StaticResources;

public class Pow extends SingleUnit {
	
	TickCount explodeTC;
	final static float EXPAND_RATE = 1.5f; 
	
	public Pow(Context context, AbstractUnit parent) {
		super(context, parent);
		explodeTC = new TickCount(6);
	}

	@Override
	public void update() {
		explodeTC.update();
		if(explodeTC.isLive) destRect.inset(-(explodeTC.cur * EXPAND_RATE), - (explodeTC.cur * EXPAND_RATE));
		if(explodeTC.isExpired()) consume();
		super.update();
	}

	public Pow spawn(PointF sPoint) {
		startPoint = sPoint;
		explodeTC.start();
		setVelocity(8f, -12f);
		spawn(UnitType.POW, 0);
		return this;
	}

	@Override
	public float getScaleModifier() {
		return 1;
	}

	@Override
	public RectF getAdjustedDestRect() {
		return new RectF(startPoint.x, startPoint.y, startPoint.x + StaticResources.getFrameWidth(context, UnitType.POW, 0), startPoint.y + StaticResources.getBitmapHeight(context, UnitType.POW, 0));
	}
	
	
}
