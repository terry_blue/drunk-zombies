package com.twentyfourktapps.drunkzombies.units;

import android.content.Context;
import android.graphics.PointF;

public class BulletSpawner extends AbstractUnit {

	
	public BulletSpawner(Context context) {
		super(context, null);
	
	}

	
	public BulletSpawner fire(int bulletType, PointF startPoint, PointF endPoint, double rotation, boolean isHarmful) {
		members.add(new BulletGroup(context, this).fire(bulletType, startPoint, endPoint, rotation, isHarmful));
		spawn();
		return this;
	}
}
