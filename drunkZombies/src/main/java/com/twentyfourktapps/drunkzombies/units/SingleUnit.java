package com.twentyfourktapps.drunkzombies.units;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.util.Util.RectAdjuster;

public abstract class SingleUnit extends AbstractUnit implements RectAdjuster {

	final String TAG = "SingleUnit";
	
	static Context context = null;
	public static enum UnitType { ZOMBIE, BOSS, BULLET, POW, SPEW };
	public UnitType unitType;
	public int unitSelection;
	protected float velocityX, velocityY, speed = 15f;
	protected final float SPEED = 7f;
	public RectF destRect, startRect;
	double rotation, rotationSpeed = 30;
	
	protected boolean isAnimating;
	int maxAnimationFrames, curAnimationFrame = 0;
	
	Rect srcRect;
	Paint paint;
	
	PointF startPoint, endPoint;
	
	public SingleUnit(Context cxt, AbstractUnit parent) {
		super(cxt, parent);
		
		if(context == null)
			context = cxt;
		
		
		destRect = new RectF();
		srcRect = new Rect();
		paint = new Paint();
		startPoint = new PointF();
		endPoint = new PointF();
	}
	
	
	
	@Override
	public void update() {
		if(isLive) {
			destRect.offset(velocityX  * speed, velocityY * speed);
			
			if(isAnimating) {
				if(curAnimationFrame < maxAnimationFrames - 1) curAnimationFrame++;
				else curAnimationFrame = 0;
			}
		}
	}
	public void updateMembers() {
		super.update();
	}
	
	@Override
	public void draw(Canvas canvas) {
		if(isLive)
			canvas.drawBitmap(StaticResources.getBitmap(context, unitType, unitSelection), StaticResources.getSourceRect(context, unitType, unitSelection, curAnimationFrame), destRect, paint);				
	}
	
	public void spawn(UnitType uType, int uSelection) {
		unitType = uType;
		unitSelection = uSelection;	
		setNormalSpeed();
		
	
		destRect = getAdjustedDestRect();	
			
		isLive = true;
	}
	
	public void setVelocity(PointF startPoint, PointF endPoint) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		
		
		
		float diffX = endPoint.x - startPoint.x;
		float diffY = endPoint.y - startPoint.y;
		double magnitude = Math.sqrt((diffX * diffX) + (diffY * diffY));
		if(Double.isNaN(magnitude))  {
			Log.d(TAG, "destPoint.x = " + endPoint.x + ", startPoint.x = " + startPoint.x);
			Log.d(TAG, "destPoint.y = " + endPoint.y + ", startPoint.y = " + startPoint.y);
		} else setVelocity(diffX * (float)(1d / magnitude), diffY * (float)(1d / magnitude));
	}
	
	public void setVelocity(float velX, float velY) {
		velocityX = velX;
		velocityY = velY;		
	}
	public void setXVelocity(float velX) {
		velocityX = velX;
	}
	public void setYVelocity(float velY) {
		velocityY = velY;
	}
	public void setNormalVelocity() {
		setVelocity(startPoint, endPoint);
	}
	
	public void setNormalSpeed() {
		speed = (float)getMoveSpeed();
	}
	
	public double getMoveSpeed() {
		return SPEED;
	}
	
	@Override
	public void consumeInvalidMembers(RectF gameRect) {
		if(isLive) {
			if(destRect.left > gameRect.right
			|| destRect.right < gameRect.left
			|| destRect.top > gameRect.bottom
			|| destRect.bottom < gameRect.top)
				consume();
			
		}
	}
	
	
	public UnitType getUnitType() {
		return null;
	}
	
	public static class TickCount {
		final public int MAX;
		public int cur;
		public boolean isLive;
		
		public TickCount(int max) {
			MAX = max;
			cur = 0;
		}
		
		public void start() {
			cur = 0;
			isLive = true;
		}
		
		public void update() {
			if(isLive)
				cur++;
		}
		
		public void reset() {
			isLive = false;
			cur = 0;
		}
		
		public boolean isExpired() {
			if(cur >= MAX) {
				isLive = false;
				return true;
			}
			return false;
		}
		
		//returns a percentage based on completion (0 = 0% - 1.0 = 100%)
		public float getPercentComplete() {
			return (float)cur/(float)MAX;
		}
	}
	

	
}
