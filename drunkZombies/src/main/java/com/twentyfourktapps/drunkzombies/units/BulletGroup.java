package com.twentyfourktapps.drunkzombies.units;

import java.util.Iterator;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.Consts;
import com.twentyfourktapps.drunkzombies.obj.PlayerLevels;

public class BulletGroup extends AbstractUnit {	
	
	public BulletGroup(Context context, AbstractUnit parent) {
		super(context, parent);
		// TODO Auto-generated constructor stub
	}

	public RectF getGroupRect() {
		float top = 9999999; float left = 9999999; float right = 0; float bottom = 0;
		
		Iterator<AbstractUnit> it = members.iterator();
		while(it.hasNext()) {
			SingleUnit unit = (SingleUnit)it.next();
			if(unit.isLive) {
				if(unit.destRect.top < top) top = unit.destRect.top;
				if(unit.destRect.bottom > bottom) bottom = unit.destRect.bottom;
				if(unit.destRect.left < left) left = unit.destRect.left;
				if(unit.destRect.right > right) right = unit.destRect.right;
			
			}
		}		
		
		if(top < 9999999 && left < 9999999) return new RectF(left, top, right, bottom);
		
		return new RectF();
	}
	
	public BulletGroup fire(int bulletType, PointF startPoint, PointF endPoint, double rotationDeg, boolean isHarmful) {
		PointF destPoint = new PointF(endPoint.x, endPoint.y);		
		
		switch(bulletType) {
		case Bullet.PLAYER: {
			double[] offsets = new double[] { 0, 0, 2, 3, 4.5, 6, 7.5 };
			double diffX = endPoint.x - startPoint.x;
			double diffY = endPoint.y - startPoint.y;
			double adj = Math.sqrt((diffX * diffX) + (diffY * diffY));
			
			for(int i = 0; i < PlayerLevels.getBulletsPerGroup(bulletType); i++) {				
				double offset = offsets[PlayerLevels.getBulletsPerGroup(bulletType)] - (i * 3);
				double opp = Math.tan(Math.toRadians(offset)) * adj;
				double hyp = Math.sqrt((opp * opp) + (adj * adj));
				destPoint.x = (float)(startPoint.x + (Math.cos(Math.toRadians(rotationDeg + offset)) * hyp));
				destPoint.y = (float)(startPoint.y + (Math.sin(Math.toRadians(rotationDeg + offset)) * hyp));
				members.add(new Bullet(context, this).fire(bulletType, startPoint, destPoint, rotationDeg));								
			}
			break;
		}
		case Bullet.GREEN_BOOM:
			
		case Bullet.BLUE_POWER:
		case Bullet.GREEN_POWER:
		case Bullet.YELLOW_POWER: {
			int shardCount = PlayerLevels.getShardCount(bulletType);
			PointF[] explodePoints = Consts.getExplodePoints(shardCount);
			for(int i = 0; i < shardCount; i++) 
				members.add(new Bullet(context, this).fire(bulletType, startPoint, new PointF(startPoint.x + explodePoints[i].x, startPoint.y + explodePoints[i].y), rotationDeg));				
			break;
		}
		
		 
		default: {
			members.add(new Bullet(context, this).fire(bulletType, startPoint, destPoint, rotationDeg));
		}
		}
		
		spawn();		
		
		return this;
	}
}
