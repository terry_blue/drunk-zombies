package com.twentyfourktapps.drunkzombies.units;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;

import com.twentyfourktapps.drunkzombies.interfaces.UnitInterface;


public abstract class AbstractUnit implements UnitInterface {

	final private static String TAG = "AbstractUnit";
	static Context context = null;
	
	protected ArrayList<AbstractUnit> members; 
	final protected AbstractUnit PARENT;
	public boolean isLive = false;		
		
	public AbstractUnit(Context cxt, AbstractUnit parent) {
		if(context == null)
			context = cxt;
		
		PARENT = parent;
		members = new ArrayList<AbstractUnit>();
	}
	
	
	@Override
	public void update() {
		if(isLive) {
			Iterator<AbstractUnit> it = members.iterator();
			while(it.hasNext()) {
				AbstractUnit member = it.next();
				if(member.isLive) member.update();
				else it.remove();
			}
			if(members.size() == 0 && !(this instanceof Zombie)) consume();
		}
	}
	
	@Override
	public void draw(Canvas canvas) {
		for(AbstractUnit member : members) {
			if(isLive)
				member.draw(canvas);
		}
	}
	
	@Override
	public void spawn() {		
		isLive = true;
	}
	
	@Override
	public void consume() {
		isLive = false;
	}
	
	
	public void consumeInvalidMembers(RectF gameRect) {
		if(isLive) {			
			for(AbstractUnit unit : members) 
				if(unit.isLive) 
					unit.consumeInvalidMembers(gameRect);			
		}
	}
	
	public ArrayList<AbstractUnit> getMembers() {
		return members;
	}
}
