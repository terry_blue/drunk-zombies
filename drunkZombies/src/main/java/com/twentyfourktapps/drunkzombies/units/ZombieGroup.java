package com.twentyfourktapps.drunkzombies.units;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

import com.twentyfourktapps.drunkzombies.FragmentGame;
import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.ZombieController;
import com.twentyfourktapps.drunkzombies.units.SingleUnit.UnitType;
import com.twentyfourktapps.drunkzombies.util.Util;


public class ZombieGroup extends AbstractUnit {

    final private static String TAG = "ZombieGroup";
	public ZombieGroup(Context context, AbstractUnit parent) {
		super(context, parent);
		// TODO Auto-generated constructor stub
	}
	
	int screenPadding = 25;
	public ZombieGroup spawn(int uSelection, int unitCount, boolean isBlock, boolean isRandom) {
		//Log.e(TAG, "spawn unit count = " + unitCount);
		//Log.e(TAG, "fragHeaight = " + FragmentGame.fragmentHeight + ", Bitmap Heoght = " + StaticResources.getBitmapHeight(context, UnitType.ZOMBIE, uSelection) + ", screen padding = "+ screenPadding);
		int fixedHeight = FragmentGame.fragmentHeight - StaticResources.getBitmapHeight(context, UnitType.ZOMBIE, uSelection) - screenPadding;
		float centreY = fixedHeight / 2f;
		Random rand = new Random();
		
		if(isRandom) {
			for(int i = 0; i < unitCount; i++) {
				int zombieType = ZombieController.getZombieType();
				int yCoord = rand.nextInt(FragmentGame.fragmentHeight - StaticResources.getBitmapHeight(context, UnitType.ZOMBIE, zombieType));
				members.add(new Zombie(context, this).spawnZombie(zombieType, yCoord));
			}
			
		} else if(isBlock) {			
			float gap = (unitCount == 1) ? (float)fixedHeight / 2f : (float)fixedHeight / (float)(unitCount - 1);
			if(unitCount == 1) members.add(new Zombie(context, this).spawnZombie(uSelection, gap));
			else {
				for(int i = 0; i < unitCount; i++)
					members.add(new Zombie(context, this).spawnZombie(uSelection, (float)i * gap));
			}
			
		} else { //Creative spawning
			int bmpHeight = StaticResources.getBitmapHeight(context, UnitType.ZOMBIE, uSelection);
			switch(unitCount) {
			case 2:
				members.add(new Zombie(context, this).spawnZombie(uSelection, rand.nextInt(fixedHeight) + screenPadding));
			case 1: 
				members.add(new Zombie(context, this).spawnZombie(uSelection, rand.nextInt(fixedHeight) + screenPadding));
				break;
			
			case 7: 
				members.add(new Zombie(context, this).spawnZombie(uSelection, bmpHeight + 10));
				members.add(new Zombie(context, this).spawnZombie(uSelection, fixedHeight - (bmpHeight + 10)));			
			case 5:
				members.add(new Zombie(context, this).spawnZombie(uSelection, screenPadding));
				members.add(new Zombie(context, this).spawnZombie(uSelection, fixedHeight));			
			case 3:				
				for(int i = 0; i < 3; i++) {
					float offset = (centreY - bmpHeight) + ((bmpHeight) * i);
					members.add(new Zombie(context, this).spawnZombie(uSelection, offset + screenPadding));					
				}
				break;			
			
			case 4:
			case 6:
				for(int i = 0; i < unitCount + 1; i++) {
					float offset = (centreY - (bmpHeight * (unitCount / 2))) + (bmpHeight * i);
					if(i != (unitCount / 2))
						members.add(new Zombie(context, this).spawnZombie(uSelection, offset));
				}
				break;
			default: { //Default results in a random pattern
				spawn(uSelection, unitCount, false, true);
			}
					
		}
		}
		
		spawn();
		return this;
	}
	
	public ZombieGroup spawnBoss(int uSelection) {
		members.add(new ZombieBoss(context, this).spawnBoss(ZombieController.getBossZombieType(), FragmentGame.fragmentHeight / 2));
		spawn();
		return this;
	}
	
	public RectF getGroupRect() {
		float top = 9999999; float left = 9999999; float right = 0; float bottom = 0;
		
		Iterator<AbstractUnit> it = members.iterator();
		while(it.hasNext()) {
			SingleUnit unit = (SingleUnit)it.next();
			if(unit.isLive) {
				if(unit.destRect.top < top) top = unit.destRect.top;
				if(unit.destRect.bottom > bottom) bottom = unit.destRect.bottom;
				if(unit.destRect.left < left) left = unit.destRect.left;
				if(unit.destRect.right > right) right = unit.destRect.right;
			
			}
		}		
		
		if(top < 9999999 && left < 9999999) return new RectF(left, top, right, bottom);
		
		return new RectF();
	}
	
	public double getGroupConsumption() {
		double drainAmount = 0;
		for(AbstractUnit aU : members) {
			if(aU instanceof Zombie && aU.isLive)
				drainAmount += ((Zombie)aU).getConsumeRate();
		}
		return drainAmount;
	}
	
	public PointF getClosestZombie() {
		ArrayList<PointF> points = new ArrayList<PointF>();
		for(AbstractUnit aU : members) {
			if(aU instanceof Zombie && aU.isLive)
				points.add(new PointF(((Zombie)aU).destRect.left, ((Zombie)aU).destRect.top));
		}
		if(points.isEmpty()) return null;
		else return Util.getLeftMostPoint(points);
	}
	
	public void drawPOWS(Canvas canvas) {
		for(AbstractUnit aU : members) {
			if(aU instanceof Zombie && aU.isLive) 
				((Zombie)aU).drawPows(canvas);
		}
	}
	
	public void drawHPbars(Canvas canvas) {
		for(AbstractUnit aU : members) {
			if(aU instanceof Zombie && aU.isLive) 
				((Zombie)aU).drawHpBar(canvas);
		}
	}


}
