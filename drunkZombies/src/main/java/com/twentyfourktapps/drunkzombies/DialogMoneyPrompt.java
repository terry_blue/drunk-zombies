package com.twentyfourktapps.drunkzombies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.twentyfourktapps.drunkzombies.obj.StaticResources;
import com.twentyfourktapps.drunkzombies.obj.StaticResources.Font;

public class DialogMoneyPrompt extends DialogFragment implements OnClickListener{

	final static String TAG = "DialogMoneyPrompt";
	private static DialogMoneyPrompt instance = null;
	
	public static DialogMoneyPrompt getInstance() {
		if(instance == null) 
			instance = new DialogMoneyPrompt();
		
		return instance;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialog_theme);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_not_enough_money, container);
		
		view.findViewById(R.id.notenoughmoney_button_getmore).setOnClickListener(this);
		view.findViewById(R.id.notenoughmoney_button_ok).setOnClickListener(this);
		
		return view;
		
	}


	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.notenoughmoney_button_getmore: 
			getActivity().startActivity(new Intent(getView().getContext(), ActivityPurchase.class));
			
		case R.id.notenoughmoney_button_ok:
			dismiss();
		}
		
	}
	
	
	@Override
	public void onStart() {
		super.onStart();
		setFonts();
		
	}
	
	
	
	
	private void setFonts() {
		((TextView)getView().findViewById(R.id.notenoughmoney_textview_prompt)).setTypeface(StaticResources.getFont(getActivity(), Font.FEAST));
		((Button)getView().findViewById(R.id.notenoughmoney_button_getmore)).setTypeface(StaticResources.getFont(getActivity(), Font.KREEW));
		((Button)getView().findViewById(R.id.notenoughmoney_button_ok)).setTypeface(StaticResources.getFont(getActivity(), Font.KREEW));
	}
}
