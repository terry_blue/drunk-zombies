package com.twentyfourktapps.drunkzombies;

import java.util.Random;

import android.graphics.PointF;

public class Consts {
	
	final static String TAG = "Consts";
	public final static int FPS = 24;
	
	
	//TODO check level array lengths are consistent with their price counterparts
	public final static int[] uLevel_DAMAGE = new int[]{ 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 54, 58, 62, 64, 66, 68, 72, 76, 80 };
	public final static float[] uLevel_AGILITY = new float[] { 1f, 1.2f, 1.4f, 1.6f, 1.8f, 2f, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f};
	public final static int[] uLevel_CRITICAL = new int[] { 5, 10, 15, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78,80, 81, 82, 83, 84 };
	public final static int[] uLevel_POISON = new int[] { 15,20,25,30,35,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,72,74,76,78,80 };
	public final static int[] uLevel_KICK = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
	public final static float[] uLevel_MULTISHOT = new float[] { 1f, 2.5f, 2.55f, 2.6f, 2.65f, 2.7f, 3.5f, 3.54f, 3.58f, 3.62f, 3.64f, 4.5f, 4.54f, 4.58f, 4.62f, 5.5f, 5.55f, 5.6f, 6.5f, 6.55f, 6.6f, 6.65f, 6.7f, 6.75f, 6.8f, 6.85f, 6.9f, 6.95f };
	
	public final static int[] uLevel_TURRETBLUE = new int[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70  }; //Radius
	public final static int[] uLevel_TURRETYELLOW = new int[] { 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80  }; //Radius
	public final static int[] uLevel_TURRETGREEN = new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14  }; //Radius	
	public final static float[] uLevel_TURRETYELLOW_DAMAGE = new float[] { 1, 1.2f, 1.4f, 1.6f, 1.8f, 2, 2.2f, 2.4f, 2.6f, 2.8f, 3, 3.2f, 3.4f };
	public final static int[] uLevel_TURRETGREEN_DAMAGE = new int[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70 };
	public final static int[] uLevel_TURRETBLUE_DAMAGE = new int[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70 };
	public final static float[] uLevel_TURRETYELLOW_ROF = new float[] { 10f, 12f, 15f, 17f, 20f, 22f, 24f, 26f, 28f, 30f, 33f, 36f, 40f, 42f, 44f, 46f, 48f, 50f, 55f, 60f };
	public final static float[] uLevel_TURRETGREEN_ROF = new float[] { 0.1f, 0.2f, 0.5f, 0.7f, 0.8f, 0.9f, 1f, 1.2f, 1.4f, 1.5f, 1.6f, 1.7f, 1.8f, 1.9f, 2f, 2.1f, 2.2f, 2.3f, 2.4f, 0.8f   };
	public final static float[] uLevel_TURRETBLUE_ROF = new float[] { 1f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f, 1.8f, 1.9f, 2f, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f  };
	public final static float[] uLevel_TURRET_TURNSPEED = new float[] { 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 10f, 30f };
	
	public final static int[] uLevel_POWERYELLOW_DAMAGE = new int[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70 };
	public final static int[] uLevel_POWERGREEN_DAMAGE = new int[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70 };
	public final static int[] uLevel_POWERBLUE_DAMAGE = new int[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70 };
	public final static float[] uLevel_POWERYELLOW_LENGTH = new float[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };
	public final static float[] uLevel_POWERGREEN_LENGTH = new float[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70 };
	public final static float[] uLevel_POWERBLUE_LENGTH = new float[] { 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70 };
	public final static float[] uLevel_POWERYELLOW_RADIUS = new float[] { 0.08f, 0.1f, 0.12f, 0.14f, 0.16f, 0.18f, 0.2f, 0.22f, 0.24f, 0.26f, 0.28f, 0.3f, 0.32f, 0.34f, 0.36f, 0.38f, 0.4f, 0.42f, 0.44f, 0.46f, 0.48f, 0.5f, 0.52f, 0.54f, 0.56f, 0.58f, 0.6f, 0.62f, 0.64f, 0.66f, 0.68f, 0.7f };
	public final static float[] uLevel_POWERGREEN_RADIUS = new float[] { 1.1f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f, 1.8f, 1.9f, 2, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f, 3f, 3.1f, 3.2f, 3.3f, 3.4f, 3.5f, 3.6f, 3.7f, 3.8f, 3.9f, 4, 4.1f, 4.2f };
	public final static float[] uLevel_POWERBLUE_RADIUS = new float[] { 1.1f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f, 1.8f, 1.9f, 2, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f, 3f, 3.1f, 3.2f, 3.3f, 3.4f, 3.5f, 3.6f, 3.7f, 3.8f, 3.9f, 4, 4.1f, 4.2f };
	
	public final static int[] uLevel_BOTTLE_COUNT_RED = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };
	public final static int[] uLevel_BOTTLE_COUNT_ORANGE = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };
	public final static int[] uLevel_BOTTLE_COUNT_GREEN = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };
	public final static int[] uLevel_BOTTLE_QUALITY = new int[] { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100 };
	public final static float[] uLevel_BOTTLE_THICKNESS = new float[] { 0.05f, 0.1f, 0.15f, 0.2f, 0.25f, 0.3f, 0.35f, 0.4f, 0.45f, 0.5f, 0.55f, 0.6f, 0.65f, 0.7f, 0.75f, 0.8f, 0.85f, 0.9f, 0.95f, 0.99f  };
	
	public final static int[] uPrice_DAMAGE = new int[] { 100,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,2400,2600,2800,3000,3250,3500,3750,4000,4250,4500,4750,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000,10500,11000,12000,13000,14000,15000,16000,17000,18000,19000,20000,21000,22000,23000,24000,25000,26000,27000,28000,29000,30000,31000,32000,33000,34000,35000,36000,37000,38000,39000,40000,42000,44000,46000,48000,50000,55000,60000,65000,70000,75000,80000,85000,90000,95000,100000,110000,120000,130000,140000,150000,160000,170000,180000,200000 };
	public final static int[] uPrice_AGILITY = new int[] { 100,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,2400,2600,2800,3000,3250,3500,3750,4000,4250,4500,4750,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000,10500,11000,12000,13000,14000 };
	public final static int[] uPrice_CRITICAL = new int[] { 5000,10000,15000,20000,25000,30000,35000,40000,45000,50000,55000,60000,65000,70000,75000,80000,85000,90000,95000,100000,110000,120000,125000,130000,135000,140000,145000,150000,175000,200000 };
	public final static int[] uPrice_POISON = new int[] { 1000,2000,4000,6000,8000,10000,15000,20000,25000,30000,35000,40000,45000,50000,60000,70000,80000,100000,150000,200000,250000,300000,350000,400000,450000,500000 };
	public final static int[] uPrice_KICK = new int[] { 1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,12500,15000,17500,20000,25000,30000,35000,40000,45000,50000 };
	public final static int[] uPrice_MULTISHOT = new int[] { 1000,2500,5000,10000,15000,20000,30000,40000,50000,60000,70000,80000,90000,100000,120000,140000,160000,180000,200000,250000,300000,350000,400000,500000,600000,700000,800000 };
	
	public final static int[] uPrice_TURRETBLUE_UPGRADE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_TURRETGREEN_UPGRADE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_TURRETYELLOW_UPGRADE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_TURRETBLUE_DAMAGE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_TURRETGREEN_DAMAGE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_TURRETYELLOW_DAMAGE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static float[] uPrice_TURRETBLUE_ROF = new float[] { 1f, 1.2f, 1.4f, 1.6f, 1.8f, 2f, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f };
	public final static float[] uPrice_TURRETGREEN_ROF = new float[] { 1f, 1.2f, 1.4f, 1.6f, 1.8f, 2f, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f };
	public final static float[] uPrice_TURRETYELLOW_ROF = new float[] { 1f, 1.2f, 1.4f, 1.6f, 1.8f, 2f, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f };
	public final static int[] uPrice_TURRET_TURNSPEED = new int[] { 10, 10, 10, 10, 10, 7, 8, 9, 10, 11, 12, 13, 14 };
	
	public final static int[] uPrice_POWERYELLOW_DAMAGE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_POWERGREEN_DAMAGE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_POWERBLUE_DAMAGE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_POWERYELLOW_LENGTH = new int[] { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 22, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, }; //Number of nuts in exploding group
	public final static int[] uPrice_POWERGREEN_LENGTH = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_POWERBLUE_LENGTH = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_POWERYELLOW_RADIUS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_POWERGREEN_RADIUS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_POWERBLUE_RADIUS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	
	public final static int[] uPrice_BOTTLE_COUNT_RED = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42  };
	public final static int[] uPrice_BOTTLE_COUNT_ORANGE = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_BOTTLE_COUNT_GREEN = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42   };
	public final static int[] uPrice_BOTTLE_QUALITY = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 };
	public final static int[] uPrice_BOTTLE_THICKNESS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 12, 12, 13, 13, 17, 456, 456, 48, 8484, 4848, 654, 4964, 4645, 4645, 4654, 5456456, 545, 4, 4 ,4, 4, 4, 4, 4, 4  };
	
	public final static int price_TURRET_BLUE = 20;
	public final static int price_TURRET_GREEN = 100;
	public final static int price_TURRET_YELLOW = 200;
	
	public final static int BOTTLE_HP_RED = 50;
	public final static int BOTTLE_HP_ORANGE = 100;
	public final static int BOTTLE_HP_GREEN = 200;
	
	public final static long ZOMBIE_SPAWNER_TIME_DELAY = 750;
	public final static int[] ZOMBIE_START_HP = new int[] { 25, 30, 35, 40, 45, 50, 50, 60, 30, 20, 60, 70 };
	public final static int[] BOSS_START_HP = new int[] { 100, 120, 150 };
	public final static int[] ZOMBIE_COIN_REWARD = new int[] { 5, 2, 2, 4, 5, 8, 9, 7, 8, 50, 50, 50 };
	public final static int[] BOSS_COIN_REWARD = new int[] { 50, 50, 50 };
	public final static double[] ZOMBIE_MOVE_SPEED = new double[] { 3, 5, 5, 2, 3, 4, 7, 9, 8, 3, 7, 3, 1, 2, 3 };
	public final static double[] BOSS_MOVE_SPEED = new double[] { 3, 7, 5 };
	public final static double[] ZOMBIE_CONSUME_RATE = new double[] { 0.7d, 0.3d, 0.5d, 0.5d, 0.5d, 0.5f, 0.5d, 0.6d, 0.6d, 0.8d, 0.8d, 0.8d };
	public final static double[] BOSS_CONSUME_RATE = new double[] { 0.8d, 0.8d, 0.8d };
	public final static long[] ZOMBIE_CONSUME_START_DELAY = new long[] { 150, 250, 300, 100, 100, 100, 100, 100, 100, 120, 120, 120 };
	public final static long[] BOSS_CONSUME_START_DELAY = new long[] { 120, 120, 120 };
	
	
	public final static int[] BULLET_YELLOW_POWER_START_HP = new int[] { 50, 60, 70, 80, 90, 100, 110, 120, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190, 195, 200, 203, 206, 209, 212, 215, 218, 221, 224, 227, 230  };
	public final static int BULLET_SPEED_PLAYER = 30;
	public final static int BULLET_SPEED_BLUE = 80;
	public final static int BULLET_SPEED_GREEN = 100;
	public final static int BULLET_SPEED_YELLOW = 12;
	public final static int BULLET_SPEED_INCOMING = 50;	
	
	public final static String BundleKey_zombiesKilled = "gameplay_zombieskilled";
	public final static String BundleKey_victoryStatus = "victory_status";
	public final static String BundleKey_bottlesSaved = "bottles_saved";
	public final static String BundleKey_bonus_amount = "bonus_amount";
	
	public final static String SP_KEY_USER_ID = "udid";	
	public final static String SP_KEY_SYNC_COUNT_USER = "user_synccount";
	public final static String SP_KEY_SYNC_COUNT_CURRENCIES = "currency_synccount";
	public final static String SP_KEY_SYNC_COUNT_LEVELS = "levels_synccount";
	
	public final static String SP_KEY_DIFFICULTY = "key_difficulty";
	public final static String SP_KEY_TURRET_SPOT_1 = "key_turret_spot_1";
	public final static String SP_KEY_TURRET_SPOT_2 = "key_turret_spot_2";
	public final static String SP_KEY_TURRET_BLUE_BOUGHT = "key_turret_blue_bought";
	public final static String SP_KEY_TURRET_YELLOW_BOUGHT = "key_turret_yellow_bought";
	public final static String SP_KEY_TURRET_GREEN_BOUGHT = "key_turret_green_bought";
	
	public final static String SP_KEY_STATS_TIME_PLAYED = "key_totaltime_played";
	public final static String SP_KEY_STATS_UPGRADES_BOUGHT = "key_upgrades_bought";
	public final static String SP_KEY_STATS_WAVES_ATTEMPTED = "key_waves_attempted";
	public final static String SP_KEY_STATS_COINS_EARNED = "key_coins_earned";
	public final static String SP_KEY_STATS_COINS_SPENT = "key_coins_spent";
	public final static String SP_KEY_STATS_TOKENS_EANRED = "key_tokens_earned";
	public final static String SP_KEY_STATS_TOKENS_SPENT = "key_tokens_spent";
	public final static String SP_KEY_STATS_TOTAL_ZOMBIES_KILLED = "key_total_zombies_killed";
	public final static String SP_KEY_STATS_BOTTLES_LOST = "key_bottles_lost";
	
	public final static String BundleKey_upgradeDialog_upgradeId = "upgrade_dialog_id";
	
	public final static int BONUS_PER_WAVE = 5;
	public final static int BONUS_PER_ZOMBIE = 1;
	public final static int BONUS_PER_BOTTLE = 2;
	protected static String SP_KEY_STATS_TOKENS_EARNED;
	
	
	public static PointF[] getExplodePoints(int shardCount) {
		if(shardCount < 1) 
			return null;
		
		PointF[] array = new PointF[shardCount];
		
		Random rand = new Random();
		int offset = rand.nextInt(360);
		float gap = 360f / (float)shardCount;
		
		for(int i = 0; i < shardCount; i++) {
			float fullAngle = ((float)i * gap) + (float)offset;
			array[i] = new PointF((float)Math.sin(Math.toRadians(fullAngle)), (float)Math.cos(Math.toRadians(fullAngle)));
		}
	
		return array;
	}
	
}
